﻿using System.ComponentModel.DataAnnotations.Schema;
using VotoElectronico.Entidades.Shell;

namespace VotoElectronico.Entidades.Pk.PkSeguridad
{
    public class Sg08_UsuarioRol : Auditoria
    {

        public long Id { get; set; }

        #region relaciones
        [Index("IX_UsuarioRol", IsUnique = true, Order = 1)]
        public long RolId { get; set; }
        public virtual Sg07_Rol Rol { get; set; }

        [Index("IX_UsuarioRol", IsUnique = true, Order = 2)]
        public long UsuarioId { get; set; }
        public virtual Sg01_Usuario Usuario { get; set; }
        #endregion

    }
}
