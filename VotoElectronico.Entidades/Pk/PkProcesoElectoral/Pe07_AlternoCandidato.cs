﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using VotoElectronico.Entidades.Pk.PkSeguridad;
using VotoElectronico.Entidades.Shell;

namespace VotoElectronico.Entidades.Pk.PkProcesoElectoral
{
    public class Pe07_AlternoCandidato: Auditoria
    {
        public Pe07_AlternoCandidato()
        {
        }

        [ForeignKey("Candidato")]
        public long Id { get; set; }
        
        [StringLength(60)]
        public string Foto { get; set; }

        #region Relaciones
        public long PersonaId { get; set; }
        public virtual Sg02_Persona Persona { get; set; }
        public virtual Pe06_Candidato Candidato { get; set; }
        #endregion

    }
}
