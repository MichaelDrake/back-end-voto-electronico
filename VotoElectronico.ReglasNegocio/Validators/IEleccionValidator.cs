﻿using EcVotoElectronico.Repositorios;
using VotoElectronico.Entidades.Pk.PkProcesoElectoral;

namespace VotoElectronico.ReglasNegocio.Validators
{
    public class EleccionValidator : IEleccionValidator
    {
        private readonly IProcesoElectoralRepository _procesoElectoralRepository;
        public EleccionValidator(IProcesoElectoralRepository  procesoElectoralRepository)
        {
            _procesoElectoralRepository = procesoElectoralRepository;
        }
        public bool ConfiguradoEnProceso(long eleccionId)
           => _procesoElectoralRepository.GetExists<Pe01_ProcesoElectoral>(x => x.EleccionId == eleccionId);
    }
}
