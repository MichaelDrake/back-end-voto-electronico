﻿using EcVotoElectronico.Repositorios;
using System;
using VotoElectronico.Entidades.Pk.PkProcesoElectoral;
using VotoElectronico.Generico.Propiedades;

namespace VotoElectronico.ReglasNegocio.Validators
{
    public class ProcesoValidator : IProcesoValidator
    {
        private readonly IProcesoElectoralRepository _procesoElectoralRepository;
        private readonly ICandidatoRepository _candidatoRepository;
        private readonly IListaRepository _listaRepository;
        public ProcesoValidator(IProcesoElectoralRepository  procesoElectoralRepository,
            ICandidatoRepository candidatoRepository,
            IListaRepository listaRepository)
        {
            _procesoElectoralRepository = procesoElectoralRepository;
            _candidatoRepository = candidatoRepository;
            _listaRepository = listaRepository;
        }
        public void ValidarProceso(long procesoId)
        {
            if(!VerificarProcesoEditable(procesoId))
            throw new Exception("No se puede realizar la acción solicitada para un proceso ya iniciado");;
        }
        public void ValidarProcesoParaEmitirVoto(long procesoId)
        {
            if (!VerificarProcesoEnProceso(procesoId))
                throw new Exception("No se puede realizar la acción solicitada para un proceso no iniciado"); ;
        }

        public void ValidarProcesoCandidatoId(long candidatoId)
        {
            var candidato = _candidatoRepository.GetById<Pe06_Candidato>(candidatoId);
            if(candidato == null)
                throw new Exception("Candidato no existe");

            var procesoId = candidato.Lista?.ProcesoElectoralId ?? 0;
            if (!VerificarProcesoEditable(procesoId))
                throw new Exception("No se puede realizar la acción solicitada para un proceso ya iniciado"); ;
        }

        public void ValidarProcesoListaId(long listaId)
        {
            var lista = _listaRepository.GetById<Pe05_Lista>(listaId);
            if(lista == null)
                throw new Exception("Lista no existe");

            var procesoId = lista.ProcesoElectoralId;
            if (!VerificarProcesoEditable(procesoId))
                throw new Exception("No se puede realizar la acción solicitada para un proceso ya iniciado"); ;
        }


        public bool VerificarProcesoEditable(long procesoId)
        {
            var proceso = _procesoElectoralRepository.GetById<Pe01_ProcesoElectoral>(procesoId);
            return proceso != null && proceso.Estado.Equals(Auditoria.EstadoActivo) && proceso.FechaInicio >= DateTime.Now && proceso.FechaFin >= DateTime.Now;
        }
        public bool VerificarProcesoEnProceso(long procesoId)
        {
            var now = DateTime.Now;
            var proceso = _procesoElectoralRepository.GetById<Pe01_ProcesoElectoral>(procesoId);
            return proceso != null && proceso.Estado.Equals(Auditoria.EstadoActivo) && proceso.FechaInicio <= now && proceso.FechaFin >= now;
        }
    }
}
