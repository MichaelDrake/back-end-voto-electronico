﻿using EcVotoElectronico.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using VotoElectronico.Entidades.Pk.PkProcesoElectoral;
using VotoElectronico.Entidades.Pk.PkSeguridad;
using VotoElectronico.Generico.Propiedades;

namespace VotoElectronico.ReglasNegocio.Validators
{
    public class CandidatoValidator : ICandidatoValidator
    {
        private readonly IListaRepository _listaRepository;
        private readonly IPersonaRepository _personaRepository;
        public CandidatoValidator(IListaRepository listaRepository,
            IPersonaRepository personaRepository)
        {
            _listaRepository = listaRepository;
            _personaRepository = personaRepository;
        }
        public void ValidarCandidato(Pe06_Candidato candidato)
        {
            if (candidato != null)
            {
                if (ValidarPersona(candidato.PersonaId))
                {
                    var lista = _listaRepository.GetById<Pe05_Lista>(candidato.ListaId);
                    if (lista != null)
                    {
                        var escaniosProceso = lista.ProcesoElectoral?.Eleccion?.Escanios?.Where(x => x.Estado.Equals(Auditoria.EstadoActivo));
                        if (escaniosProceso != null)
                        {
                            if (ValidarEscanioId(escaniosProceso.AsEnumerable(), candidato.EscanioId))
                            {
                                if (ValidarEscanioNoUtilizado(lista, candidato.EscanioId))
                                {
                                    if (ValidarEscaniosDisponibles(lista))
                                    {
                                        if (ValidarPersonaNoExisteListasProceso(lista.ProcesoElectoral?.Listas?.AsEnumerable(), candidato.PersonaId))
                                        {
                                            var aplicaAlpterno = lista.ProcesoElectoral?.Eleccion?.AplicaCandidatoAlterno ?? false;
                                            if (aplicaAlpterno)
                                            {
                                                if (ValidarPersona(candidato.Alterno?.PersonaId ?? 0))
                                                {
                                                    if (ValidarPersonaNoExisteEnAlternos(lista.ProcesoElectoral?.Listas?.AsEnumerable(), candidato.PersonaId))
                                                    {
                                                        if (ValidarPersonaNoExisteListasProceso(lista.ProcesoElectoral?.Listas?.AsEnumerable(), candidato.Alterno?.PersonaId ?? 0))
                                                        {
                                                            if (!ValidarPersonaNoExisteEnAlternos(lista.ProcesoElectoral?.Listas?.AsEnumerable(), candidato.Alterno?.PersonaId ?? 0))
                                                                throw new Exception("La persona alterna ya pertenece a una lista como alterno.");
                                                        }
                                                        else
                                                            throw new Exception("La persona alterna ya pertenece a una lista como principal.");
                                                    }
                                                    else
                                                        throw new Exception("La persona principal ya pertenece a una lista como alterno.");
                                                }
                                                else throw new Exception("persona alterna no encontrada");
                                            }
                                        }
                                        else
                                            throw new Exception("La persona ya pertenece a una lista.");
                                    }
                                    else
                                        throw new Exception("Lista sin escaños disponibles para asignar");
                                }
                                else
                                    throw new Exception("Escaño ya se encuentra asignado");
                            }
                            else
                                throw new Exception("Escaño inválido");
                        }
                        else
                            throw new Exception("Proceso sin Escaños");
                    }
                    else
                        throw new Exception("Lista inválida");
                }
                else
                    throw new Exception("persona no encontrada");
            }
            else
                throw new Exception("Candidato nulo");
        }
        bool ValidarEscanioNoUtilizado(Pe05_Lista lista, long escanioId)
        {
           return !(lista.candidatos?.Any(y => y.EscanioId == escanioId) ?? false);
        }
        bool ValidarEscaniosDisponibles(Pe05_Lista lista)
        {
            var numeroEscanios = lista.ProcesoElectoral?.Eleccion?.Escanios?.Where(x => x.Estado.Equals(Auditoria.EstadoActivo))?.Count() ?? 0;
            var escaniosUtilizados = lista.candidatos?.Where(x => x.Estado.Equals(Auditoria.EstadoActivo))?.Count() ?? 0;
            return escaniosUtilizados < numeroEscanios;
        }

        bool ValidarPersona(long personaId)
        {
            return _personaRepository.GetExists<Sg02_Persona>(x => x.Id == personaId && x.Estado.Equals(Auditoria.EstadoActivo));
        }

        bool ValidarEscanioId(IEnumerable<Pe04_Escanio> escaniosProceso, long escanioId)
            => escaniosProceso.Any(x => escanioId == x.Id);

        bool ValidarPersonaNoExisteListasProceso(IEnumerable<Pe05_Lista> listasProceso, long personaId)
        {
            return !(listasProceso?.Any(x => x.candidatos.Any(y => y.PersonaId == personaId)) ?? false);
        }
        bool ValidarPersonaNoExisteEnAlternos(IEnumerable<Pe05_Lista> listasProceso, long personaId)
        {
            return !(listasProceso?.Any(x => x.candidatos.Any(y => y.Alterno.PersonaId == personaId)) ?? false);
        }

    }
}
