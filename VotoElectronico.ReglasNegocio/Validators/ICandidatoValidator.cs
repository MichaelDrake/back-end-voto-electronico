﻿
using VotoElectronico.Entidades.Pk.PkProcesoElectoral;

namespace VotoElectronico.ReglasNegocio.Validators
{
    public interface ICandidatoValidator
    {
        void ValidarCandidato(Pe06_Candidato candidato);
    }

}
