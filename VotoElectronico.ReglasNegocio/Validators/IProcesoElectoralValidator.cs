﻿
namespace VotoElectronico.ReglasNegocio.Validators
{
    public interface IProcesoValidator
    {
        void ValidarProceso(long procesoId);
        void ValidarProcesoCandidatoId(long candidatoId);
        void ValidarProcesoListaId(long listaId);
        void ValidarProcesoParaEmitirVoto(long procesoId);
    }

}
