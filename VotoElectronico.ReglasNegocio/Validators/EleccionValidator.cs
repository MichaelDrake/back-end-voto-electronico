﻿
namespace VotoElectronico.ReglasNegocio.Validators
{
    public interface IEleccionValidator
    {
        bool ConfiguradoEnProceso(long eleccionId);
    }

}
