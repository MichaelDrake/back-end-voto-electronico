﻿using System.Linq;
using VotoElectronico.Entidades.Pk.PkSeguridad;
using VotoElectronico.Generico;
using VotoElectronico.Generico.Configs;
using VotoElectronico.Generico.Enumeraciones;
using VotoElectronicoExtensiones.Configuraciones;

namespace VotoElectronico.LogicaCondicional
{
    public class Sg01_UsuarioCondicional
    {

        public ISpecification<Sg01_Usuario> FiltrarUsuario(string nombre = "", string identificacion = "")
        {
            return  new Specification<Sg01_Usuario>(
                x=>(string.IsNullOrEmpty(nombre) || x.NombreUsuario.Contains(nombre)) 
                && (string.IsNullOrEmpty(nombre) || x.Persona.Identificacion.Contains(identificacion))
                );
        }

        public ISpecification<Sg01_Usuario> FiltrarListaUsuariosByParams(DtoEspecificacion dto)
        {
            var rolAdministrador = Enumeration.ObtenerDescripcion(Roles.Administrador);
            return new Specification<Sg01_Usuario>(
                usuario => (string.IsNullOrEmpty(dto.parametroBusqueda1) || usuario.NombreUsuario.Contains(dto.parametroBusqueda1))
                && (string.IsNullOrEmpty(dto.parametroBusqueda6) || usuario.Estado.Equals(dto.parametroBusqueda6))
                && (!usuario.UsuariosRol.Any(x => x.Rol.NombreRol.Equals(rolAdministrador))
                ));
        }
    }
}
