﻿using System.Linq;
using VotoElectronico.Entidades.Pk.PkSeguridad;
using VotoElectronico.Generico;
using VotoElectronico.Generico.Configs;
using VotoElectronico.Generico.Enumeraciones;
using VotoElectronico.Generico.Propiedades;
using VotoElectronicoExtensiones.Configuraciones;

namespace VotoElectronico.LogicaCondicional
{
    public class Sg02_PersonaCondicional
    {


        public ISpecification<Sg02_Persona> FiltrarPersonaPorIdentificacion(string identificacion = "")
        {
            var rolAdministrador = Enumeration.ObtenerDescripcion(Roles.Administrador);
            return new Specification<Sg02_Persona>(
                persona => (string.IsNullOrEmpty(identificacion) || persona.Identificacion.Contains(identificacion) )
                && persona.Estado.Equals(Auditoria.EstadoActivo)
                && !persona.Usuarios.Any(x => x.UsuariosRol.Any(y => y.Rol.NombreRol.Equals(rolAdministrador)))
                );
        }

        /// <summary>
        /// Funcion para filtrar listas de personas por parametros
        /// </summary>
        /// <param name="dto.parametroBusqueda1"> Llegan campos para busqueda por nombre</param>
        /// <param name="dto.parametroBusqueda2"> Identificacion de la persona </param>
        /// <param name="dto.parametroBusqueda3"> Email de la persona </param>
        /// <param name="dto.parametroBusqueda6"> Estado (ACTIVO/INACTIVO)</param>
        /// <returns></returns>
        public ISpecification<Sg02_Persona> FiltrarPersonaByParams(DtoEspecificacion dto)
        {
            var RolAdministrador = Enumeration.ObtenerDescripcion(Roles.Administrador);

            return new Specification<Sg02_Persona>(
                persona => (string.IsNullOrEmpty(dto.parametroBusqueda1) 
                || persona.NombreUno.Contains(dto.parametroBusqueda1)
                || persona.NombreDos.Contains(dto.parametroBusqueda1)
                || persona.ApellidoUno.Contains(dto.parametroBusqueda1)
                || persona.ApellidoDos.Contains(dto.parametroBusqueda1))
                && (string.IsNullOrEmpty(dto.parametroBusqueda2) || persona.Identificacion.Contains(dto.parametroBusqueda2))
                && (string.IsNullOrEmpty(dto.parametroBusqueda3) || persona.Email.Contains(dto.parametroBusqueda3))
                 && (string.IsNullOrEmpty(dto.parametroBusqueda6)  || persona.Estado.Equals(dto.parametroBusqueda6))
                 && !persona.Usuarios.Any(usuario => usuario.UsuariosRol.Any(rol => rol.Rol.NombreRol.Equals(RolAdministrador)))
                );
        }
    }
}
