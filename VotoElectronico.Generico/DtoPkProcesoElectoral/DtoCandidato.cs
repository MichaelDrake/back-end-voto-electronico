﻿using Newtonsoft.Json;
using VotoElectronico.Entidades.Pk.PkSeguridad;

namespace VotoElectronico.Generico
{
    public class DtoCandidato
    {
        [JsonProperty("id")]
        public long Id { get; set; }
        
        [JsonProperty("personaId")]
        public long personaId { get; set; }

        [JsonProperty("personaIdAlterno")]
        public long personaIdAlterno { get; set; }

        [JsonProperty("nombreCandidato")]
        public string nombreCandidato { get; set; }
        [JsonProperty("nombreCandidatoAlterno")]
        public string nombreCandidatoAlterno { get; set; }

        [JsonProperty("identificacion")]
        public string identificacion { get; set; }

        [JsonProperty("procesoElectoralId")]
        public long procesoElectoralId { get; set; }
        [JsonProperty("nombreProcesoElectoral")]
        public string nombreProcesoElectoral { get; set; }

        [JsonProperty("escanioId")]
        public long escanioId { get; set; }

        [JsonProperty("nombreEscanio")]
        public string nombreEscanio { get; set; }

        [JsonProperty("listaId")]
        public long listaId { get; set; }

        [JsonProperty("nombreLista")]
        public string nombreLista { get; set; }

        [JsonProperty("usuarioCreacion")]
        public string usuarioCreacion { get; set; }
        [JsonProperty("usuarioModificacion")]
        public string usuarioModificacion { get; set; }

        [JsonProperty("estado")]
        public string estado { get; set; }

        [JsonProperty("fotoObjeto")]
        public DtoImagen fotoObjeto { get; set; }
        [JsonProperty("fotoObjetoAlterno")]
        public DtoImagen fotoObjetoAlterno { get; set; }

        [JsonProperty("fotoUrl")]
        public string fotoUrl { get; set; }
        [JsonProperty("fotoUrlAlterno")]
        public string fotoUrlAlterno { get; set; }
        [JsonProperty("cantidadVotosPersonales")]
        public int CantidadVotosPersonales { get; set; }
        [JsonProperty("ganadorSugerido")]
        public bool GanadorSugerido { get; set; }

        [JsonProperty("ganadorSugeridoEmpate")]
        public bool GanadorSugeridoEmpate { get; set; }

        [JsonProperty("porcentajeCantidadVotosPersonales")]
        public decimal PorcentajeCantidadVotosPersonales { get; set; }
        [JsonProperty("porcentajeEmpadronadosVotaron")]
        public decimal PorcentajeEmpadronadosVotaron { get; set; }
        public DtoCandidato Alterno { get; set; }
        [JsonProperty("elegible")]
        public bool Elegible { get; set; }

        //Se aumenta el objeto persona


        public DtoPersona objetoPersona { get; set; }
        public DtoPersona objetoPersonaAlterno { get; set; }
    }
}
