﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace VotoElectronico.Generico
{
    public class DtoProcesoElectoral
    {
        [JsonProperty("id")]
        public long Id { get; set; }
        
        [JsonProperty("nombreProcesoElectoral")]
        public string nombreProcesoElectoral { get; set; }

        [JsonProperty("fechaInicio")]
        public DateTime fechaInicio { get; set; }

        [JsonProperty("fechaFin")]
        public DateTime fechaFin { get; set; }


        [JsonProperty("eleccionId")]
        public long eleccionId { get; set; }

        [JsonProperty("nombreEleccion")]
        public string nombreEleccion { get; set; }
        [JsonProperty("usuarioCreacion")]
        public string usuarioCreacion { get; set; }

        [JsonProperty("usuarioModificacion")]
        public string usuarioModificacion { get; set; }

        [JsonProperty("estado")]
        public string estado { get; set; }
        [JsonProperty("estadoProceso")]
        public string estadoProceso { get; set; }
        [JsonProperty("color")]
        public string color { get; set; }

        [JsonProperty("votoRealizado")]
        public bool votoRealizado { get; set; }

        // Campos adicionales al DTO para obtener mayor informacion.

        [JsonProperty("tipoEleccion")]
        public string tipoEleccion { get; set; }

        [JsonProperty("editable")]
        public bool editable { get; set; }

        [JsonProperty("aplicaAlterno")]
        public bool aplicaAlterno { get; set; }

        [JsonProperty("aplicaAlicuota")]
        public bool aplicaAlicuota { get; set; }


    }
}
