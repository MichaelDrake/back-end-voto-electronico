﻿using Newtonsoft.Json;
using System.Collections.Generic;
using VotoElectronico.Entidades.Shell;

namespace VotoElectronico.Generico
{
    public class DtoResultadosProcesoElectoral
    {
        public int NumeroEmpadronados { get; set; }
        public int NumeroEmpadronadosVotaron { get; set; }
        public int NumeroVotosValidos { get; set; }
        public int NumeroVotosPluriculturales { get; set; }
        public int NumeroVotosBlancos { get; set; }
        public int NumeroVotosNulos { get; set; }
        public decimal PorcentajeVotantes { get; set; }
        public bool SuperaNumeroEscaneos { get; set; }
        public int CantidadEscaniosSobrantes { get; set; }
        public bool EmpateDecimal { get; set; }
        public int CantidadEscaneos { get; set; }
        public bool AplicaAlicuota { get; set; }
        public bool AplicaAlterno { get; set; }
        public decimal cifraRepartidora { get; set; }
        public bool EleccionValida { get; set; }
        public List<DtoDatosProcesoCargo> DetallesProcesoCargo { get; set; }
        public List<DtoVotoDetalleLista> DetallesListas { get; set; }

    }

    public class DtoVotoDetalleLista
    {
        public bool EsTotal { get; set; }
        public bool EsNoValido { get; set; }
        public string NombreLista { get; set; }
        public string ImagenLista { get; set; }
        public List<DtoCandidato> Candidatos { get; set; }
        public long ListaId { get; set; }
        public int CantidadVotosLista { get; set; }
        public decimal PorcentajeEmpadronados { get; set; }
        public decimal NumeroEscaniosAsignadosDecimal { get; set; }
        public decimal NumeroEscaniosAsignadosSuma { get; set; }
        public int NumeroEscaniosAsignados { get; set; }
        public int NumeroEscaniosAsignadosSobrantes { get; set; }
        public int NumeroEscaniosAsignadosMasSobrantes { get; set; }
        public int EscaniosEmpatados { get; set; }
        public bool TieneCandidatosEmpatados { get; set; }
        public long EscanioId { get; set; }
        public List<DtoListaCoeficiente> Coeficientes { get; set; }
        public List<DtoDatosProcesoCargo> DetallesProcesoCargo { get; set; }
        public decimal TotalPonderados { get; set; }
        public decimal PorcentajePonderados { get; set; }
        public decimal PorcentajeValidosTotalLista { get; set; }

    }
    public class DtoListaCoeficiente
    {
        public long ListaId { get; set; }
        public decimal Coeficiente { get; set; }
        public bool Ganador { get; set; }
        public bool GanadorFaltante { get; set; }
    }
    public class DtoDatosProcesoCargo
    {
       
        public string NombreCargoProceso { get; set; }
        public int CantidadEmpadronadosCargo { get; set; }
        public decimal Participacion { get; set; }
        public decimal Alicuota { get; set; }
        public decimal Ponderacion { get; set; }
        public int CantidadVotosConsignados { get; set; }
        public decimal CantidadVotosPonderados { get; set; }
    }
}
