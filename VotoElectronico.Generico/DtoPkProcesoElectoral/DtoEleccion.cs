﻿using Newtonsoft.Json;


namespace VotoElectronico.Generico
{
    public class DtoEleccion
    {
        [JsonProperty("id")]
        public long Id { get; set; }
        
        [JsonProperty("nombreEleccion")]
        public string nombreEleccion { get; set; }

        [JsonProperty("tipoEleccionId")]
        public long tipoEleccionId { get; set; }

        [JsonProperty("nombreTipoEleccion")]
        public string nombreTipoEleccion { get; set; }

        [JsonProperty("usuarioCreacion")]
        public string usuarioCreacion { get; set; }
        [JsonProperty("usuarioModificacion")]
        public string usuarioModificacion { get; set; }

        [JsonProperty("estado")]
        public string estado { get; set; }
        [JsonProperty("etiquetaEscanios")]
        public string EtiquetaEscanios { get; set; }
        [JsonProperty("cantidadEscanios")]
        public int CantidadEscanios { get; set; }
        [JsonProperty("aplicaAlicuota")]
        public bool aplicaAlicuota { get; set; }
        [JsonProperty("aplicaAlterno")]
        public bool aplicaAlterno { get; set; }


    }
}
