﻿using VotoElectronico.Generico.Configs;

namespace VotoElectronico.Generico.Enumeraciones
{
    public enum EstadosEntidad
    {
        [Enumeration("ACTIVO")]
        Activo = 1,
        [Enumeration("INACTIVO")]
        Inactivo = 2,
        [Enumeration("TODOS")]
        Todos = 3
    }
    public enum Roles
    {
        [Enumeration("ADMINISTRADOR")]
        Administrador,
        [Enumeration("MIEMBRO DE JUNTA")]
        MiembroJunta,
        [Enumeration("ELECTOR")]
        Elector,

    }
    public enum TipoRecurso
    {
        [Enumeration("MENU")]
        Menu,
        [Enumeration("RUTA")]
        Ruta,
    }


    public enum EstadosVoto
    {
        [Enumeration("VALIDO")]
        VALIDO,
        [Enumeration("NULO")]
        NULO,
        [Enumeration("BLANCO")]
        BLANCO
    }
    public enum TipoEleccion
    {
        [Enumeration("Unipersonal")]
        UNIPERSONAL,
        [Enumeration("Pluripersonal")]
        PLURIPERSONAL,
    }

    public enum Cargos
    {
        [Enumeration("ESTUDIANTE")]
        ESTUDIANTE,
        [Enumeration("PROFESOR")]
        PROFESOR,
        [Enumeration("TRABAJADOR")]
        TRABAJADOR,
    }

    public enum EstadosProceso
    {
        [Enumeration("EN PROCESO")]
        EnProceso = 1,
        [Enumeration("NO INICIADO")]
        NoIniciado = 2,
        [Enumeration("FINALIZADO")]
        Finalizado = 3
    }
    public enum ColoresProceso
    {
        [Enumeration("#2ECC71")]
        EnProceso = 1,
        [Enumeration("#F1C40F")]
        NoIniciado = 2,
        [Enumeration("#CC0000")]
        Finalizado = 3
    }
}
