﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace VotoElectronico.Generico
{
    public class DtoVoto
    {
        [JsonProperty("mascara")]
        public string Mascara { get; set; }

        [JsonProperty("estado")]
        public string Estado { get; set; }

        [JsonProperty("pelcId")]
        public string ProcesoElectoralId { get; set; }

        [JsonProperty("opciones")]
        public IEnumerable<DtoOpcionVoto> opcionesVoto { get; set; }

        [JsonProperty("nombreProcesoElectoral")]
        public string nombreProcesoElectoral { get; set; }

        [JsonProperty("fechaVoto")]
        public DateTime? fechaVoto { get; set; }

    }


    public class DtoOpcionVoto
    {
     
        [JsonProperty("listaId")]
        public long listaId { get; set; }

        [JsonProperty("nombreLista")]
        public string nombreLista { get; set; }

        [JsonProperty("candidatoId")]
        public long candidatoId { get; set; }

        [JsonProperty("nombreCandidato")]
        public string nombreCandidato { get; set; }

        [JsonProperty("nombreCandidatoAlterno")]
        public string nombreCandidatoAlterno { get; set; }

        [JsonProperty("escanioId")]
        public long escanioId { get; set; }

        [JsonProperty("nombreEscanio")]
        public string nombreEscanio { get; set; }
    }

}
