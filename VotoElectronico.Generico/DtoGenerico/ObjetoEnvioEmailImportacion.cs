﻿using Newtonsoft.Json;
using VotoElectronico.Entidades.Pk.PkSeguridad;

namespace VotoElectronico.Generico
{
    public class ObjetoEnvioEmailImportacion
    {

        public  Sg02_Persona Persona { get; set; }
        public Sg01_Usuario Usuario { get; set; }
    }
}
