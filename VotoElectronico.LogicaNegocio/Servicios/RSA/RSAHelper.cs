﻿
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Security;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using VotoElectronico.LogicaNegocio.Servicios.RSA;

namespace VotoElectronico.LogicaNegocio.Servicios
{
    public class RsaHelper : IRsaHelper
    {
        private readonly RSACryptoServiceProvider _privateKey;
        private readonly RSACryptoServiceProvider _publicKey;

        public RsaHelper()
        {
            string public_pem = Encoding.Default.GetString(RecursosRSA.publickey);
            string private_pem = Encoding.Default.GetString(RecursosRSA.privatekey);

            _privateKey = GetPrivateKeyFromPemFile(private_pem);
            _publicKey = GetPublicKeyFromPemFile(public_pem);


        }

        public string FirmaDigital(string text)
        {
            //// Write the message to a byte array using UTF8 as the encoding.
            var encoder = new UTF8Encoding();
            var bytesText = encoder.GetBytes(text);
            //var encryptedBytes = _privateKey.SignData(bytesText, false);
            // Hash and sign the data.
            var signedData = HashAndSignBytes(bytesText, _privateKey);
            return Convert.ToBase64String(signedData);
        }

        public bool VerificarFirmaDigital(string datosOriginales, string datosFirmados)
        {
            var encoder = new UTF8Encoding();
            var datosOriginalesBytes = encoder.GetBytes(datosOriginales);
            var datosFirmadosBytes = Convert.FromBase64String(datosFirmados);
            //var decryptedBytes = _publicKey.VerifyData(Convert.FromBase64String(encrypted), false);
            return VerifySignedHash(datosOriginalesBytes, datosFirmadosBytes, _publicKey);
           //return Encoding.UTF8.GetString(decryptedBytes, 0, decryptedBytes.Length);
        }

        private RSACryptoServiceProvider GetPrivateKeyFromPemFile(string file)
        {
            using (TextReader privateKeyTextReader = new StringReader(file))
            {
                AsymmetricCipherKeyPair readKeyPair = (AsymmetricCipherKeyPair)new PemReader(privateKeyTextReader).ReadObject();

                RSAParameters rsaParams = DotNetUtilities.ToRSAParameters((RsaPrivateCrtKeyParameters)readKeyPair.Private);
                RSACryptoServiceProvider csp = new RSACryptoServiceProvider();
                csp.ImportParameters(rsaParams);
                return csp;
            }
        }

        private RSACryptoServiceProvider GetPublicKeyFromPemFile(String file)
        {
            using (TextReader publicKeyTextReader = new StringReader(file))
            {
                RsaKeyParameters publicKeyParam = (RsaKeyParameters)new PemReader(publicKeyTextReader).ReadObject();

                RSAParameters rsaParams = DotNetUtilities.ToRSAParameters(publicKeyParam);

                RSACryptoServiceProvider csp = new RSACryptoServiceProvider();
                csp.ImportParameters(rsaParams);
                return csp;
            }
        }

        public static byte[] HashAndSignBytes(byte[] DataToSign, RSACryptoServiceProvider RSAalg)
        {
            try
            {
                return RSAalg.SignData(DataToSign, CryptoConfig.MapNameToOID("SHA512"));
            }
            catch (CryptographicException e)
            {
                Console.WriteLine(e.Message);

                return null;
            }
        }


        public static bool VerifySignedHash(byte[] DataToVerify, byte[] SignedData, RSACryptoServiceProvider RSAalg)
        {
            return RSAalg.VerifyData(DataToVerify, CryptoConfig.MapNameToOID("SHA512"), SignedData);

            //try
            //{
            //    return RSAalg.VerifyData(DataToVerify, SHA512.Create(), SignedData);
            //}
            //catch (CryptographicException e)
            //{
            //    Console.WriteLine(e.Message);

            //    return false;
            //}
        }

    }
}
