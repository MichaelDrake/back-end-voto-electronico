﻿
using VotoElectronico.Entidades.Pk.PkProcesoElectoral;
using VotoElectronico.Generico;

namespace VotoElectronico.LogicaNegocio.Servicios
{
    public interface IProcesoElectoralService
    {

        DtoApiResponseMessage ObtenerProcesoElectoralMedianteId(DtoProcesoElectoral dto);

        DtoApiResponseMessage CrearProcesoElectoral(DtoProcesoElectoral dto, string token);

        DtoApiResponseMessage ActualizarProcesoElectoral(DtoProcesoElectoral dto, string token);

        DtoApiResponseMessage EliminarProcesoElectoral(DtoProcesoElectoral dto);

        DtoApiResponseMessage ObtenerProcesoElectoralesPorNombre(string nombreProcesoElectoral, string estado, int anio, bool finalizados = false);

        DtoApiResponseMessage ObtenerProcesoElectoralesVigentesByToken(string token);
        DtoApiResponseMessage ObtenerTodosProcesosElectoralesActivos();

        bool procesoElectoralEditable(long procesoElectoralId);

        DtoApiResponseMessage ObtenerProcesoElectoralesFinalizadosByParams(string token, string nombreProcesoElectoral, long anio);
    }
}

