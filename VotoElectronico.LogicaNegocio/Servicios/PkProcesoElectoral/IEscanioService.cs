﻿
using VotoElectronico.Generico;

namespace VotoElectronico.LogicaNegocio.Servicios
{
    public interface IEscanioService
    {

        DtoApiResponseMessage ObtenerEscanioMedianteId(DtoEscanio dto);

        DtoApiResponseMessage CrearEscanio(DtoEscanio dto, string token);

        DtoApiResponseMessage ActualizarEscanio(DtoEscanio dto, string token);

        DtoApiResponseMessage EliminarEscanio(DtoEscanio dto, string token);

        DtoApiResponseMessage ObtenerEscanios(string nombre, string estado);

        DtoApiResponseMessage ObtenerEscaniosPorListaId(long listaId);

        DtoApiResponseMessage ObtenerEscaniosPorProcesoElectoralId(long procesoElectoralId);
        DtoApiResponseMessage ObtenerEscaniosPorEleccionId(long eleccionId);
        DtoApiResponseMessage ReactivarEscanio(long idEscanio, string token);
        void CrearEscaniosEleccionPluripersonal(long eleccionId, int cantidadEscanios, string nombreEscanios, string token);
        void ActualizarEscaniosEleccionPluripersonal(long eleccionId, int cantidadEscanios, string nombreEscanios, string token);
        void EliminarEscaniosEleccionId(long eleccionId);
    }
}

