﻿
using VotoElectronico.Generico;

namespace VotoElectronico.LogicaNegocio.Servicios
{
    public interface IEleccionService
    {

        DtoApiResponseMessage ObtenerEleccionMedianteId(DtoEleccion dto);

        DtoApiResponseMessage CrearEleccion(DtoEleccion dto, string token);

        DtoApiResponseMessage ActualizarEleccion(DtoEleccion dto, string token);

        DtoApiResponseMessage EliminarEleccion(DtoEleccion dto, string token);

        DtoApiResponseMessage ObtenerEleccionesPorNombre(string nombre, string estado);
        DtoApiResponseMessage ReactivarElecion(long idEscanio, string token);
    }
}

