﻿using EcVotoElectronico.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using VotoElectronico.Entidades.Pk.PkProcesoElectoral;
using VotoElectronico.Generico;
using VotoElectronico.Generico.Configs;
using VotoElectronico.Generico.Enumeraciones;
using VotoElectronico.Generico.Propiedades;
using VotoElectronico.LogicaCondicional;
using VotoElectronico.Util;

namespace VotoElectronico.LogicaNegocio.Servicios
{
    public class EscanioService : IEscanioService
    {

        private readonly IEscanioRepository _escanioRepository;
        private IApiResponseMessage _apiResponseMessage;
        private readonly ISesionService _sesionService;
        private readonly IEleccionRepository _eleccionRepository;

        public EscanioService(IEscanioRepository escanioRepository, IApiResponseMessage utilitarios, ISesionService sesionService, IEleccionRepository eleccionRepository)
        {
            _escanioRepository = escanioRepository;
            _apiResponseMessage = utilitarios;
            _sesionService = sesionService;
            _eleccionRepository = eleccionRepository;
        }

        #region Métodos Publicos

        //public DtoApiResponseMessage ObtenerEscaniosPorNombre(string nombreEscanio)
        //{
        //    var spec = new Pe04_EscanioCondicional().FiltrarEscanioPorNombre(nombreEscanio);
        //    var dtoMapeado = MapearListaEntidadADtoEscanio(_escanioRepository.FiltrarEscanioPorNombre(spec));

        //    if (dtoMapeado.Count() != 0)
        //    {
        //        return _apiResponseMessage.crearDtoApiResponseMessage(dtoMapeado, "VE_PEL_ESC_005");
        //    }
        //    else
        //    {
        //        return _apiResponseMessage.crearDtoApiResponseMessage(dtoMapeado, "VE_PEL_ESC_006");
        //    }

        //}

        public DtoApiResponseMessage ObtenerEscaniosPorListaId(long listaId)
        {
            var spec = new Pe04_EscanioCondicional().FiltrarEscaniosMedianteListaId(listaId);
            var dtoMapeado = MapearListaEntidadADtoEscanio(_escanioRepository.FiltrarEscanios(spec));

            return dtoMapeado != null && dtoMapeado.Count() > 0 ? _apiResponseMessage.CrearDtoApiResponseMessage(dtoMapeado, "VE_PEL_ESC_005") :
                _apiResponseMessage.CrearDtoApiResponseMessage(null, "VE_PEL_ESC_006");

        }





        public DtoApiResponseMessage ObtenerEscanioMedianteId(DtoEscanio dto)
        {
            var Escanio = ObtenerEscanioId(dto.escanioId);

            if (Escanio != null)
            {
                var dtoMapeado = mapearEntidadADto(Escanio);
                return _apiResponseMessage.CrearDtoApiResponseMessage(dtoMapeado, "VE_PEL_ESC_004");
            }
            else
            {
                return _apiResponseMessage.CrearDtoApiResponseMessage(null, "VE_PEL_ESC_007");
            }
        }

        public DtoApiResponseMessage CrearEscanio(DtoEscanio dto, string token)
        {
            var escanio = mapearDtoAEntidad(dto, token);
            Crear(escanio);
            var dtoMapeado = mapearEntidadADto(escanio);
            return _apiResponseMessage.CrearDtoApiResponseMessage(dtoMapeado, "VE_PEL_ESC_001");
        }

        public DtoApiResponseMessage ActualizarEscanio(DtoEscanio dto, string token)
        {
            var Escanio = ObtenerEscanioId(dto.escanioId);

            if (Escanio != null)
            {
                Escanio.NombreEscanio = dto.nombreEscanio;
                Escanio.EleccionId = dto.eleccionId;
                Escanio.UsuarioModificacion = _sesionService.ObtenerUsuarioPorToken(token)?.NombreUsuario;
                Escanio.FechaModificacion = DateTime.Now;
                Actualizar(Escanio);
                var dtoMapeado = mapearEntidadADto(Escanio);
                return _apiResponseMessage.CrearDtoApiResponseMessage(dtoMapeado, "VE_PEL_ESC_002");
            }
            else
            {
                return _apiResponseMessage.CrearDtoApiResponseMessage(null, "VE_PEL_ESC_007");
            }
        }


        public DtoApiResponseMessage EliminarEscanio(DtoEscanio dto, string token)
        {
            var Escanio = ObtenerEscanioId(dto.escanioId);
            if (Escanio != null)
            {
                Escanio.Estado = Auditoria.EstadoInactivo;
                Escanio.FechaModificacion = DateTime.Now;
                Escanio.UsuarioModificacion = _sesionService.ObtenerUsuarioPorToken(token)?.NombreUsuario;
                var dtoMapeado = mapearEntidadADto(Escanio);
                Actualizar(Escanio);
                return _apiResponseMessage.CrearDtoApiResponseMessage(dtoMapeado, "VE_PEL_ESC_003");
            }
            else
            {
                return _apiResponseMessage.CrearDtoApiResponseMessage(null, "VE_PEL_ESC_007");
            }

        }

        public DtoApiResponseMessage ReactivarEscanio(long idEscanio, string token)
        {
            var Escanio = ObtenerEscanioId(idEscanio);
            if (Escanio != null)
            {
                Escanio.Estado = Auditoria.EstadoActivo;
                Escanio.FechaModificacion = DateTime.Now;
                Escanio.UsuarioModificacion = _sesionService.ObtenerUsuarioPorToken(token)?.NombreUsuario;

                var escanio = mapearEntidadADto(Escanio);
                Actualizar(Escanio);
                return _apiResponseMessage.CrearDtoApiResponseMessage(escanio, "VE_GNR_UP_001");
            }
            else
            {
                return _apiResponseMessage.CrearDtoApiResponseMessage(null, "VE_PEL_ESC_007");
            }

        }
        public void CrearEscaniosEleccionPluripersonal(long eleccionId, int cantidadEscanios, string nombreEscanios, string token)
        {
            var listaEscanios = new List<Pe04_Escanio>();
            for (int i = 1; i <= cantidadEscanios; i++)
            {
                listaEscanios.Add(mapearDtoAEntidad(new DtoEscanio()
                {
                    nombreEscanio = $"{nombreEscanios} {i}",
                    eleccionId = eleccionId,
                }, token));
            }
            CrearLista(listaEscanios);
        }

        public void ActualizarEscaniosEleccionPluripersonal(long eleccionId, int cantidadEscanios, string nombreEscanios, string token)
        {
            
            var escaniosEleccion = _escanioRepository.Get<Pe04_Escanio>(x => x.EleccionId == eleccionId)?.OrderBy(x => x.FechaCreacion);

            if (escaniosEleccion != null)
            {
                var usuario = _sesionService.ObtenerUsuarioPorToken(token)?.NombreUsuario;
                if (escaniosEleccion.Count() > cantidadEscanios)
                {
                    int i = 1;
                    foreach (var escanio in escaniosEleccion)
                    {
                        escanio.NombreEscanio = $"{nombreEscanios} {i}";
                        if (i > cantidadEscanios)
                            escanio.Estado = Auditoria.EstadoInactivo;
                        else
                            escanio.Estado = Auditoria.EstadoActivo;
                        escanio.UsuarioModificacion = usuario;
                        i++;
                    }
                    ActualizarLista(escaniosEleccion.ToList());
                }
                else if (escaniosEleccion.Count() == cantidadEscanios)
                {
                    int i = 1;
                    foreach (var escanio in escaniosEleccion)
                    {
                       escanio.NombreEscanio = $"{nombreEscanios} {i}";
                        escanio.Estado = Auditoria.EstadoActivo;
                        escanio.UsuarioModificacion = usuario;
                        i++;
                    }
                    ActualizarLista(escaniosEleccion.ToList());
                }
                else if (escaniosEleccion.Count() < cantidadEscanios)
                {
                    int i = 1;
                    foreach (var escanio in escaniosEleccion)
                    {
                        escanio.NombreEscanio = $"{nombreEscanios} {i}";
                        escanio.Estado = Auditoria.EstadoActivo;
                        escanio.UsuarioModificacion = usuario;
                        i++;
                    }
                    ActualizarLista(escaniosEleccion.ToList());

                    var listaEscanios = new List<Pe04_Escanio>();
                    for (int j = i; j <= cantidadEscanios; j++)
                    {
                        listaEscanios.Add(mapearDtoAEntidad(new DtoEscanio()
                        {
                            nombreEscanio = $"{nombreEscanios} {j}",
                            eleccionId = eleccionId,
                        }, token));
                    }
                    CrearLista(listaEscanios);
                }
               
            }
        }


        public DtoApiResponseMessage ObtenerEscanios(string nombre, string estado)
        {
            var escanios = MapearListaEntidadADtoEscanio(_escanioRepository.FiltrarEscanios(new Pe04_EscanioCondicional().FiltrarEscanios(nombre, estado)));
            if (escanios != null)
                return _apiResponseMessage.CrearDtoApiResponseMessage(escanios, "VE_PEL_ESC_005");
            return _apiResponseMessage.CrearDtoApiResponseMessage(null, "VE_PEL_ESC_006");
        }



        public void HabilitarEscanio(DtoEscanio dto)
        {
            var Escanio = ObtenerEscanioId(dto.escanioId);
            Escanio.Estado = "ACTIVO";
            Actualizar(Escanio);
        }

        public void InhabilitarEscanio(DtoEscanio dto)
        {
            var Escanio = ObtenerEscanioId(dto.escanioId);
            Escanio.Estado = "INACTIVO";
            Actualizar(Escanio);
        }

        public DtoApiResponseMessage ObtenerEscaniosPorProcesoElectoralId(long procesoElectoralId)
        {
            var spec = new Pe04_EscanioCondicional().FiltrarEscaniosMedianteProcesoElectoralId(procesoElectoralId);
            var dtoMapeado = MapearListaEntidadADtoEscanio(_escanioRepository.FiltrarEscanios(spec));
           // var eleccion = _eleccionRepository.GetById<Pe02_Eleccion>(eleccionId);
            //var escanios = _escanioRepository.Get<Pe04_Escanio>(x => x.Eleccion.ProcesosElectorales.Any(x => x.Id == procesoElectoralId));
            //var dtoMapeado = MapearListaEntidadADtoEscanio(escanios);

            return dtoMapeado != null && dtoMapeado.Count() > 0 ? _apiResponseMessage.CrearDtoApiResponseMessage(dtoMapeado, "VE_PEL_ESC_005") :
                _apiResponseMessage.CrearDtoApiResponseMessage(null, "VE_PEL_ESC_006");
        }

        public DtoApiResponseMessage ObtenerEscaniosPorEleccionId(long eleccionId)
        {
            var eleccion = _eleccionRepository.GetById<Pe02_Eleccion>(eleccionId);
            var escanios = _escanioRepository.Get<Pe04_Escanio>(x => x.EleccionId == eleccionId);
            if (eleccion.TipoEleccion?.NombreTipoEleccion?.Equals(Enumeration.ObtenerDescripcion(TipoEleccion.PLURIPERSONAL)) ?? false)
            {
                var dtoMapeado = MapearListaEntidadPluripersonal(escanios);
                return dtoMapeado != null && dtoMapeado?.Count() > 0 ? _apiResponseMessage.CrearDtoApiResponseMessage(dtoMapeado, "VE_PEL_ESC_005") :
                   _apiResponseMessage.CrearDtoApiResponseMessage(null, "VE_PEL_ESC_006");

            }
            else
            {
                var dtoMapeado = MapearListaEntidadADtoEscanio(escanios);
                return dtoMapeado != null && dtoMapeado?.Count() > 0 ? _apiResponseMessage.CrearDtoApiResponseMessage(dtoMapeado, "VE_PEL_ESC_005") :
                   _apiResponseMessage.CrearDtoApiResponseMessage(null, "VE_PEL_ESC_006");
            }

        }

      
        public void EliminarEscaniosEleccionId(long eleccionId)
        {
            var escanios = _escanioRepository.Get<Pe04_Escanio>(x => x.EleccionId == eleccionId);
            foreach (var escanio in escanios)
                _escanioRepository.Delete<Pe04_Escanio>(escanio.Id);
            _escanioRepository.Save();
        }


        #endregion

        #region Métodos Privados
        Pe04_Escanio ObtenerEscanioId(long id)
        => _escanioRepository.GetById<Pe04_Escanio>(id);

        Pe04_Escanio mapearDtoAEntidad(DtoEscanio dto, string token)
            => new Pe04_Escanio()
            {
                NombreEscanio = dto.nombreEscanio,
                EleccionId = dto.eleccionId,
                Estado = Auditoria.EstadoActivo,
                UsuarioCreacion = _sesionService.ObtenerUsuarioPorToken(token)?.NombreUsuario,
                FechaCreacion = DateTime.Now,
            };

        IEnumerable<DtoEscanio> mapearEntidadADto(Pe04_Escanio Escanio)
        {
            DtoEscanio dto = new DtoEscanio();
            dto.escanioId = Escanio.Id; ;
            dto.nombreEscanio = Escanio.NombreEscanio;
            dto.eleccionId = Escanio.EleccionId;
            dto.usuarioCreacion = Escanio.UsuarioCreacion;
            dto.usuarioModificacion = Escanio.UsuarioModificacion;
            dto.estado = Escanio.Estado;

            List<DtoEscanio> lista = new List<DtoEscanio>();
            lista.Add(dto);
            return lista;
        }

        IEnumerable<DtoEscanio> MapearListaEntidadPluripersonal(IEnumerable<Pe04_Escanio> escanio)
        {
            return escanio?.GroupBy(x => new { x.Eleccion.EtiquetaEscanios }).Select(y => new DtoEscanio()
            {
                nombreEscanio = y.Key.EtiquetaEscanios,
                nombreEleccion = y.FirstOrDefault()?.Eleccion.NombreEleccion,
                estado = y.FirstOrDefault()?.Estado
            });
        }

        IEnumerable<DtoEscanio> MapearListaEntidadADtoEscanio(IEnumerable<Pe04_Escanio> escanio)
        {
            return escanio == null ? null : escanio.Select(x => new DtoEscanio()
            {
                escanioId = x.Id,
                nombreEscanio = x.NombreEscanio,
                eleccionId = x.EleccionId,
                nombreEleccion = x.Eleccion.NombreEleccion,
                usuarioCreacion = x.UsuarioCreacion,
                usuarioModificacion = x.UsuarioModificacion,
                estado = x.Estado
            });
        }


        void Crear(Pe04_Escanio Escanio)
        {
            _escanioRepository.Create<Pe04_Escanio>(Escanio);
            _escanioRepository.Save();
        }
        void CrearLista(IEnumerable<Pe04_Escanio> listaEscanios)
        {
            _escanioRepository.CreateList<Pe04_Escanio>(listaEscanios);
            _escanioRepository.Save();
        }

        void Actualizar(Pe04_Escanio Escanio)
        {
            _escanioRepository.Update<Pe04_Escanio>(Escanio);
            _escanioRepository.Save();
        }
        void ActualizarLista(List<Pe04_Escanio> escanios)
        {
            foreach(var escanio in escanios)
            _escanioRepository.Update <Pe04_Escanio>(escanio);
            _escanioRepository.Save();
        }

        #endregion


    }
}
