﻿using EcVotoElectronico.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using VotoElectronico.Entidades.Pk.PkProcesoElectoral;
using VotoElectronico.Generico;
using VotoElectronico.Generico.Configs;
using VotoElectronico.Generico.Enumeraciones;
using VotoElectronico.Generico.Propiedades;
using VotoElectronico.LogicaCondicional;
using VotoElectronico.Util;

namespace VotoElectronico.LogicaNegocio.Servicios
{
    public class ProcesoElectoralService : IProcesoElectoralService
    {

        private readonly IProcesoElectoralRepository _procesoElectoralRepository;
        private readonly IApiResponseMessage _apiResponseMessage;
        private readonly IListaService _listaService;
        private readonly IPadronVotacionService _padronVotacionService;
        private readonly ISesionService _sesionService;
        private readonly IRolService _rolService;

        public ProcesoElectoralService(IProcesoElectoralRepository ProcesoElectoralRepository, IApiResponseMessage utilitarios, IListaService listaService, IPadronVotacionService padronVotacionService, ISesionService sesionService, IRolService rolService)
        {
            _procesoElectoralRepository = ProcesoElectoralRepository;
            _padronVotacionService = padronVotacionService;
            _apiResponseMessage = utilitarios;
            _listaService = listaService;
            _sesionService = sesionService;
            _rolService = rolService;
        }

        #region Métodos Publicos

        public DtoApiResponseMessage ObtenerProcesoElectoralesPorNombre(string nombreProcesoElectoral, string estado, int anio, bool finalizados = false)
        {
            var spec = new Pe01_ProcesoElectoralCondicional().FiltrarProcesoElectoralPorNombre(nombreProcesoElectoral, estado, anio, finalizados);
            var dtoMapeado = MapearListaEntidadADtoProcesoElectoral(_procesoElectoralRepository.FiltrarProcesoElectoralEspecificacion(spec));

            if ((dtoMapeado?.Count() ?? 0) != 0)
            {
                return _apiResponseMessage.CrearDtoApiResponseMessage(dtoMapeado, "VE_PEL_PEL_005");
            }
            else
            {
                return _apiResponseMessage.CrearDtoApiResponseMessage(null, "VE_PEL_PEL_006");
            }

        }

        public DtoApiResponseMessage ObtenerProcesoElectoralesFinalizadosByParams(string token, string nombreProcesoElectoral,  long anio)
        {
            var spec = new Pe01_ProcesoElectoralCondicional().FiltrarEleccionesFinalizadasByToken(token, nombreProcesoElectoral, anio);
            var dtoMapeado = MapearListaEntidadADtoProcesoElectoral(_procesoElectoralRepository.FiltrarProcesoElectoralEspecificacion(spec));

            if ((dtoMapeado?.Count() ?? 0) != 0)
            {
                return _apiResponseMessage.CrearDtoApiResponseMessage(dtoMapeado, "VE_PEL_PEL_005");
            }
            else
            {
                return _apiResponseMessage.CrearDtoApiResponseMessage(null, "VE_PEL_PEL_006");
            }

        }

        public DtoApiResponseMessage ObtenerProcesoElectoralesVigentesByToken(string token)
        {
            var spec = new Pe01_ProcesoElectoralCondicional().FiltrarEleccionesVigentesByToken(token);
            var dtoMapeado = MapearListaEntidadADtoProcesoElectoral(_procesoElectoralRepository.FiltrarProcesoElectoralEspecificacion(spec),token);

            if ((dtoMapeado?.Count() ?? 0) != 0)
            {
                return _apiResponseMessage.CrearDtoApiResponseMessage(dtoMapeado, "VE_PEL_PEL_005");
            }
            else
            {
                return _apiResponseMessage.CrearDtoApiResponseMessage(null, "VE_PEL_PEL_006");
            }

        }
        public DtoApiResponseMessage ObtenerTodosProcesosElectoralesActivos()
        {
            var now = DateTime.Now;
            var dtoMapeado = MapearListaEntidadADtoProcesoElectoral(_procesoElectoralRepository.Get<Pe01_ProcesoElectoral>(x => x.Estado.Equals(Auditoria.EstadoActivo) && x.FechaFin < now)?.OrderByDescending(y => y.FechaCreacion));
            if ((dtoMapeado?.Count() ?? 0) != 0)
                return _apiResponseMessage.CrearDtoApiResponseMessage(dtoMapeado, "VE_PEL_PEL_005");
            return _apiResponseMessage.CrearDtoApiResponseMessage(null, "VE_PEL_PEL_006");

        }




        public DtoApiResponseMessage ObtenerProcesoElectoralMedianteId(DtoProcesoElectoral dto)
        {
            var ProcesoElectoral = ObtenerProcesoElectoralId(dto.Id);

            if (ProcesoElectoral != null)
            {
                var dtoMapeado = mapearEntidadADto(ProcesoElectoral);
                return _apiResponseMessage.CrearDtoApiResponseMessage(dtoMapeado, "VE_PEL_PEL_004");
            }
            else
            {
                return _apiResponseMessage.CrearDtoApiResponseMessage(null, "VE_PEL_PEL_007");
            }
        }

        public DtoApiResponseMessage CrearProcesoElectoral(DtoProcesoElectoral dto, string token)
        {
            var ProcesoElectoral = mapearDtoAEntidad(dto, token);
            Crear(ProcesoElectoral);
            var dtoMapeado = mapearEntidadADto(ProcesoElectoral);
            return _apiResponseMessage.CrearDtoApiResponseMessage(dtoMapeado, "VE_PEL_PEL_001");
        }

        public DtoApiResponseMessage ActualizarProcesoElectoral(DtoProcesoElectoral dto, string token)
        {
            var ProcesoElectoral = ObtenerProcesoElectoralId(dto.Id);

            if (ProcesoElectoral != null)
            {
                ProcesoElectoral.NombreProcesoElectoral = dto.nombreProcesoElectoral;
                ProcesoElectoral.FechaInicio = dto.fechaInicio;
                ProcesoElectoral.FechaFin = dto.fechaFin;
                ProcesoElectoral.EleccionId = dto.eleccionId;
                ProcesoElectoral.Estado = dto.estado;
                ProcesoElectoral.UsuarioModificacion = _sesionService.ObtenerUsuarioPorToken(token)?.NombreUsuario;
                ProcesoElectoral.FechaModificacion = DateTime.Now;
                Actualizar(ProcesoElectoral);
                var dtoMapeado = mapearEntidadADto(ProcesoElectoral);
                return _apiResponseMessage.CrearDtoApiResponseMessage(dtoMapeado, "VE_PEL_PEL_002");
            }
            else
            {
                return _apiResponseMessage.CrearDtoApiResponseMessage(null, "VE_PEL_PEL_007");
            }
        }


        public DtoApiResponseMessage EliminarProcesoElectoral(DtoProcesoElectoral dto)
        {
            var ProcesoElectoral = ObtenerProcesoElectoralId(dto.Id);
            if (ProcesoElectoral != null)
            {
                using var transaccion = new TransactionScope();
                try
                {
                    EliminarPadroProcesoElectoral(ProcesoElectoral.Id);
                    EliminarListasProcesoElectoral(ProcesoElectoral.Id);
                    Eliminar(ProcesoElectoral.Id);
                    transaccion.Complete();

                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    transaccion.Dispose();
                }
                var dtoMapeado = mapearEntidadADto(ProcesoElectoral);
                return _apiResponseMessage.CrearDtoApiResponseMessage(dtoMapeado, "VE_PEL_PEL_003");
            }
            else
            {
                return _apiResponseMessage.CrearDtoApiResponseMessage(null, "VE_PEL_PEL_007");
            }

        }

        void EliminarListasProcesoElectoral(long procesoId)
        => _listaService.EliminarListasProceso(procesoId);

        void EliminarPadroProcesoElectoral(long procesoElectoralId)
            => _padronVotacionService.EliminarPadronesPorProceso(procesoElectoralId);


        public void HabilitarProcesoElectoral(DtoProcesoElectoral dto)
        {
            var ProcesoElectoral = ObtenerProcesoElectoralId(dto.Id);
            ProcesoElectoral.Estado = "ACTIVO";
            Actualizar(ProcesoElectoral);
        }

        public void InhabilitarProcesoElectoral(DtoProcesoElectoral dto)
        {
            var ProcesoElectoral = ObtenerProcesoElectoralId(dto.Id);
            ProcesoElectoral.Estado = "INACTIVO";
            Actualizar(ProcesoElectoral);
        }


        public bool procesoElectoralEditable(long procesoElectoralId)
        {
            var proceso = _procesoElectoralRepository.GetById<Pe01_ProcesoElectoral>(procesoElectoralId);
            return proceso != null && proceso.Estado.Equals(Auditoria.EstadoActivo) && proceso.FechaInicio >= DateTime.Now && proceso.FechaFin >= DateTime.Now;
        }




        #endregion

        #region Métodos Privados
        Pe01_ProcesoElectoral ObtenerProcesoElectoralId(long id)
        => _procesoElectoralRepository.GetById<Pe01_ProcesoElectoral>(id);

        Pe01_ProcesoElectoral mapearDtoAEntidad(DtoProcesoElectoral dto, string token)
            => new Pe01_ProcesoElectoral()
            {
                NombreProcesoElectoral = dto.nombreProcesoElectoral,
                FechaInicio = dto.fechaInicio,
                FechaFin = dto.fechaFin,
                EleccionId = dto.eleccionId,
                Estado = Auditoria.EstadoActivo,
                UsuarioCreacion = _sesionService.ObtenerUsuarioPorToken(token)?.NombreUsuario,
                FechaCreacion = DateTime.Now,
            };

        IEnumerable<DtoProcesoElectoral> mapearEntidadADto(Pe01_ProcesoElectoral ProcesoElectoral)
        {
            DtoProcesoElectoral dto = new DtoProcesoElectoral();
            var now = DateTime.Now;
            dto.Id = ProcesoElectoral?.Id ?? 0;
            dto.nombreProcesoElectoral = ProcesoElectoral?.NombreProcesoElectoral;
            dto.fechaInicio = ProcesoElectoral?.FechaInicio ?? DateTime.MinValue;
            dto.fechaFin = ProcesoElectoral?.FechaFin ?? DateTime.MinValue;
            dto.eleccionId = ProcesoElectoral?.EleccionId ?? 0;
            dto.usuarioCreacion = ProcesoElectoral?.UsuarioCreacion;
            dto.usuarioModificacion = ProcesoElectoral?.UsuarioModificacion;
            dto.estado = ProcesoElectoral?.Estado;
            dto.nombreEleccion = ProcesoElectoral?.Eleccion?.NombreEleccion;
            dto.aplicaAlterno = ProcesoElectoral?.Eleccion?.AplicaCandidatoAlterno ?? false;
            dto.aplicaAlicuota = ProcesoElectoral?.Eleccion?.AplicaAlicuotaCargo ?? false;
            dto.tipoEleccion = ProcesoElectoral?.Eleccion?.TipoEleccion?.NombreTipoEleccion;
            dto.estadoProceso = ObtenerEstadoProceso(now, ProcesoElectoral);
            dto.color = ObtenerColorProceso(now, ProcesoElectoral);
                
            List<DtoProcesoElectoral> lista = new List<DtoProcesoElectoral>() { dto };
            return lista;
        }

        IEnumerable<DtoProcesoElectoral> MapearListaEntidadADtoProcesoElectoral(IEnumerable<Pe01_ProcesoElectoral> ProcesoElectoral, string token = "")
        {
            var now = DateTime.Now;
            return ProcesoElectoral?.Select(x => new DtoProcesoElectoral()
            {
                Id = x.Id,
                nombreProcesoElectoral = x.NombreProcesoElectoral,
                fechaInicio = x.FechaInicio,
                fechaFin = x.FechaFin,
                eleccionId = x.EleccionId,
                nombreEleccion = x.Eleccion.NombreEleccion,
                usuarioCreacion = x.UsuarioCreacion,
                usuarioModificacion = x.UsuarioModificacion,
                estado = x.Estado,
                tipoEleccion = x.Eleccion.TipoEleccion.NombreTipoEleccion,
                votoRealizado = string.IsNullOrEmpty(token) ? x.PadronesVotacion.Any(padron => padron.VotoRealizado): x.PadronesVotacion.Any(padron => padron.VotoRealizado && (padron.Usuario.Token?.Equals(token)?? false)),
                editable = procesoElectoralEditable(x.Id),
                aplicaAlterno = x.Eleccion?.AplicaCandidatoAlterno ?? false,
                estadoProceso = ObtenerEstadoProceso(now,x),
                color = ObtenerColorProceso(now,x),
                

            });
        }

        string ObtenerEstadoProceso(DateTime now, Pe01_ProcesoElectoral proceso)
        {
            if (proceso == null)
                return string.Empty;
            if (now < proceso.FechaInicio && now < proceso.FechaFin)
                return Enumeration.ObtenerDescripcion(EstadosProceso.NoIniciado);
            if(now >= proceso.FechaInicio && now <= proceso.FechaFin)
                return Enumeration.ObtenerDescripcion(EstadosProceso.EnProceso);
            if (now > proceso.FechaFin && now > proceso.FechaInicio )
                return Enumeration.ObtenerDescripcion(EstadosProceso.Finalizado);
            return string.Empty;
        }
        string ObtenerColorProceso(DateTime now, Pe01_ProcesoElectoral proceso)
        {
            if (proceso == null)
                return string.Empty;
            if (now < proceso.FechaInicio && now < proceso.FechaFin)
                return Enumeration.ObtenerDescripcion(ColoresProceso.NoIniciado);
            if (now >= proceso.FechaInicio && now <= proceso.FechaFin)
                return Enumeration.ObtenerDescripcion(ColoresProceso.EnProceso);
            if (now > proceso.FechaFin && now > proceso.FechaInicio)
                return Enumeration.ObtenerDescripcion(ColoresProceso.Finalizado);
            return string.Empty;
        }

        void Crear(Pe01_ProcesoElectoral ProcesoElectoral)
        {
            _procesoElectoralRepository.Create<Pe01_ProcesoElectoral>(ProcesoElectoral);
            _procesoElectoralRepository.Save();
        }

        void Actualizar(Pe01_ProcesoElectoral ProcesoElectoral)
        {
            _procesoElectoralRepository.Update<Pe01_ProcesoElectoral>(ProcesoElectoral);
            _procesoElectoralRepository.Save();
        }

        void Eliminar(long idProcesoElectoral)
        {
            _procesoElectoralRepository.Delete<Pe01_ProcesoElectoral>(idProcesoElectoral);
            _procesoElectoralRepository.Save();
        }
        #endregion


    }
}
