﻿using EcVotoElectronico.Repositorios;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Transactions;
using VotoElectronico.Entidades.Pk.PkProcesoElectoral;
using VotoElectronico.Entidades.Pk.PkSeguridad;
using VotoElectronico.Generico;
using VotoElectronico.Generico.Propiedades;
using VotoElectronico.LogicaCondicional;
using VotoElectronico.LogicaNegocio.Servicios.GoogleDrive;
using VotoElectronico.ReglasNegocio.Validators;
using VotoElectronico.Util;

namespace VotoElectronico.LogicaNegocio.Servicios
{
    public class CandidatoService : ICandidatoService
    {

        private readonly ICandidatoRepository _candidatoRepository;
        private readonly IPersonaRepository _personaRepository;
        private readonly IApiResponseMessage _utilitarios;
        private readonly IGoogleDriveService _googleDriveService;
        private readonly IListaRepository _listaRepository;
        private readonly ISesionService _sesionService;
        private readonly ICandidatoAlternoRepository _candidatoAlternoRepository;
        private readonly ICandidatoValidator _candidatoValidator;
        private readonly string pathCandidatos = ConfigurationManager.AppSettings["path-candidatos"];


        public CandidatoService(
            ICandidatoRepository candidatoRepository, 
            IApiResponseMessage apiResponseMessage,
            IGoogleDriveService googleDriveService,
            IPersonaRepository personaRepository,
            IListaRepository listaRepository,
            ISesionService sesionService,
            ICandidatoAlternoRepository candidatoAlternoRepository,
            ICandidatoValidator candidatoValidato
            )
        {
            _candidatoRepository = candidatoRepository;
            _utilitarios = apiResponseMessage;
            _googleDriveService = googleDriveService;
            _personaRepository = personaRepository;
            _listaRepository = listaRepository;
            _sesionService = sesionService;
            _candidatoAlternoRepository = candidatoAlternoRepository;
            _candidatoValidator = candidatoValidato;
        }

        #region Métodos Publicos
        public void EliminarCandidatosLista(long listaId)
        =>EliminarListaCandidatos(_candidatoRepository.Get<Pe06_Candidato>(x => x.ListaId == listaId));
        
        
        public DtoApiResponseMessage ObtenerCandidatoMedianteParametros(long parametro1, string parametro2, string parametro3)
        {
            var spec = new Pe06_CandidatoCondicional().FiltrarCandidatosPorParametros(parametro1, parametro2, parametro3);
            var dtoMapeado = MapearCandidatoEntidadADtoCandidato(_candidatoRepository.FiltrarCandidatoMedianteParametros(spec));

            if (dtoMapeado.Count() != 0)
            {
                return _utilitarios.CrearDtoApiResponseMessage(dtoMapeado, "VE_PEL_CAN_005");
            }
            else
            {
                return _utilitarios.CrearDtoApiResponseMessage(null, "VE_PEL_CAN_006");
            }

        }



        public DtoApiResponseMessage ObtenerCandidatoMedianteListaId(long listaId, string estado)
        {
            var spec = new Pe06_CandidatoCondicional().FiltrarCandidatosPorListaId(listaId, estado);
            var dtoMapeado = MapearCandidatoEntidadADtoCandidato(_candidatoRepository.FiltrarCandidatoMedianteListaID(spec));

            if (dtoMapeado.Count() != 0)
            {
                return _utilitarios.CrearDtoApiResponseMessage(dtoMapeado, "VE_PEL_CAN_005");
            }
            else
            {
                return _utilitarios.CrearDtoApiResponseMessage(null, "VE_PEL_CAN_006");
            }

        }

        public DtoApiResponseMessage ObtenerCandidatoMedianteId(DtoCandidato dto)
        {
            var candidato = ObtenerCandidatoId(dto.Id);

            if (candidato != null)
            {
                var dtoMapeado = mapearEntidadADto(candidato);
                return _utilitarios.CrearDtoApiResponseMessage(dtoMapeado, "VE_PEL_CAN_004");
            }
            else
            {
                return _utilitarios.CrearDtoApiResponseMessage(null, "VE_PEL_CAN_007");
            }
        }

        public DtoApiResponseMessage CrearCandidato(DtoCandidato dto, string token)
        {
            var candidato = new Pe06_Candidato();
            using var transaction = new TransactionScope();
            try {
                       candidato = mapearDtoAEntidad(dto, token);
                var proceso = _listaRepository.GetById<Pe05_Lista>(dto.listaId)?.ProcesoElectoral;
                if (proceso == null)
                    throw new Exception("Proceso no Existe");
                if (proceso.Eleccion?.AplicaCandidatoAlterno ?? false)
                {
                    if (dto.personaId == dto.personaIdAlterno)
                        throw new Exception("Una persona no puede ser alterno y candidato a la vez");

                           candidato.Alterno = mapearDtoAEntidadAlterno(dto, token);
                }

                _candidatoValidator.ValidarCandidato(candidato);

                if (dto.fotoObjeto != null)
                    candidato.Foto = _googleDriveService.UploadBase64(dto.fotoObjeto.base64, dto.fotoObjeto.tipo, dto.fotoObjeto.extension, pathCandidatos);

                if (proceso.Eleccion?.AplicaCandidatoAlterno ?? false)
                {
                    if (dto.fotoObjetoAlterno != null && candidato.Alterno != null)
                        candidato.Alterno.Foto = _googleDriveService.UploadBase64(dto.fotoObjetoAlterno.base64, dto.fotoObjetoAlterno.tipo, dto.fotoObjetoAlterno.extension, pathCandidatos);
                }

                Crear(candidato);
                transaction.Complete();
            }
            catch (Exception e) {
                throw e;
            }
            finally {
                transaction.Dispose();
            }
            var dtoMapeado = mapearEntidadADto(candidato);
            return _utilitarios.CrearDtoApiResponseMessage(dtoMapeado, "VE_PEL_CAN_001");
        }

        public DtoApiResponseMessage ActualizarCandidato(DtoCandidato dto)
        {
            var candidato = ObtenerCandidatoId(dto.Id);

            if (candidato != null)
            {
                candidato.PersonaId = dto.personaId;
                candidato.EscanioId = dto.escanioId;
                candidato.ListaId = dto.listaId;
                candidato.UsuarioModificacion = dto.usuarioModificacion;
                candidato.FechaModificacion = DateTime.Now;
                candidato.Estado = dto.estado;
                Actualizar(candidato);
                var dtoMapeado = mapearEntidadADto(candidato);
                return _utilitarios.CrearDtoApiResponseMessage(dtoMapeado, "VE_PEL_CAN_002");
            }
            else
            {
                return _utilitarios.CrearDtoApiResponseMessage(null, "VE_PEL_CAN_007");
            }
        }


        public DtoApiResponseMessage EliminarCandidato(DtoCandidato dto)
        {
            var Candidato = ObtenerCandidatoId(dto.Id);
            if (Candidato != null)
            {  
                using var transaction = new TransactionScope();
                try {
                    if (Candidato.Alterno != null)
                        EliminarAlterno(Candidato.Alterno);
                    Eliminar(Candidato.Id);
                    transaction.Complete();
                }
                catch (Exception e)
                {
                    throw e;
                }
                finally {
                    transaction.Dispose();
                }
                
                var dtoMapeado = mapearEntidadADto(Candidato);
                return _utilitarios.CrearDtoApiResponseMessage(dtoMapeado, "VE_PEL_CAN_003");
            }
            else
            {
                return _utilitarios.CrearDtoApiResponseMessage(null, "VE_PEL_CAN_007");
            }

        }

        public void HabilitarCandidato(DtoCandidato dto)
        {
            var Candidato = ObtenerCandidatoId(dto.Id);
            Candidato.Estado = "ACTIVO";
            Actualizar(Candidato);
        }

        public void InhabilitarCandidato(DtoCandidato dto)
        {
            var Candidato = ObtenerCandidatoId(dto.Id);
            Candidato.Estado = "INACTIVO";
            Actualizar(Candidato);
        }





        #endregion

        #region Métodos Privados
        Pe06_Candidato ObtenerCandidatoId(long id)
        => _candidatoRepository.GetById<Pe06_Candidato>(id);

        Pe06_Candidato mapearDtoAEntidad(DtoCandidato dto, string token)
            => new Pe06_Candidato()
            {
                PersonaId = dto.personaId,
                EscanioId = dto.escanioId,
                ListaId = dto.listaId,
                Estado = Auditoria.EstadoActivo,
                UsuarioCreacion = _sesionService.ObtenerUsuarioPorToken(token)?.NombreUsuario,
                FechaCreacion = DateTime.Now,
                Foto = dto.fotoUrl,
            };

        Pe07_AlternoCandidato mapearDtoAEntidadAlterno(DtoCandidato dto, string token)
        => new Pe07_AlternoCandidato()
        {
            PersonaId = dto.personaIdAlterno,
            Estado = Auditoria.EstadoActivo,
            UsuarioCreacion = _sesionService.ObtenerUsuarioPorToken(token)?.NombreUsuario,
            FechaCreacion = DateTime.Now,
            Foto = dto.fotoUrlAlterno,
        };

        IEnumerable<DtoCandidato> mapearEntidadADto(Pe06_Candidato candidato)
        {
            DtoCandidato dto = new DtoCandidato();
            dto.Id = candidato.Id; ;
            dto.personaId = candidato.PersonaId;
            dto.escanioId = candidato.EscanioId;
            dto.listaId = candidato.ListaId;
            dto.usuarioCreacion = candidato.UsuarioCreacion;
            dto.usuarioModificacion = candidato.UsuarioModificacion;
            dto.estado = candidato.Estado;
            dto.fotoUrl = candidato.Foto;

            List<DtoCandidato> Candidato = new List<DtoCandidato>();
            Candidato.Add(dto);
            return Candidato;
        }

        IEnumerable<DtoCandidato> MapearCandidatoEntidadADtoCandidato(IEnumerable<Pe06_Candidato> Candidato)
        {
            //var persona = _personaRepository.GetOne<Sg02_Persona>(persona => persona.Id.Equals(usuario.PersonaId));
            //var dtoPersona = mapearEntidadPersonaADto(persona);

            return Candidato.Select(x => new DtoCandidato()
            {
                Id = x.Id,
                personaId = x.PersonaId,
                nombreCandidato = ObtenerNombresPersona(x.Persona),
                nombreCandidatoAlterno = ObtenerNombresPersona(x.Alterno?.Persona),
                identificacion = x.Persona.Identificacion,
                procesoElectoralId = x.Lista.ProcesoElectoralId,
                nombreProcesoElectoral = x.Lista.ProcesoElectoral.NombreProcesoElectoral,
                escanioId = x.EscanioId,
                nombreEscanio = x.Escanio.NombreEscanio,
                listaId = x.ListaId,
                nombreLista = x.Lista.NombreLista,
                usuarioCreacion = x.UsuarioCreacion,
                usuarioModificacion = x.UsuarioModificacion,
                estado = x.Estado,
                fotoUrl = string.IsNullOrEmpty(x.Foto) ? null : $"{CtEstaticas.StrGoogleDrive}{x.Foto}",
                fotoUrlAlterno = string.IsNullOrEmpty(x.Alterno?.Foto) ? null : $"{CtEstaticas.StrGoogleDrive}{x.Alterno?.Foto}",
                objetoPersona = mapearEntidadPersonaADto(x.Persona),
                objetoPersonaAlterno = mapearEntidadPersonaADto(x.Alterno?.Persona)

            });
        }

        string ObtenerNombresPersona(Sg02_Persona persona)
            => persona is null ? null : persona.NombreUno + " " + persona.ApellidoUno;
        
     

        DtoPersona mapearEntidadPersonaADto(Sg02_Persona Persona)
            => Persona == null ? null :  new DtoPersona()
            {
                Id = Persona.Id,
                nombreUno = Persona.NombreUno,
                nombreDos = Persona.NombreDos,
                apellidoUno = Persona.ApellidoUno,
                apellidoDos = Persona.ApellidoDos,
                identificacion = Persona.Identificacion,
                telefonoUno = Persona.Telefono,
                telefonoDos = Persona.Telefono2,
                estado = Persona.Estado,
                email = Persona.Email,
            };
        

        void Crear(Pe06_Candidato Candidato)
        {
            _candidatoRepository.Create<Pe06_Candidato>(Candidato);
            _candidatoRepository.Save();
        }

        void Actualizar(Pe06_Candidato Candidato)
        {
            _candidatoRepository.Update<Pe06_Candidato>(Candidato);
            _candidatoRepository.Save();
        }

        void Eliminar(long idCandidato)
        {
            _candidatoRepository.Delete<Pe06_Candidato>(idCandidato);
            _candidatoRepository.Save();
        }

        void EliminarAlterno(Pe07_AlternoCandidato alternoCandidato)
        {
            _candidatoAlternoRepository.Delete<Pe07_AlternoCandidato>(alternoCandidato.Id);
            _candidatoRepository.Save();
        }

        void EliminarListaCandidatos(IEnumerable<Pe06_Candidato> candidatos)
        {
            if (candidatos != null)
            {
                foreach (var candidato in candidatos)
                {
                    if (candidato.Alterno != null)
                        EliminarAlterno(candidato.Alterno);
                    _candidatoRepository.Delete<Pe06_Candidato>(candidato.Id);
                }
                  
                _candidatoRepository.Save();
            }
        }
        #endregion


    }
}
