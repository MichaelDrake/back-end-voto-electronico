﻿using EcVotoElectronico.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using VotoElectronico.Entidades.Pk.PkProcesoElectoral;
using VotoElectronico.Generico;
using VotoElectronico.Generico.Configs;
using VotoElectronico.Generico.Enumeraciones;
using VotoElectronico.Generico.Propiedades;
using VotoElectronico.LogicaCondicional;
using VotoElectronico.ReglasNegocio.Validators;
using VotoElectronico.Util;

namespace VotoElectronico.LogicaNegocio.Servicios
{
    public class EleccionService : IEleccionService
    {

        private readonly IEleccionRepository _eleccionRepository;
        private readonly IApiResponseMessage _apiResponseMessage;
        private readonly ISesionService _sesionService;
        private readonly IEscanioService _escanioService;
        private readonly ITipoEleccionRepository _tipoEleccionRepository;
        private readonly IEleccionValidator _eleccionValidator;


        public EleccionService(IEleccionRepository eleccionRepository, IApiResponseMessage utilitarios, ISesionService sesionService, IEscanioService escanioService, ITipoEleccionRepository tipoEleccionRepository, IEleccionValidator eleccionValidator)
        {
            _eleccionRepository = eleccionRepository;
            _sesionService = sesionService;
            _apiResponseMessage = utilitarios;
            _escanioService = escanioService;
            _tipoEleccionRepository = tipoEleccionRepository;
            _eleccionValidator = eleccionValidator;
        }

        #region Métodos Publicos

        public DtoApiResponseMessage ObtenerEleccionesPorNombre(string nombreEleccion, string estado)
        {
            var spec = new Pe02_EleccionCondicional().FiltrarEleccionPorNombre(nombreEleccion, estado);
            var dtoMapeado = MapearListaEntidadADtoEleccion(_eleccionRepository.FiltrarEleccionPorNombre(spec));

            if (dtoMapeado.Count() != 0)
            {
                return _apiResponseMessage.CrearDtoApiResponseMessage(dtoMapeado, "VE_PEL_ELC_005");
            }
            else
            {
                return _apiResponseMessage.CrearDtoApiResponseMessage(null, "VE_PEL_ELC_006");
            }

        }


        public DtoApiResponseMessage ObtenerEleccionMedianteId(DtoEleccion dto)
        {
            var eleccion = ObtenerEleccionId(dto.Id);

            if (eleccion != null)
            {
                var dtoMapeado = mapearEntidadADto(eleccion);
                return _apiResponseMessage.CrearDtoApiResponseMessage(dtoMapeado, "VE_PEL_ELC_004");
            }
            else
            {
                return _apiResponseMessage.CrearDtoApiResponseMessage(null, "VE_PEL_ELC_007");
            }
        }

        public DtoApiResponseMessage CrearEleccion(DtoEleccion dto, string token)
        {
            var eleccion = mapearDtoAEntidad(dto, token);
            var etiquetaPluricultural = Enumeration.ObtenerDescripcion(TipoEleccion.PLURIPERSONAL);
            var tipoEleccionPluripersonal = _tipoEleccionRepository.GetOneOrDefault<Pe03_TipoEleccion>(x => x.NombreTipoEleccion.Equals(etiquetaPluricultural));
            using var transaccion = new TransactionScope();
            try
            {
                Crear(eleccion);

                if (tipoEleccionPluripersonal.Id == eleccion.TipoEleccionId)
                {
                    if (string.IsNullOrEmpty(eleccion.EtiquetaEscanios))
                        throw new Exception("Etiqueta escanios obligatoria para una eleccion pluripersonal");
                    if (eleccion.CantidadEscanios == 0)
                        throw new Exception("Cantidad de escaños no puede ser cero para una  eleccion pluripersonal");
                    CrearEscanioCantidad(eleccion.Id, eleccion.CantidadEscanios, eleccion.EtiquetaEscanios, token);

                }

                transaccion.Complete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                transaccion.Dispose();
            }

            var dtoMapeado = mapearEntidadADto(eleccion);
            return _apiResponseMessage.CrearDtoApiResponseMessage(dtoMapeado, "VE_PEL_ELC_001");
        }

        public DtoApiResponseMessage ActualizarEleccion(DtoEleccion dto, string token)
        {
            var eleccion = ObtenerEleccionId(dto.Id);

            if (eleccion != null)
            {
                var etiquetaPluricultural = Enumeration.ObtenerDescripcion(TipoEleccion.PLURIPERSONAL);
                var tipoEleccionPluripersonal = _tipoEleccionRepository.GetOneOrDefault<Pe03_TipoEleccion>(x => x.NombreTipoEleccion.Equals(etiquetaPluricultural));

                eleccion.NombreEleccion = dto.nombreEleccion;
                eleccion.UsuarioModificacion = _sesionService.ObtenerUsuarioPorToken(token)?.NombreUsuario;
                eleccion.AplicaAlicuotaCargo = dto.aplicaAlicuota;
                eleccion.AplicaCandidatoAlterno = dto.aplicaAlterno;
                eleccion.FechaModificacion = DateTime.Now;

                if (tipoEleccionPluripersonal.Id == eleccion.TipoEleccionId)
                {

                    if (dto.CantidadEscanios == 0)
                        throw new Exception("Cantidad de escaños no puede ser cero para una  eleccion pluripersonal");

                    eleccion.EtiquetaEscanios = dto.EtiquetaEscanios;

                    if (dto.CantidadEscanios != eleccion.CantidadEscanios)
                    {
                        if (_eleccionValidator.ConfiguradoEnProceso(eleccion.Id))
                            throw new Exception("Elección Configurada en algún proceso, no se puede modificar");
                        eleccion.CantidadEscanios = dto.CantidadEscanios;
                    }
                    using var transaccion = new TransactionScope();
                    try
                    {
                        Actualizar(eleccion);
                        _escanioService.ActualizarEscaniosEleccionPluripersonal(eleccion.Id, eleccion.CantidadEscanios, eleccion.EtiquetaEscanios, token);
                        transaccion.Complete();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        transaccion.Dispose();
                    }
                    var dtoMapeado = mapearEntidadADto(eleccion);
                    return _apiResponseMessage.CrearDtoApiResponseMessage(dtoMapeado, "VE_PEL_ELC_002");

                }
                else
                {
                    Actualizar(eleccion);
                    var dtoMapeado = mapearEntidadADto(eleccion);
                    return _apiResponseMessage.CrearDtoApiResponseMessage(dtoMapeado, "VE_PEL_ELC_002");
                }
            }
            else
            {
                return _apiResponseMessage.CrearDtoApiResponseMessage(null, "VE_PEL_ELC_007");
            }
        }


        public DtoApiResponseMessage EliminarEleccion(DtoEleccion dto, string token)
        {

            var eleccion = ObtenerEleccionId(dto.Id);
            if (eleccion != null)
            {
                if (_eleccionValidator.ConfiguradoEnProceso(eleccion.Id))
                {
                    eleccion.Estado = Auditoria.EstadoInactivo;
                    eleccion.UsuarioModificacion = _sesionService.ObtenerUsuarioPorToken(token)?.NombreUsuario;
                    eleccion.FechaModificacion = DateTime.Now;
                    Actualizar(eleccion);
                }
                else {
                    EliminarEscanios(eleccion.Id);
                    Eliminar(eleccion.Id);
                }
                return _apiResponseMessage.CrearDtoApiResponseMessage(null, "VE_PEL_ELC_003");

            }
            else
            {
                return _apiResponseMessage.CrearDtoApiResponseMessage(null, "VE_PEL_ELC_007");
            }

        }

        public DtoApiResponseMessage ReactivarElecion(long idEscanio, string token)
        {
            var Escanio = ObtenerEleccionId(idEscanio);
            if (Escanio != null)
            {
                Escanio.Estado = Auditoria.EstadoActivo;
                Escanio.UsuarioModificacion = _sesionService.ObtenerUsuarioPorToken(token)?.NombreUsuario;
                Escanio.FechaModificacion = DateTime.Now;
                var escanio = mapearEntidadADto(Escanio);
                Actualizar(Escanio);
                return _apiResponseMessage.CrearDtoApiResponseMessage(escanio, "VE_GNR_UP_001");
            }
            else
            {
                return _apiResponseMessage.CrearDtoApiResponseMessage(null, "VE_PEL_ELC_007");
            }

        }

        public void HabilitarEleccion(DtoEleccion dto)
        {
            var Eleccion = ObtenerEleccionId(dto.Id);
            Eleccion.Estado = "ACTIVO";
            Actualizar(Eleccion);
        }

        public void InhabilitarEleccion(DtoEleccion dto)
        {
            var Eleccion = ObtenerEleccionId(dto.Id);
            Eleccion.Estado = "INACTIVO";
            Actualizar(Eleccion);
        }





        #endregion

        #region Métodos Privados
        void CrearEscanioCantidad(long eleccionId, int cantidadEscanios, string nombreEscanios, string token)
        {
            _escanioService.CrearEscaniosEleccionPluripersonal(eleccionId, cantidadEscanios, nombreEscanios, token);
        }


        Pe02_Eleccion ObtenerEleccionId(long id)
        => _eleccionRepository.GetById<Pe02_Eleccion>(id);

        Pe02_Eleccion mapearDtoAEntidad(DtoEleccion dto, string token)
            => new Pe02_Eleccion()
            {
                NombreEleccion = dto.nombreEleccion,
                TipoEleccionId = dto.tipoEleccionId,
                EtiquetaEscanios = dto.EtiquetaEscanios,
                CantidadEscanios = dto.CantidadEscanios,
                AplicaAlicuotaCargo = dto.aplicaAlicuota,
                AplicaCandidatoAlterno = dto.aplicaAlterno,
                Estado = Auditoria.EstadoActivo,
                UsuarioCreacion = _sesionService.ObtenerUsuarioPorToken(token)?.NombreUsuario,
                FechaCreacion = DateTime.Now,
            };

        IEnumerable<DtoEleccion> mapearEntidadADto(Pe02_Eleccion Eleccion)
        {
            DtoEleccion dto = new DtoEleccion();
            dto.Id = Eleccion.Id; ;
            dto.nombreEleccion = Eleccion.NombreEleccion;
            dto.tipoEleccionId = Eleccion.TipoEleccionId;
            dto.usuarioCreacion = Eleccion.UsuarioCreacion;
            dto.EtiquetaEscanios = Eleccion.EtiquetaEscanios;
            dto.CantidadEscanios = Eleccion.CantidadEscanios;
            dto.usuarioModificacion = Eleccion.UsuarioModificacion;
            dto.estado = Eleccion.Estado;
            dto.aplicaAlicuota = Eleccion.AplicaAlicuotaCargo;
            dto.aplicaAlterno = Eleccion.AplicaCandidatoAlterno;
            List<DtoEleccion> lista = new List<DtoEleccion>();
            lista.Add(dto);
            return lista;
        }

        IEnumerable<DtoEleccion> MapearListaEntidadADtoEleccion(IEnumerable<Pe02_Eleccion> Eleccion)
        {
            return Eleccion?.Select(x => new DtoEleccion()
            {
                Id = x.Id,
                nombreEleccion = x.NombreEleccion,
                tipoEleccionId = x.TipoEleccionId,
                nombreTipoEleccion = x.TipoEleccion.NombreTipoEleccion,
                usuarioCreacion = x.UsuarioCreacion,
                usuarioModificacion = x.UsuarioModificacion,
                EtiquetaEscanios = x.EtiquetaEscanios,
                CantidadEscanios = x.CantidadEscanios,
                aplicaAlterno = x.AplicaCandidatoAlterno,
                aplicaAlicuota = x.AplicaAlicuotaCargo,
                estado = x.Estado

            });
        }


        void Crear(Pe02_Eleccion Eleccion)
        {
            _eleccionRepository.Create<Pe02_Eleccion>(Eleccion);
            _eleccionRepository.Save();
        }

        void Actualizar(Pe02_Eleccion Eleccion)
        {
            _eleccionRepository.Update<Pe02_Eleccion>(Eleccion);
            _eleccionRepository.Save();
        }

        void Eliminar(long idEleccion)
        {
            _eleccionRepository.Delete<Pe02_Eleccion>(idEleccion);
            _eleccionRepository.Save();
        }
        void EliminarEscanios(long eleccionId)
        {
            _escanioService.EliminarEscaniosEleccionId(eleccionId);
        }
        #endregion


    }
}
