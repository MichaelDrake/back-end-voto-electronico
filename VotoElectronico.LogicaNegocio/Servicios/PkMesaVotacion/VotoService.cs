﻿using EcVotoElectronico.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using VotoElectronico.Entidades;
using VotoElectronico.Entidades.Pk.PkProcesoElectoral;
using VotoElectronico.Entidades.Pk.PkSeguridad;
using VotoElectronico.Generico;
using VotoElectronico.Generico.Configs;
using VotoElectronico.Generico.Enumeraciones;
using VotoElectronico.Generico.Propiedades;
using VotoElectronico.Util;

namespace VotoElectronico.LogicaNegocio.Servicios
{
    public class VotoService : IVotoService
    {


        private readonly IApiResponseMessage _apiResponseMessage;
        private readonly IPadronVotacionRepository _padronVotacionRepository;
        private readonly IVotoRepository _votoRepository;
        private readonly ICargoRepository _cargoRepository;
        private readonly string stringValido = Enumeration.ObtenerDescripcion(EstadosVoto.VALIDO);
        private readonly string stringBlanco = Enumeration.ObtenerDescripcion(EstadosVoto.BLANCO);
        private readonly string stringNulo = Enumeration.ObtenerDescripcion(EstadosVoto.NULO);
        private readonly decimal porcentajeMinimoLista = (decimal)2 / (decimal)3;
        private readonly decimal porcentajeMinimoCandidato = (decimal)20 / (decimal)100;


        public VotoService(IApiResponseMessage apiResponseMessage, IPadronVotacionRepository padronVotacionRepository, IVotoRepository votoRepository, ICargoRepository cargoRepository)
        {
            _apiResponseMessage = apiResponseMessage;
            _padronVotacionRepository = padronVotacionRepository;
            _votoRepository = votoRepository;
            _cargoRepository = cargoRepository;
        }

        #region Métodos Públicos
        public DtoApiResponseMessage ObtenerResumenProcesoElectoral(long procesoElectoralId)
            => _apiResponseMessage.CrearDtoApiResponseMessage(new List<DtoResultadosProcesoElectoral>() { ContarVotosProcesoElectoral(procesoElectoralId) }, "VE_RSM_INS_001");

        #endregion
        #region  Métodos Privados


        DtoResultadosProcesoElectoral ContarVotosProcesoElectoral(long procesoElectoralId)
        {
            var procesoelectoral = _padronVotacionRepository.GetById<Pe01_ProcesoElectoral>(procesoElectoralId);
            if (verificaresunipersonal(procesoelectoral))
                return ContarVotosProcesoElectoralUnipersonal(procesoelectoral);
            else if (verificarespluriperdonal(procesoelectoral))
                return ContarVotosProcesoElectoralPluripersonal(procesoelectoral);
            throw new Exception("No se puede definir el tipo de Eleccción");
        }

        bool verificaresunipersonal(Pe01_ProcesoElectoral proceso)
            => proceso?.Eleccion?.TipoEleccion?.NombreTipoEleccion?.Equals(Enumeration.ObtenerDescripcion(TipoEleccion.UNIPERSONAL)) ?? false;
        bool verificarespluriperdonal(Pe01_ProcesoElectoral proceso)
        => proceso?.Eleccion?.TipoEleccion?.NombreTipoEleccion?.Equals(Enumeration.ObtenerDescripcion(TipoEleccion.PLURIPERSONAL)) ?? false;



        DtoResultadosProcesoElectoral ContarVotosProcesoElectoralPluripersonal(Pe01_ProcesoElectoral procesoElectoral)
        {
            if (procesoElectoral != null)
            {
                var listasProceso = procesoElectoral.Listas?.Where(x => x.Estado.Equals(Auditoria.EstadoActivo));
                if (listasProceso?.Count() > 0)
                {
                    var votosProceso = _votoRepository.Get<Mv01_Voto>(x => x.ProcesoElectoralId == procesoElectoral.Id);
                    if (votosProceso == null)
                        throw new Exception("Proceso sin Votos");
                    var aplicaAlterno = procesoElectoral.Eleccion?.AplicaCandidatoAlterno ?? false;
                    var aplicaAlicuota = procesoElectoral.Eleccion?.AplicaAlicuotaCargo ?? false;

                    var totalValidosProceso = votosProceso.Count(x => x.Estado.Equals(stringValido));
                    var totalValidosPluriculturales = (votosProceso?.Where(x => x.Estado.Equals(stringValido))?.Sum(x => (x.Opciones.Count()) * (1 /*ObtenerCargoPonderacion(x.Cargo.Ponderacion)*/))) ?? 0;
                    var cantidadEmpadronados = procesoElectoral.PadronesVotacion?.Where(x => x.Estado.Equals(Auditoria.EstadoActivo))?.Count() ?? 0;
                    var resultadoListas = new List<DtoVotoDetalleLista>();
                    var numeroEscaniosProceso = procesoElectoral.Eleccion?.CantidadEscanios ?? 0;
                    if (numeroEscaniosProceso == 0)
                        throw new Exception("El número de escaños de un proceso electorál no puede ser cero");

                    listasProceso.ToList()?.ForEach(x =>
                    {
                        resultadoListas.Add(ObtenerInformacionListaPluriPersonal(x, votosProceso, numeroEscaniosProceso, procesoElectoral.Eleccion?.AplicaCandidatoAlterno ?? false, totalValidosPluriculturales, cantidadEmpadronados));
                    });
                    AsignarVotosListas(resultadoListas, numeroEscaniosProceso, out bool superaNumeroEscaneos, out decimal cifraRepartidora, out bool empateDecimal, out int cantidadEscaniosSobrantes);
                    AsignarCandidatosGanadores(resultadoListas);

                    var porcentajeVotantes = cantidadEmpadronados == 0 ? 0 : ((decimal)(votosProceso?.Count() ?? 0) / (decimal)cantidadEmpadronados) * 100;
                    return new DtoResultadosProcesoElectoral()
                    {
                        AplicaAlicuota = aplicaAlicuota,
                        AplicaAlterno = aplicaAlterno,
                        NumeroEmpadronados = cantidadEmpadronados,
                        NumeroEmpadronadosVotaron = votosProceso.Count(),
                        NumeroVotosValidos = totalValidosProceso,
                        NumeroVotosPluriculturales = totalValidosPluriculturales,
                        NumeroVotosBlancos = votosProceso.Count(x => x.Estado.Equals(stringBlanco)),
                        NumeroVotosNulos = votosProceso.Count(x => x.Estado.Equals(stringNulo)),
                        PorcentajeVotantes = porcentajeVotantes,
                        DetallesListas = resultadoListas,
                        SuperaNumeroEscaneos = superaNumeroEscaneos,
                        CantidadEscaniosSobrantes = cantidadEscaniosSobrantes,
                        EmpateDecimal = empateDecimal,
                        CantidadEscaneos = numeroEscaniosProceso,
                        cifraRepartidora = cifraRepartidora,
                        EleccionValida = porcentajeVotantes >= (decimal)66.67,
                    };
                }
                throw new Exception("Proceso sin Listas ");

            }
            throw new Exception("Proceso electoral votación no existe");

        }

        DtoResultadosProcesoElectoral ContarVotosProcesoElectoralUnipersonal(Pe01_ProcesoElectoral procesoElectoral)
        {
            if (procesoElectoral != null)
            {
                var estadoActivo = Auditoria.EstadoActivo;
                var listasProceso = procesoElectoral.Listas?.Where(x => x.Estado.Equals(estadoActivo));
                if (listasProceso?.Any() ?? false)
                {
                    var votosProceso = _votoRepository.Get<Mv01_Voto>(x => x.ProcesoElectoralId == procesoElectoral.Id);
                    if (votosProceso == null)
                        throw new Exception("Proceso sin Votos");
                    var aplicaAlterno = procesoElectoral.Eleccion?.AplicaCandidatoAlterno ?? false;
                    var aplicaAlicuota = procesoElectoral.Eleccion?.AplicaAlicuotaCargo ?? false;

                    var totalValidosProceso = votosProceso.Count(x => x.Estado.Equals(stringValido));
                    var cantidadEmpadronados = procesoElectoral.PadronesVotacion?.Where(x => x.Estado.Equals(estadoActivo))?.Count() ?? 0;
                    var resultadoListas = new List<DtoVotoDetalleLista>();
                    listasProceso.ToList()?.ForEach(x => resultadoListas.Add(ObtenerInformacionListaUniPersonal(x, votosProceso, cantidadEmpadronados, totalValidosProceso, aplicaAlterno, aplicaAlicuota)));
                    string cargoEstudiante = Enumeration.ObtenerDescripcion(Cargos.ESTUDIANTE);
                    string cargoTrabajador = Enumeration.ObtenerDescripcion(Cargos.TRABAJADOR);
                    string cargoProfesor = Enumeration.ObtenerDescripcion(Cargos.PROFESOR);
                    var porcentajeVotantes = cantidadEmpadronados == 0 ? 0 : ((decimal)(votosProceso?.Count() ?? 0) / (decimal)cantidadEmpadronados) * 100;
                    var cargos = !aplicaAlicuota ? null : _cargoRepository.Get<Sg03_Cargo>(x => x.Estado.Equals(estadoActivo) && (x.NombreCargo.Equals(cargoEstudiante) || x.NombreCargo.Equals(cargoProfesor) || x.NombreCargo.Equals(cargoTrabajador)));
                    var objetoDevolver = new DtoResultadosProcesoElectoral()
                    {
                        AplicaAlicuota = aplicaAlicuota,
                        AplicaAlterno = aplicaAlterno,
                        NumeroEmpadronados = cantidadEmpadronados,
                        NumeroEmpadronadosVotaron = votosProceso.Count(),
                        NumeroVotosValidos = totalValidosProceso,
                        NumeroVotosBlancos = votosProceso.Count(x => x.Estado.Equals(stringBlanco)),
                        NumeroVotosNulos = votosProceso.Count(x => x.Estado.Equals(stringNulo)),
                        PorcentajeVotantes = porcentajeVotantes,
                        EleccionValida = porcentajeVotantes >= porcentajeMinimoLista,
                        DetallesListas = resultadoListas,
                        DetallesProcesoCargo = !aplicaAlicuota ? null : cargos?.Select(y =>
                        {
                            var CantidadEmpadronadosCargo = procesoElectoral.PadronesVotacion?.Count(x => x.Estado.Equals(estadoActivo) && x.Usuario.UsuariosCargo.FirstOrDefault(z => z.Estado.Equals(estadoActivo)).Cargo.NombreCargo.Equals(y.NombreCargo)) ?? 0;
                            return new DtoDatosProcesoCargo()
                            {
                                NombreCargoProceso = y.NombreCargo,
                                CantidadEmpadronadosCargo = CantidadEmpadronadosCargo,
                                Participacion = y.Participacion,
                                CantidadVotosConsignados = votosProceso.Where(a => a.Cargo.NombreCargo.Equals(y.NombreCargo))?.Count() ?? 0
                            };
                        })?.ToList(),
                    };
                    if (aplicaAlicuota)
                    {
                        var numeroProfesores = objetoDevolver.DetallesProcesoCargo?.FirstOrDefault(x => x.NombreCargoProceso.Equals(cargoProfesor))?.CantidadEmpadronadosCargo ?? 0;
                        if (numeroProfesores != 0)
                        {
                            objetoDevolver.DetallesProcesoCargo?.ForEach(x =>
                            {
                                x.Alicuota = x.CantidadEmpadronadosCargo == 0 ? 0 : ((decimal)0.01 * (decimal)x.Participacion * numeroProfesores) / (decimal)x.CantidadEmpadronadosCargo;
                                x.Ponderacion = (decimal)x.Alicuota * (decimal)x.CantidadEmpadronadosCargo;
                                x.CantidadVotosPonderados = x.CantidadVotosConsignados * x.Alicuota;

                            });
                        }
                        var detallesCargo = objetoDevolver.DetallesProcesoCargo;

                        objetoDevolver.DetallesListas?.ForEach(c =>
                        {
                            c.DetallesProcesoCargo = detallesCargo?.Select(x =>
                            {
                                var cantidadVotosConsignados = votosProceso?.Where(e => e.Estado.Equals(stringValido) && e.Cargo.NombreCargo.Equals(x.NombreCargoProceso) && e.Opciones.Any(f => f.ListaId == c.ListaId))?.Count() ?? 0;
                                return new DtoDatosProcesoCargo()
                                {
                                    NombreCargoProceso = x.NombreCargoProceso,
                                    CantidadVotosConsignados = cantidadVotosConsignados,
                                    CantidadVotosPonderados = cantidadVotosConsignados * x.Alicuota,
                                };
                            })?.ToList();
                        });

                        objetoDevolver.DetallesListas?.ForEach(c =>
                        {
                            c.TotalPonderados = c.DetallesProcesoCargo?.Sum(x => x.CantidadVotosPonderados) ?? 0;
                        });

                        decimal totalPonderadosFinal = 0;
                        AniadirDatosNulosBlancos(objetoDevolver.DetallesListas, votosProceso?.ToList(), detallesCargo);
                        var aux = objetoDevolver.DetallesListas;
                        aux?.Add(new DtoVotoDetalleLista()
                        {
                            EsTotal = true,
                            DetallesProcesoCargo = objetoDevolver.DetallesProcesoCargo?.Select(d =>
                            {
                                return new DtoDatosProcesoCargo()
                                {
                                    NombreCargoProceso = $"Total {d.NombreCargoProceso}",
                                    CantidadVotosConsignados = objetoDevolver.DetallesListas?.ToList()?.Sum(x => x.DetallesProcesoCargo.Where(a => a.NombreCargoProceso == d.NombreCargoProceso)?.Sum(y => y.CantidadVotosConsignados)) ?? 0,
                                    CantidadVotosPonderados = objetoDevolver.DetallesListas?.ToList()?.Sum(x => x.DetallesProcesoCargo.Where(a => a.NombreCargoProceso == d.NombreCargoProceso)?.Sum(y => y.CantidadVotosPonderados)) ?? 0
                                };
                            })?.ToList(),
                            TotalPonderados = totalPonderadosFinal = objetoDevolver.DetallesListas?.Sum(x => x.TotalPonderados) ?? 0
                        });


                        objetoDevolver.DetallesListas = aux;

                        objetoDevolver.DetallesListas?.ToList()?.ForEach(x =>
                        {
                            x.PorcentajePonderados = totalPonderadosFinal == 0 ? 0 : (decimal)(x.TotalPonderados * 100) / (decimal)totalPonderadosFinal;
                        });
                    }
                    else
                    {
                        AniadirDatosBlancosNulosNoAlicuota(objetoDevolver.DetallesListas, votosProceso?.ToList(), cantidadEmpadronados, totalValidosProceso);
                    };
                    return objetoDevolver;
                }
                throw new Exception("Proceso sin listas ");
            }
            throw new Exception("Proceso electoral votación no existe");
        }


        void AniadirDatosNulosBlancos(List<DtoVotoDetalleLista> detallesListas, List<Mv01_Voto> votosNoValidos, List<DtoDatosProcesoCargo> procesosCargo)
        {
            List<DtoVotoDetalleLista> aux = new List<DtoVotoDetalleLista>
            {

                new DtoVotoDetalleLista()
                {
                EsNoValido = true,
                NombreLista = "VOTOS NULOS",
                DetallesProcesoCargo = procesosCargo?.Select(d =>
                {  var consignados = votosNoValidos?.Count(x => x.Estado.Equals(stringNulo) && x.Cargo.NombreCargo.Equals(d.NombreCargoProceso)) ?? 0;
                        return new DtoDatosProcesoCargo()
                        {
                            NombreCargoProceso = d.NombreCargoProceso,
                            CantidadVotosConsignados = consignados,
                            CantidadVotosPonderados = d.Alicuota * consignados
                        };
                })?.ToList(),
            },
                new DtoVotoDetalleLista()
                {
                    EsNoValido = true,
                    NombreLista = "VOTOS BLANCOS",
                    DetallesProcesoCargo = procesosCargo?.Select(d =>
                    {
                        var consignados = votosNoValidos?.Count(x => x.Estado.Equals(stringBlanco) && x.Cargo.NombreCargo.Equals(d.NombreCargoProceso)) ?? 0;
                        return new DtoDatosProcesoCargo()
                        {
                            NombreCargoProceso = d.NombreCargoProceso,
                            CantidadVotosConsignados = consignados,
                            CantidadVotosPonderados = d.Alicuota * consignados
                        };
                    })?.ToList(),
                },
            };

            aux?.ForEach(c =>
            {
                c.TotalPonderados = c.DetallesProcesoCargo?.Sum(x => x.CantidadVotosPonderados) ?? 0;
            });

            detallesListas.AddRange(aux);

        }
        void AniadirDatosBlancosNulosNoAlicuota(List<DtoVotoDetalleLista> detallesListas, List<Mv01_Voto> votosNoValidos, int totalEmpadronados, int totalValidosProceso)
        {
            var cantidadBlancosLista = votosNoValidos?.Where(x => x.Estado.Equals(stringBlanco))?.Count() ?? 0;
            var cantidadnulosLista = votosNoValidos?.Where(x => x.Estado.Equals(stringNulo))?.Count() ?? 0;
            var aux = new List<DtoVotoDetalleLista>() {
              new DtoVotoDetalleLista()
            {
                EsNoValido = true,
                CantidadVotosLista = cantidadnulosLista,
                PorcentajeEmpadronados = ((decimal)cantidadnulosLista / totalEmpadronados) * 100,
                PorcentajeValidosTotalLista = totalValidosProceso == 0 ? 0 : ((decimal)cantidadnulosLista / totalValidosProceso) * 100,
                NombreLista = "VOTOS NULOS",
            },
            new DtoVotoDetalleLista()
            {
                EsNoValido = true,
                CantidadVotosLista = cantidadBlancosLista,
                PorcentajeEmpadronados = ((decimal)cantidadBlancosLista / totalEmpadronados) * 100,
                PorcentajeValidosTotalLista = totalValidosProceso == 0 ? 0 : ((decimal)cantidadBlancosLista / totalValidosProceso) * 100,
                NombreLista = "VOTOS BLANCOS",
            }
            };

            detallesListas.AddRange(aux);
        }

        DtoVotoDetalleLista ObtenerInformacionListaPluriPersonal(Pe05_Lista lista, IEnumerable<Mv01_Voto> votos, int numeroEscanios, bool aplicaAlterno, decimal totalValidosProceso, int numeroEmpadronados)
        {
            var cantidadValidosLista = (votos?.Where(x => x.Estado.Equals(stringValido))?.Sum(x => (x.Opciones.Where(y => y.Candidato.ListaId == lista.Id)?.Count() ?? 0) * (1 /*ObtenerCargoPonderacion(x.Cargo.Ponderacion)*/))) ?? 0;
            var detalleLista = new DtoVotoDetalleLista()
            {
                CantidadVotosLista = cantidadValidosLista,
                //PorcentajeEmpadronados = ((decimal)cantidadValidosLista / totalEmpadronados) * 100,
                PorcentajeValidosTotalLista = totalValidosProceso == 0 ? 0 : ((decimal)cantidadValidosLista / totalValidosProceso) * 100,
                ListaId = lista?.Id ?? 0,
                NombreLista = lista?.NombreLista,
                ImagenLista = string.IsNullOrEmpty(lista?.Logo) ? CtEstaticas.StrImagenNoLista : $"{ CtEstaticas.StrGoogleDrive}{lista?.Logo}",
                Coeficientes = CalcularCoeficientes(cantidadValidosLista, numeroEscanios, lista.Id)?.ToList(),
                Candidatos = lista?.candidatos?.Select(x =>
                {
                    var votosCandidato = votos?.Where(y => y.Estado.Equals(stringValido))?.Sum(z => z.Opciones.Where(a => a.Candidato.ListaId == lista.Id && a.CandidatoId == x.Id)?.Count() ?? 0) ?? 0;
                    return new DtoCandidato()
                    {
                        nombreCandidato = $"{x.Persona?.NombreUno} {x.Persona?.ApellidoUno}",
                        fotoUrl = string.IsNullOrEmpty(x?.Foto) ? CtEstaticas.StrImagenNoUser : $"{ CtEstaticas.StrGoogleDrive}{x.Foto}",
                        CantidadVotosPersonales = votosCandidato,
                        PorcentajeCantidadVotosPersonales = totalValidosProceso == 0 ? 0 : ((decimal)votosCandidato / totalValidosProceso) * 100,
                        PorcentajeEmpadronadosVotaron = numeroEmpadronados == 0 ? 0 : ((decimal)votosCandidato / (decimal)numeroEmpadronados) * 100,
                        Elegible = numeroEmpadronados != 0 && ((decimal)votosCandidato / (decimal)numeroEmpadronados) >= porcentajeMinimoCandidato,
                        nombreCandidatoAlterno = !aplicaAlterno ? null : $"{x.Alterno?.Persona?.NombreUno} {x.Alterno?.Persona?.ApellidoUno}",
                        fotoUrlAlterno = !aplicaAlterno ? null : string.IsNullOrEmpty(x?.Alterno?.Foto) ? CtEstaticas.StrImagenNoUser : $"{ CtEstaticas.StrGoogleDrive}{x.Alterno?.Foto}",
                    };
                })?.ToList()
            };
            return detalleLista;
        }

        DtoVotoDetalleLista ObtenerInformacionListaUniPersonal(Pe05_Lista lista, IEnumerable<Mv01_Voto> votos, decimal totalEmpadronados, decimal totalValidosProceso, bool aplicaAlterno, bool aplicaAlicuota)
        {
            var cantidadValidosLista = aplicaAlicuota ? 0 : votos?.Where(x => (x.Opciones.FirstOrDefault(y => y.ListaId == lista.Id) != null) && x.Estado.Equals(stringValido))?.Count() ?? 0;
            if (totalEmpadronados == 0)
                throw new Exception("Proceso sin Empadronados");
            return new DtoVotoDetalleLista()
            {
                CantidadVotosLista = cantidadValidosLista,
                PorcentajeEmpadronados = aplicaAlicuota ? 0 : (((decimal)cantidadValidosLista / totalEmpadronados) * 100),
                PorcentajeValidosTotalLista = aplicaAlicuota ? 0 : (totalValidosProceso == 0 ? 0 : ((decimal)cantidadValidosLista / totalValidosProceso) * 100),
                ListaId = lista?.Id ?? 0,
                NombreLista = lista?.NombreLista,
                ImagenLista = string.IsNullOrEmpty(lista?.Logo) ? CtEstaticas.StrImagenNoLista : $"{ CtEstaticas.StrGoogleDrive}{lista?.Logo}",
                Candidatos = lista?.candidatos?.Select(x => new DtoCandidato()
                {
                    Id = x.Id,
                    nombreEscanio = x.Escanio?.NombreEscanio,
                    nombreCandidato = $"{x.Persona?.NombreUno} {x.Persona?.ApellidoUno}",
                    fotoUrl = string.IsNullOrEmpty(x?.Foto) ? CtEstaticas.StrImagenNoUser : $"{ CtEstaticas.StrGoogleDrive}{x.Foto}",
                    nombreCandidatoAlterno = !aplicaAlterno ? null : $"{x.Alterno?.Persona?.NombreUno} {x.Alterno?.Persona?.ApellidoUno}",
                    fotoUrlAlterno = !aplicaAlterno ? null : string.IsNullOrEmpty(x?.Alterno?.Foto) ? CtEstaticas.StrImagenNoUser : $"{ CtEstaticas.StrGoogleDrive}{x?.Alterno?.Foto}",
                })?.ToList()
            };
        }

        IEnumerable<DtoListaCoeficiente> CalcularCoeficientes(decimal cantidadVotos, int numeroEscanios, long listaId)
        {
            var listaCoeficientesLista = new List<DtoListaCoeficiente>();
            for (decimal i = 1; i <= numeroEscanios; i++)
            {
                listaCoeficientesLista.Add(new DtoListaCoeficiente()
                {
                    ListaId = listaId,
                    Coeficiente = cantidadVotos / i
                });
            }
            return listaCoeficientesLista;
        }

        void AsignarCandidatosGanadores(List<DtoVotoDetalleLista> detallesLista)
        {
            detallesLista?.ForEach(x =>
            {
                var cantidadEscanios = x.NumeroEscaniosAsignados;
                if (cantidadEscanios > 0)
                {
                    var contadorEscaniosAsignados = 1;
                    var cantidadVotosPersonalesUltimoCandidato = 0;
                    x.Candidatos?.OrderByDescending(b => b.CantidadVotosPersonales)?.ToList()?.ForEach(y =>
                    {
                        if (contadorEscaniosAsignados < cantidadEscanios)
                            y.GanadorSugerido = true;
                        if (contadorEscaniosAsignados == cantidadEscanios)
                        {
                            y.GanadorSugerido = true;
                            cantidadVotosPersonalesUltimoCandidato = y.CantidadVotosPersonales;
                        }
                        contadorEscaniosAsignados++;
                    });

                    var listaCandidatosPosibleEmpate = x.Candidatos?.Where(a => a.CantidadVotosPersonales == cantidadVotosPersonalesUltimoCandidato && !a.GanadorSugerido);

                    if (listaCandidatosPosibleEmpate?.Count() > 0)
                    {
                        x.EscaniosEmpatados = cantidadEscanios - (x.Candidatos?.Where(d => d.GanadorSugerido && d.CantidadVotosPersonales == cantidadVotosPersonalesUltimoCandidato)?.Count() ?? 0);
                        x.TieneCandidatosEmpatados = true;
                        x.Candidatos?.Where(d => d.CantidadVotosPersonales == cantidadVotosPersonalesUltimoCandidato)?.ToList()?.ForEach(z =>
                       {
                           z.GanadorSugerido = false;
                           z.GanadorSugeridoEmpate = true;
                       });
                    }
                    x.Candidatos = x.Candidatos?.OrderByDescending(y => y.CantidadVotosPersonales)?.ToList();
                }
            });
        }


        void AsignarVotosListas(List<DtoVotoDetalleLista> detallesLista, int numeroEscanios, out bool superaNumeroEscaneos, out decimal cifraRepetidora, out bool empateDecimal, out int cantidadEscaniosSobrantes)
        {
            cantidadEscaniosSobrantes = 0;
            superaNumeroEscaneos = false;
            empateDecimal = false;
            var listaCoeficientes = new List<DtoListaCoeficiente>();

            detallesLista?.ForEach(x =>
            {
                x.Coeficientes?.ToList()?.ForEach(y =>
                {
                    listaCoeficientes.Add(y);
                });
            });

            listaCoeficientes = listaCoeficientes.OrderByDescending(x => x.Coeficiente)?.ToList();
            var cifraRepetidoraUsar = listaCoeficientes[numeroEscanios - 1]?.Coeficiente ?? 0;
            cifraRepetidora = cifraRepetidoraUsar;
            if (cifraRepetidoraUsar != 0)
            {
                detallesLista?.ForEach(x =>
                {
                    var votosAsignados = x.CantidadVotosLista / (decimal)cifraRepetidoraUsar;
                    var parteentera = (int)Math.Truncate(votosAsignados);
                    x.NumeroEscaniosAsignados = parteentera;
                    x.NumeroEscaniosAsignadosDecimal = votosAsignados - parteentera;
                    x.NumeroEscaniosAsignadosSuma = parteentera + x.NumeroEscaniosAsignadosDecimal;
                    SetearCoeficientesGanadoresFaltantes(x, parteentera);

                });
            }


            if (!ValidarEscaniosCompletos(detallesLista, numeroEscanios, out int numeroEscaniosAsinados))
            {
                AgragarEscaniosFaltantes(detallesLista, numeroEscanios, numeroEscaniosAsinados, out superaNumeroEscaneos, out empateDecimal, out cantidadEscaniosSobrantes);
            }



        }
        void SetearCoeficientesGanadoresFaltantes(DtoVotoDetalleLista detalleLista, int cantidadEscaniosAsignados)
        {
            for (int i = 1; i <= cantidadEscaniosAsignados; i++)
            {
                var coeficienteGanador = detalleLista.Coeficientes?.FirstOrDefault(x => !x.Ganador);
                if (coeficienteGanador != null)
                    coeficienteGanador.Ganador = true;
            }
        }

        void SetearCoeficientesEmpatados(DtoVotoDetalleLista detalleLista, int cantidadEscaniosAsignados)
        {
            for (int i = 1; i <= cantidadEscaniosAsignados; i++)
            {
                var coeficienteGanador = detalleLista.Coeficientes?.FirstOrDefault(x => !x.Ganador);
                if (coeficienteGanador != null)
                    coeficienteGanador.GanadorFaltante = true;
            }
        }



        void QuitarCoeficientesGanadores(DtoVotoDetalleLista detalleLista, int cantidadEscaniosSobrantes)
        {
            var cantidadSobrante = cantidadEscaniosSobrantes < 0 ? cantidadEscaniosSobrantes * -1 : 0;
            for (int i = 1; i <= cantidadSobrante; i++)
            {
                var ganador = detalleLista.Coeficientes?.OrderBy(x => x.Coeficiente)?.FirstOrDefault(x => x.Ganador);
                if (ganador != null)
                    ganador.Ganador = false;
            }
        }
        bool ValidarEscaniosCompletos(List<DtoVotoDetalleLista> detalleLista, int numeroEscanios, out int numeroEscaniosAsignados)
        {
            numeroEscaniosAsignados = detalleLista?.Select(x => x.NumeroEscaniosAsignados)?.Sum() ?? 0;
            return numeroEscaniosAsignados == numeroEscanios;
        }

        void AgragarEscaniosFaltantes(List<DtoVotoDetalleLista> detallesListas, int numeroEscanios, int numeroEscaniosAsinados, out bool superaNumeroEscaneos, out bool empateAsignacionDecimal, out int cantidadEscaniosSobrantes)
        {
            var escaniosFaltantes = numeroEscanios - numeroEscaniosAsinados;
            cantidadEscaniosSobrantes = 0;
            superaNumeroEscaneos = escaniosFaltantes < 0;
            empateAsignacionDecimal = false;
            if (escaniosFaltantes > 0)
            {

                var listaOrdenadaListas = detallesListas?.OrderByDescending(x => x.NumeroEscaniosAsignadosDecimal);
                var numeroDecimalMayor = listaOrdenadaListas?.FirstOrDefault()?.NumeroEscaniosAsignadosDecimal;
                var listasMayoresDecimal = listaOrdenadaListas?.Where(x => x.NumeroEscaniosAsignadosDecimal == numeroDecimalMayor);

                if (listasMayoresDecimal?.Count() == 1)
                {
                    listasMayoresDecimal.ToList().FirstOrDefault().NumeroEscaniosAsignados += escaniosFaltantes;
                    listasMayoresDecimal.ToList().FirstOrDefault().NumeroEscaniosAsignadosSobrantes = escaniosFaltantes;
                    //listasMayoresDecimal.ToList().FirstOrDefault().NumeroEscaniosAsignadosMasSobrantes = listasMayoresDecimal.FirstOrDefault().NumeroEscaniosAsignados + escaniosFaltantes;
                    SetearCoeficientesGanadoresFaltantes(listasMayoresDecimal.ToList().FirstOrDefault(), escaniosFaltantes);
                }
                else if (listasMayoresDecimal?.Count() > 1)
                {
                    cantidadEscaniosSobrantes = escaniosFaltantes;
                    empateAsignacionDecimal = true;
                    listasMayoresDecimal.ToList().ForEach(x =>
                    {
                        SetearCoeficientesEmpatados(x, escaniosFaltantes);
                    });

                }

            }
            else if (escaniosFaltantes < 0)
            {
                cantidadEscaniosSobrantes = escaniosFaltantes * -1;
                //var listaRestar = detalleLista.Where(x => x.NumeroEscaniosAsignados > 0)?.OrderBy(x => x.NumeroEscaniosAsignados)?.FirstOrDefault();
                //if (listaRestar != null)
                //{
                //    listaRestar.NumeroEscaniosAsignados += escaniosFaltantes;
                //    listaRestar.NumeroEscaniosAsignadosSuma += escaniosFaltantes;
                //    QuitarCoeficientesGanadores(listaRestar, escaniosFaltantes);
                //}
            }


        }
        #endregion
    }
}
