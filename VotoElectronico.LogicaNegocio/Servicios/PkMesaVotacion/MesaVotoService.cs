﻿using EcVotoElectronico.Repositorios;
using System;
using System.Transactions;
using VotoElectronico.Entidades;
using VotoElectronico.Generico;
using VotoElectronico.Generico.Enumeraciones;
using VotoElectronico.Util;
using VotoElectronico.Generico.Propiedades;
using System.Collections.Generic;

namespace VotoElectronico.LogicaNegocio.Servicios
{
    public class MesaVotoService : IMesaVotoService
    {

        private readonly IApiResponseMessage _apiResponseMessage;
        private readonly IRsaHelper _rsaHelper;
        private readonly IAesService _aesService;
        private readonly IVotoRepository _votoRepository;
        private readonly IOpcionRepository _opcionRepository;


        public MesaVotoService(
            IApiResponseMessage apiResponseMessage,
            IRsaHelper rsaHelper,
            IAesService aesService,
            IVotoRepository votoRepository,
            IOpcionRepository opcionRepository
            )
        {
            _apiResponseMessage = apiResponseMessage;
            _rsaHelper = rsaHelper;
            _aesService = aesService;
            _votoRepository = votoRepository;
            _opcionRepository = opcionRepository;
        }

        public DtoApiResponseMessage EmitirVoto(DtoAES dtoAes)
        {
            //1.- Verificar voto firmado
            var votoAesVerificado = _rsaHelper.VerificarFirmaDigital(dtoAes.VotoCifradoAes+dtoAes.mascara, dtoAes.VotoFirmado);
            if (votoAesVerificado)
            {
                //2.- Desencriptar voto con llaves AES
                var listaOpciones = _aesService.DecryptStringAES(dtoAes.VotoCifradoAes, dtoAes.Key, dtoAes.vectorInicializacion);

                //3.- Guardar voto en base 
                var voto = new Mv01_Voto
                {
                    Mascara = dtoAes.mascara,
                    ProcesoElectoralId = Convert.ToInt64(UtilEncodeDecode.Base64Decode(dtoAes.procesoElectoralId)),
                    FechaCreacion = DateTime.Now,
                    FechaModificacion = DateTime.Now,
               
                };
                if (dtoAes.cargoId != 0)
                    voto.CargoId = dtoAes.cargoId;

                using var transaccion = new TransactionScope();
                try
                {
                    if (listaOpciones == null)
                    {
                        voto.Estado = nameof(EstadosVoto.NULO);
                        CrearVoto(voto);
                    }
                    else if (listaOpciones.Count ==0)
                    {
                        voto.Estado = nameof(EstadosVoto.BLANCO);
                        CrearVoto(voto);
                    }
                    else
                    {
                        voto.Estado = nameof(EstadosVoto.VALIDO);
                        CrearVoto(voto);
                        //4.- guardar opciones del voto
                        foreach (DtoOpcion opcion in listaOpciones)
                        {
                            CrearOpcion(new Mv02_Opcion
                            {
                                VotoId = voto.Id,
                                ListaId = Convert.ToInt64(opcion.listaId),
                                CandidatoId = Convert.ToInt64(opcion.candidatoId),
                                EscanioId = Convert.ToInt64(opcion.escanioId),
                                Estado = Auditoria.EstadoActivo,
                                FechaCreacion = DateTime.Now,
                                FechaModificacion = DateTime.Now,
                            });

                        }
                    }
                    
                    transaccion.Complete();
                    
                }
                catch (Exception ex)
                {
                    return _apiResponseMessage.CrearDtoApiResponseMessage(null, "VE_PEL_MV_002");
                }
                finally
                {
                    transaccion.Dispose();
                    
                }
                return _apiResponseMessage.CrearDtoApiResponseMessage(null, "VE_PEL_MV_003");

            }
            else
            {
                // Si el voto no coincide con la firma se anula el voto en la mesa de identificacion y se retorna un error.
                return _apiResponseMessage.CrearDtoApiResponseMessage(null, "VE_PEL_MV_001");
            }
        }


        //@param: mascara =  llega la mascar codificada en Base64
        public DtoApiResponseMessage VerificarVoto(string mascara, string procesoElectoralId)
        {
            try
            {
                var voto = ObtenerVotoByMascara(UtilEncodeDecode.Base64Decode(mascara), UtilEncodeDecode.Base64Decode(procesoElectoralId));
                if (voto != null)
                {
                    var dtoMapeado = MapearEntidadADto(voto);
                    return _apiResponseMessage.CrearDtoApiResponseMessage(dtoMapeado, "VE_PEL_MV_004");
                }
                else
                {
                    throw new Exception("Error al autenticar el comprobante");

                }
              
            }
            catch
            {
                throw new Exception("Error al autenticar el comprobante");
            }

        }


        
        public Mv01_Voto ObtenerVotoByMascara(string mascara,string procesoIdDecode)
        {
            var procesoIdToInt = Int64.Parse(procesoIdDecode);
            var voto = _votoRepository.GetOneOrDefault<Mv01_Voto>(voto => voto.Mascara.Equals(mascara) && voto.ProcesoElectoralId == procesoIdToInt);
            return voto;

        }

        public IEnumerable<Mv02_Opcion> ObtenerOpcionesDeVotoByVotoId(long votoId)
        {
            var opcionesVoto = _opcionRepository.Get<Mv02_Opcion>(opcion => opcion.VotoId == votoId);
            return opcionesVoto;
        }

        public IEnumerable<DtoVoto> MapearEntidadADto(Mv01_Voto voto)
        {
            var dtoVoto = new DtoVoto
            {
                //Mascara = voto.Mascara,
                Estado = voto.Estado,
                opcionesVoto = null,
                nombreProcesoElectoral = voto.ProcesoElectoral.NombreProcesoElectoral,
                fechaVoto = voto.FechaCreacion
            };

            if (voto.Estado.Equals(nameof(EstadosVoto.VALIDO)))
            {
                var votoOpciones = ObtenerOpcionesDeVotoByVotoId(voto.Id);
                dtoVoto.opcionesVoto = MapearEntidadOpcionesADto(votoOpciones);
            }
            return new List<DtoVoto>{ dtoVoto };
        }


        public IEnumerable<DtoOpcionVoto> MapearEntidadOpcionesADto(IEnumerable<Mv02_Opcion> listaOpciones)
        {
            List<DtoOpcionVoto> responseList = new List<DtoOpcionVoto>();

            foreach (var opcion in listaOpciones)
            {
                responseList.Add(new DtoOpcionVoto
                {
                    listaId = opcion.ListaId,
                    nombreLista = opcion.Lista.NombreLista,
                    candidatoId = opcion.CandidatoId,
                    nombreCandidato = opcion.Candidato.Persona.NombreUno +" "+ opcion.Candidato.Persona.ApellidoUno,
                    nombreCandidatoAlterno = opcion.Candidato?.Alterno != null? opcion.Candidato?.Alterno?.Persona?.NombreUno + " " + opcion.Candidato?.Alterno?.Persona?.ApellidoUno : null,
                    escanioId = opcion.EscanioId,
                    nombreEscanio = opcion.Candidato.Escanio.NombreEscanio,
                }
                );
            }

            return responseList;
        }




        #region Métodos Privados

        void CrearVoto(Mv01_Voto voto)
        {
            _votoRepository.Create<Mv01_Voto>(voto);
            _votoRepository.Save();
        }


        void ActualizarVoto(Mv01_Voto voto)
        {
            _votoRepository.Update<Mv01_Voto>(voto);
            _votoRepository.Save();
        }


        void CrearOpcion(Mv02_Opcion opcion)
        {
            _opcionRepository.Create<Mv02_Opcion>(opcion);
            _opcionRepository.Save();
        }


        void ActualizarOpcion(Mv02_Opcion opcion)
        {
            _opcionRepository.Update<Mv02_Opcion>(opcion);
            _opcionRepository.Save();
        }

      

        #endregion
    }
}
