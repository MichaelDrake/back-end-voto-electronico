﻿
using VotoElectronico.Generico;

namespace VotoElectronico.LogicaNegocio.Servicios
{
    public interface IMesaVotoService
    {
        DtoApiResponseMessage EmitirVoto(DtoAES dtoAes);

        DtoApiResponseMessage VerificarVoto(string mascara, string procesoElectoralId);

    }
}

