﻿
using VotoElectronico.Entidades.Pk.PkMesaIdentificacion;
using VotoElectronico.Generico;

namespace VotoElectronico.LogicaNegocio.Servicios
{
    public interface IPadronVotacionService
    {
        DtoApiResponseMessage ObtenerPadronVotacionesPorNombre(long id, string nombre, int pagina);
        DtoApiResponseMessage ObtenerPadronVotacionMedianteId(DtoPadronVotacion dto);
        Mi01_PadronVotacion ObtenerPadronVotacionId(long id);
        Mi01_PadronVotacion ObtenerPadronVotacionMedianteIdentificacion(string identificacion, long procesoId, string estado);

        DtoApiResponseMessage CrearPadronVotacion(DtoPadronVotacion dto, string token);

        DtoApiResponseMessage ActualizarPadronVotacion(DtoPadronVotacion dto);

        DtoApiResponseMessage EliminarPadronVotacion(DtoPadronVotacion dtoPadronVotacion);

        bool ExisteEmpadronado(string Identificacion, long Estado);

        void HabilitarEntodadPadronVotacion(Mi01_PadronVotacion padron);
        void EliminarPadronesPorProceso(long procesoId);

        void InsertarPadroVotacion(DtoPadronVotacion dto, string token, string usuarioCreacion = "");

        DtoApiResponseMessage AutorizarVoto(DtoVotoRSA dtoVotoRsa, string token);
    }
}

