﻿using EcVotoElectronico.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using VotoElectronico.Entidades.Pk.PkSeguridad;
using VotoElectronico.Generico;
using VotoElectronico.Generico.Propiedades;
using VotoElectronico.LogicaCondicional;
using VotoElectronico.Util;

namespace VotoElectronico.LogicaNegocio.Servicios
{
    public class PersonaService : IPersonaService
    {

        private readonly IPersonaRepository _personaRepository;
        private readonly IUsuarioRepository _usuarioRepository;
        private IApiResponseMessage _utilitarios;
        private ISesionService _sesionService;

        public PersonaService(IPersonaRepository personaRepository, 
            IApiResponseMessage utilitarios, 
            ISesionService sesionService,
            IUsuarioRepository usuarioRepository
            )
        {
            _personaRepository = personaRepository;
            _utilitarios = utilitarios;
            _sesionService = sesionService;
            _usuarioRepository = usuarioRepository;
        }

        #region Métodos Publicos

        // public DtoApiResponseMessage ObtenerPersonasPorNombres(string nombreUno, string nombreDos)
        public IEnumerable<DtoPersona> ObtenerPersonasPorParametros(string identificacion)
        {
            var spec = new Sg02_PersonaCondicional().FiltrarPersonaPorIdentificacion(identificacion);
            var dtoMapeado = MapearListaEntidadADtoPersona(_personaRepository.FiltrarPersonasEspecificacionAutocomplete(spec));

            if (dtoMapeado.Count() != 0)
            {
                //return _utilitarios.crearDtoApiResponseMessage(dtoMapeado, "VE_SEG_PER_005");
                return dtoMapeado;
            }
            else
            {
                //return _utilitarios.crearDtoApiResponseMessage(dtoMapeado, "VE_SEG_PER_006");
                return dtoMapeado;
            }

        }


       

        public DtoApiResponseMessage ObtenerListaPersonasByParametros(DtoEspecificacion dto)
        {
            var spec = new Sg02_PersonaCondicional().FiltrarPersonaByParams(dto);
            var dtoMapeado = MapearListaEntidadADtoPersona(_personaRepository.FiltrarPersonasEspecificacion(spec, dto.Pagina, out int total), total);

            if ((dtoMapeado?.Count() ?? 0) != 0)
            {
                return _utilitarios.CrearDtoApiResponseMessage(dtoMapeado, "VE_SEG_PER_005");
                //return dtoMapeado;
            }
            else
            {
                return _utilitarios.CrearDtoApiResponseMessage(null, "VE_SEG_PER_006");
                //return dtoMapeado;
            }
            throw new NotImplementedException();
        }

        public Sg02_Persona ObtenerPersonaIdentificacion(string identificacion, string estado = "")
            => _personaRepository.GetOneOrDefault<Sg02_Persona>(x => x.Identificacion.Equals(identificacion) && (string.IsNullOrEmpty(estado) || estado.Equals(estado)));



        public DtoApiResponseMessage ObtenerPersonaMedianteId(DtoPersona dto)
        {
            var persona = ObtenerPersonaId(dto.Id);
            var dtoMapeado = mapearEntidadADto(persona);
            return _utilitarios.CrearDtoApiResponseMessage(dtoMapeado, "VE_SEG_PER_004");
        }

        public DtoApiResponseMessage CrearPersona(DtoPersona dto, string token)
        {
            var existePersona = _personaRepository.GetExists<Sg02_Persona>(persona => persona.Identificacion.Equals(dto.identificacion) || persona.Email.Equals(dto.email));
            
            if (existePersona)
            {
               return  _utilitarios.CrearDtoApiResponseMessage(null, "VE_SEG_PER_009");
            }
            else
            {
               return  _utilitarios.CrearDtoApiResponseMessage(mapearEntidadADto(InsertarPersona(dto, token)), "VE_SEG_PER_001");
            }
            
        }
             

        public Sg02_Persona InsertarPersona(DtoPersona dto, string token, string usuarioCreacion = "")
        {
                var persona = mapearDtoAEntidad(dto, token, usuarioCreacion);
                Crear(persona);
                return persona;
        }

        public DtoApiResponseMessage ActualizarPersona(DtoPersona dto, string token)
        {
            var existePersona = _personaRepository.GetExists<Sg02_Persona>(persona => (persona.Identificacion.Equals(dto.identificacion) || persona.Email.Equals(dto.email)) && persona.Id != dto.Id);

            if (existePersona)
            {
                return _utilitarios.CrearDtoApiResponseMessage(null, "VE_SEG_PER_009");
               
            }
            else
            {
                var persona = ObtenerPersonaId(dto.Id);

                if (persona != null)
                {
                    persona.NombreUno = dto.nombreUno;
                    persona.NombreDos = dto.nombreDos;
                    persona.ApellidoUno = dto.apellidoUno;
                    persona.ApellidoDos = dto.apellidoDos;

                    //persona.Identificacion = dto.identificacion;
                    persona.Telefono = dto.telefonoUno;
                    persona.Telefono2 = dto.telefonoDos;
                    persona.Email = dto.email;
                    persona.UsuarioModificacion = _sesionService.ObtenerUsuarioPorToken(token)?.NombreUsuario;
                    persona.FechaModificacion = DateTime.Now;
                    Actualizar(persona);
                    var dtoMapeado = mapearEntidadADto(persona);
                    return _utilitarios.CrearDtoApiResponseMessage(dtoMapeado, "VE_SEG_PER_002");
                }
                else
                {
                    return _utilitarios.CrearDtoApiResponseMessage(null, "VE_SEG_PER_007");
                }
            }



        }

        public bool ExistePersonaActivoInactivo(string identificacion)
            => _personaRepository.GetExists<Sg02_Persona>(x => x.Identificacion.Equals(identificacion));
        public bool ExistePersonaEstado(string identificacion, string estado)
            => _personaRepository.GetExists<Sg02_Persona>(x => x.Estado.Equals(estado) && x.Identificacion.Equals(identificacion));


        public DtoApiResponseMessage EliminarPersona(DtoPersona dto)
        {
            var persona = ObtenerPersonaId(dto.Id);
            if (persona != null)
            {
                _personaRepository.Delete<Sg02_Persona>(persona.Id);
                _personaRepository.Save();

                var dtoMapeado = mapearEntidadADto(persona);
                return _utilitarios.CrearDtoApiResponseMessage(dtoMapeado, "VE_SEG_PER_003");
            }
            else
            {
                return _utilitarios.CrearDtoApiResponseMessage(null, "VE_SEG_PER_007");
            }

        }


        public DtoApiResponseMessage InactivarPersona(long idPersona, string token)
        {
            var persona = ObtenerPersonaId(idPersona);

            if (persona != null)
            {
                using var transaccion = new TransactionScope();
                try
                {

                    persona.Estado = Auditoria.EstadoInactivo;
                    persona.UsuarioModificacion = _sesionService.ObtenerUsuarioPorToken(token)?.NombreUsuario;
                    persona.FechaModificacion = new DateTime();
                    Actualizar(persona);

                    var usuario = _usuarioRepository.GetOneOrDefault<Sg01_Usuario>(usuario => usuario.NombreUsuario.Equals(persona.Identificacion));
                    if(usuario != null)
                    {
                        usuario.Estado = Auditoria.EstadoInactivo;
                        usuario.UsuarioModificacion = _sesionService.ObtenerUsuarioPorToken(token)?.NombreUsuario;
                        usuario.FechaModificacion = new DateTime();
                        _usuarioRepository.Update<Sg01_Usuario>(usuario);
                        _usuarioRepository.Save();
                    }
                    transaccion.Complete();

                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    transaccion.Dispose();
                }
                var dtoMapeado = mapearEntidadADto(persona);
                return _utilitarios.CrearDtoApiResponseMessage(dtoMapeado, "VE_SEG_PER_003");
            }
            else
            {
                return _utilitarios.CrearDtoApiResponseMessage(null, "VE_SEG_PER_007");
            }

        }

        public DtoApiResponseMessage ReactivarPersona(long idPersona, string token)
        {
            var persona = ObtenerPersonaId(idPersona);

            if (persona != null)
            {
                persona.Estado = Auditoria.EstadoActivo;
                persona.UsuarioModificacion = _sesionService.ObtenerUsuarioPorToken(token)?.NombreUsuario;
                persona.FechaModificacion = new DateTime();
                Actualizar(persona);

                var dtoMapeado = mapearEntidadADto(persona);
                return _utilitarios.CrearDtoApiResponseMessage(dtoMapeado, "VE_SEG_PER_008");
            }
            else
            {
                return _utilitarios.CrearDtoApiResponseMessage(null, "VE_SEG_PER_007");
            }

        }

        public void ReactivarPersona(DtoPersona dto)
        {
            var Persona = ObtenerPersonaId(dto.Id);
            ReactivarEntidadPersona(Persona);
        }


        public void ReactivarEntidadPersona(Sg02_Persona persona)
        {
            persona.Estado = "ACTIVO";
            Actualizar(persona);
        }

        public void InabilitarPersona(DtoPersona dto)
        {
            var Persona = ObtenerPersonaId(dto.Id);
            Persona.Estado = "INACTIVO";
            Actualizar(Persona);
        }





        #endregion

        #region Métodos Privados
        Sg02_Persona ObtenerPersonaId(long id)
        => _personaRepository.GetById<Sg02_Persona>(id);

        Sg02_Persona mapearDtoAEntidad(DtoPersona dto, string token, string usuario = "")
            => new Sg02_Persona()
            {
                NombreUno = dto.nombreUno,
                NombreDos = dto.nombreDos,
                ApellidoUno = dto.apellidoUno,
                ApellidoDos = dto.apellidoDos,
                Identificacion = dto.identificacion,
                Telefono = dto.telefonoUno,
                Telefono2 = dto.telefonoDos,
                Email = dto.email,
                Estado = Auditoria.EstadoActivo,
                UsuarioCreacion = !string.IsNullOrEmpty(usuario)? usuario : _sesionService.ObtenerUsuarioPorToken(token)?.NombreUsuario,
                FechaCreacion = DateTime.Now,
            };

        IEnumerable<DtoPersona> mapearEntidadADto(Sg02_Persona Persona)
        {
            DtoPersona dto = new DtoPersona();
            dto.Id = Persona.Id;
            dto.nombreUno = Persona.NombreUno;
            dto.nombreDos = Persona.NombreDos;
            dto.apellidoUno = Persona.ApellidoUno;
            dto.apellidoDos = Persona.ApellidoDos;
            dto.identificacion = Persona.Identificacion;
            dto.telefonoUno = Persona.Telefono;
            dto.telefonoDos = Persona.Telefono2;
            //if (Persona.FechaCreacion != null)
            //{
            //    dto.fechaa = (DateTime)Persona.FechaCreacion;
            //}
            //dto.PersonaModificacion = Persona.PersonaModificacion;
            //if (Persona.FechaModificacion != null)
            //{
            //    dto.fechaModificacion = (DateTime)Persona.FechaModificacion;
            dto.email = Persona.Email;
            dto.estado = Persona.Estado;

            List<DtoPersona> lista = new List<DtoPersona>();
            lista.Add(dto);
            return lista;
        }

        IEnumerable<DtoPersona> MapearListaEntidadADtoPersona(IEnumerable<Sg02_Persona> persona, int total = 0)
        {
            return persona.Select(x => new DtoPersona()
            {
                dt1 = x.Identificacion,
                Id = x.Id,
                nombreUno = x.NombreUno,
                nombreDos = x.NombreDos,
                apellidoUno = x.ApellidoUno,
                apellidoDos = x.ApellidoDos,
                telefonoUno = x.Telefono,
                telefonoDos = x.Telefono2,
                estado = x.Estado,
                email = x.Email,
                Total = total
        });
        }


        void Crear(Sg02_Persona persona)
        {
            _personaRepository.Create<Sg02_Persona>(persona);
            _personaRepository.Save();
        }

        void Actualizar(Sg02_Persona persona)
        {
            _personaRepository.Update<Sg02_Persona>(persona);
            _personaRepository.Save();
        }

        void Eliminar(Sg02_Persona persona)
        {
            _personaRepository.Delete<Sg02_Persona>(persona.Id);
            _personaRepository.Save();
        }

       
        #endregion


    }
}
