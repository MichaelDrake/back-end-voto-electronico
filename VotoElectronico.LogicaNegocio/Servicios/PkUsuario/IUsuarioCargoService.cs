﻿
using VotoElectronico.Entidades.Pk.PkSeguridad;
using VotoElectronico.Generico;

namespace VotoElectronico.LogicaNegocio.Servicios
{
    public interface IUsuarioCargoService
    {

        void ValidarInsertarUsuarioCargo(DtoUsuarioCargo dto, string token, string usuarioCreacion = "");
        void InsertarUsuarioCargoImportacion(DtoUsuarioCargo dto, string token, string usuarioCreacion = "");

    }
}
