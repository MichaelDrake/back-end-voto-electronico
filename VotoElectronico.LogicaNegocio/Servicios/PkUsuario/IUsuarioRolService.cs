﻿
using VotoElectronico.Entidades.Pk.PkSeguridad;
using VotoElectronico.Generico;

namespace VotoElectronico.LogicaNegocio.Servicios
{
    public interface IUsuarioRolService
    {
        void ValidarInsertarUsuarioRol(DtoUsuarioRol dto, string token, string usuarioCreacion = "");

        long obtenerRolIdbyToken(string token);

        void InsertarUsuarioRol(DtoUsuarioRol dto, string token, string usuarioCreacion = "");
        void InsertarUsuarioRolImportacion(DtoUsuarioRol dto, string token, string usuarioCreacion = "");

    }
}
