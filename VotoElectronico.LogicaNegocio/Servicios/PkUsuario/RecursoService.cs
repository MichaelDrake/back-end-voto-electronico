﻿using EcVotoElectronico.Repositorios;
using System.Collections.Generic;
using System.Linq;
using VotoElectronico.Entidades.Pk.PkSeguridad;
using VotoElectronico.Generico;
using VotoElectronico.Generico.Configs;
using VotoElectronico.Generico.Enumeraciones;
using VotoElectronico.Generico.Propiedades;
using VotoElectronico.Util;

namespace VotoElectronico.LogicaNegocio.Servicios
{
    public class RecursoService : IRecursoService
    {

        private readonly IRecursoRepository _RecursoRepository;
        private IApiResponseMessage _apiResponseMessage;
        private ISesionService _sesionService;

        public RecursoService(IRecursoRepository RecursoRepository, IApiResponseMessage apiResponseMessage, ISesionService sesionService)
        {
            _RecursoRepository = RecursoRepository;
            _apiResponseMessage = apiResponseMessage;
            _sesionService = sesionService;
        }

        public DtoApiResponseMessage ObtenerMenuUsuarioPorToken(string token)
        {
            var dtoMapeado = ObtenerMenuUsuario(token);
            if (dtoMapeado?.Any() ?? false)
                return _apiResponseMessage.CrearDtoApiResponseMessage(dtoMapeado, "VE_PEL_LIS_005");
            else
                return _apiResponseMessage.CrearDtoApiResponseMessage(dtoMapeado, "VE_PEL_LIS_006");
        }

        public DtoApiResponseMessage ObtenerRecursosMenuPrincipales()
        {
            var CodigoMenu = Enumeration.ObtenerDescripcion(TipoRecurso.Menu);
            var menus = _RecursoRepository.Get<Sg06_Recurso>(x => x.Estado.Equals(Auditoria.EstadoActivo) && x.RecursoId == null && x.CodigoRecurso.Equals(CodigoMenu));
            var dtoMapeado = ConvertirListaEntidadListaGenerica(menus);
            if (menus?.Any() ?? false)
                return _apiResponseMessage.CrearDtoApiResponseMessage(dtoMapeado, "VE_PEL_LIS_005");
            else
                return _apiResponseMessage.CrearDtoApiResponseMessage(dtoMapeado, "VE_PEL_LIS_006");
        }
        public DtoApiResponseMessage ObtenerRecursosMenuPrincipalesSimple()
        {
            var CodigoMenu = Enumeration.ObtenerDescripcion(TipoRecurso.Menu);
            var menus = _RecursoRepository.Get<Sg06_Recurso>(x => x.Estado.Equals(Auditoria.EstadoActivo) && x.RecursoId == null && x.CodigoRecurso.Equals(CodigoMenu));
            var dtoMapeado = ConvertirListaEntidadListaGenericaSimple(menus);
            if (menus?.Any() ?? false)
                return _apiResponseMessage.CrearDtoApiResponseMessage(dtoMapeado, "VE_PEL_LIS_005");
            else
                return _apiResponseMessage.CrearDtoApiResponseMessage(dtoMapeado, "VE_PEL_LIS_006");
        }

        public DtoApiResponseMessage ObtenerRecursosRutaPrincipalesSimple()
        {
            var CodigoRuta = Enumeration.ObtenerDescripcion(TipoRecurso.Ruta);
            var rutas = _RecursoRepository.Get<Sg06_Recurso>(x => x.Estado.Equals(Auditoria.EstadoActivo) && x.CodigoRecurso.Equals(CodigoRuta));
            var dtoMapeado = ConvertirListaEntidadListaGenericaRuta(rutas);
            if (rutas?.Any() ?? false)
                return _apiResponseMessage.CrearDtoApiResponseMessage(dtoMapeado, "VE_PEL_LIS_005");
            else
                return _apiResponseMessage.CrearDtoApiResponseMessage(dtoMapeado, "VE_PEL_LIS_006");
        }



        public IEnumerable<DtoRecurso> ObtenerRecursosMenuPrincipalesRol(Sg07_Rol rol)
        {
            var CodigoMenu = Enumeration.ObtenerDescripcion(TipoRecurso.Menu);
            var menus = rol?.Permisos?.Where(x => x.Estado.Equals(Auditoria.EstadoActivo) && x.Recurso.RecursoId == null && x.Recurso.CodigoRecurso.Equals(CodigoMenu))?.Select(y => y.Recurso);
            return ConvertirListaEntidadListaGenericaSimple(menus);
        }

        public IEnumerable<DtoRecurso> ObtenerRecursosMenuPrincipalesRolRuta(Sg07_Rol rol)
        {
            var CodigoRuta = Enumeration.ObtenerDescripcion(TipoRecurso.Ruta);
            var menus = rol?.Permisos?.Where(x => x.Estado.Equals(Auditoria.EstadoActivo) && x.Recurso.CodigoRecurso.Equals(CodigoRuta))?.Select(y => y.Recurso);
            return ConvertirListaEntidadListaGenerica(menus);
        }


        public DtoApiResponseMessage ObtenerRecursosRuta()
        {
            var CodigoRuta = Enumeration.ObtenerDescripcion(TipoRecurso.Ruta);
            var menus = _RecursoRepository.Get<Sg06_Recurso>(x => x.Estado.Equals(Auditoria.EstadoActivo) && x.CodigoRecurso.Equals(CodigoRuta));
            var dtoMapeado = ConvertirListaEntidadListaGenericaRuta(menus);
            if (menus?.Any() ?? false)
                return _apiResponseMessage.CrearDtoApiResponseMessage(dtoMapeado, "VE_PEL_LIS_005");
            else
                return _apiResponseMessage.CrearDtoApiResponseMessage(dtoMapeado, "VE_PEL_LIS_006");
        }




        IEnumerable<Sg06_Recurso> ObtenerRecusosPadresUsuarioToken(string token)
        {
            var usuario = _sesionService.ObtenerUsuarioPorToken(token);
            var CodigoMenu = Enumeration.ObtenerDescripcion(TipoRecurso.Menu);
            var respuestaRecurso = new List<Sg06_Recurso>();
            if (usuario != null)
            {
                var rol = usuario.UsuariosRol?.FirstOrDefault(x => x.Estado.Equals(Auditoria.EstadoActivo))?.Rol;
                if (rol != null)
                {
                    var recursoRoles = rol.Permisos?.Where(y => y.Recurso.CodigoRecurso.Equals(CodigoMenu) && y.Recurso.Estado.Equals(Auditoria.EstadoActivo) && y.Estado.Equals(Auditoria.EstadoActivo));
                    respuestaRecurso = recursoRoles?.Where(a => a.Recurso.RecursoId == null)?.Select(e => e.Recurso)?.ToList();
                }
            }
            return respuestaRecurso?.OrderBy(x => x.Orden);
        }

        IEnumerable<DtoRecurso> ObtenerMenuUsuario(string token)
        {
            var listaMenu = new List<DtoRecurso>();
            var recursosPadresUsuario = ObtenerRecusosPadresUsuarioToken(token);
            if (recursosPadresUsuario != null)
            {
                var CodigoMenu = Enumeration.ObtenerDescripcion(TipoRecurso.Menu);
                foreach (var recursoUsuario in recursosPadresUsuario)
                    listaMenu.Add(ConvertirEntidadGenerico(recursoUsuario, CodigoMenu));
            }
            return listaMenu;
        }

        DtoRecurso ConvertirEntidadGenerico(Sg06_Recurso recurso, string CodigoMenu)
        {
            var recursosHijos = _RecursoRepository.Get<Sg06_Recurso>(x => x.CodigoRecurso.Equals(CodigoMenu) && x.Estado.Equals(Auditoria.EstadoActivo) && x.RecursoId == recurso.Id)?.OrderBy(x => x.Orden);
            var recursoRespuesta = new DtoRecurso()
            {
                nombreRecurso = recurso.NombreRecurso,
                RutaMenu = recurso.RutaMenu,
                RecursosHijo = recursosHijos?.Select(x => ConvertirEntidadGenerico(x, CodigoMenu))
            };
            if (recurso.RecursoId == null)
            {
                recursoRespuesta.ToolTip = recurso.NombreRecurso;
                recursoRespuesta.Tipo = "dropDown";
                recursoRespuesta.Icono = recurso.Icono;
            }
            return recursoRespuesta;
        }


        IEnumerable<DtoRecurso> ConvertirListaEntidadListaGenerica(IEnumerable<Sg06_Recurso> recursos)
        {
            return recursos?.Select(x => new DtoRecurso()
            {
                Id = x.Id,
                nombreRecurso = $"{x.NombreRecurso} - {x.RutaMenu}",
            });
        }
        IEnumerable<DtoRecurso> ConvertirListaEntidadListaGenericaSimple(IEnumerable<Sg06_Recurso> recursos)
        {
            return recursos?.Select(x => new DtoRecurso()
            {
                Id = x.Id,
                nombreRecurso = $"{x.NombreRecurso}",
            });
        }

        IEnumerable<DtoRecurso> ConvertirListaEntidadListaGenericaRuta(IEnumerable<Sg06_Recurso> recursos)
        {
            return recursos?.Select(x => new DtoRecurso()
            {
                Id = x.Id,
                nombreRecurso = $"{x.ValorRecursoString}",
            });
        }
    }
}
