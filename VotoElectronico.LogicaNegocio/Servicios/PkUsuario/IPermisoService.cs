﻿using System.Collections.Generic;
using System.Security;
using VotoElectronico.Entidades.Pk.PkSeguridad;
using VotoElectronico.Generico;

namespace VotoElectronico.LogicaNegocio.Servicios
{
    public interface IPermisoService
    {

        bool ValidarRutaMedianteRol(DtoPermiso dto, string token);
        DtoApiResponseMessage CrearPermisoMenu(long rolId, IEnumerable<DtoRecurso> recursos, string token, bool esRuta = false);

        bool validarAccesoAMesaDeVoto(DtoPermiso dto, string token);
    }
}
