﻿using EcVotoElectronico.Repositorios;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using VotoElectronico.Api.Controllers;
using VotoElectronico.Entidades.Pk.PkSeguridad;
using VotoElectronico.Generico;
using VotoElectronico.Generico.Configs;
using VotoElectronico.Generico.Enumeraciones;
using VotoElectronico.Generico.Propiedades;
using VotoElectronico.LogicaCondicional;
using VotoElectronico.LogicaNegocio.Servicios.GoogleDrive;
using VotoElectronico.Mailer;
using VotoElectronico.Util;

namespace VotoElectronico.LogicaNegocio.Servicios
{
    public class UsuarioService : IUsuarioService
    {

        private readonly IUsuarioRepository _usuarioRepository;
        private readonly IPersonaRepository _personaRepository;
        private readonly IPersonaService _personaService;
        private readonly ISesionService _sesionService;
        private IApiResponseMessage _apiResponseMessage;
        private readonly IEnvioEmail _envioEmail;
        private readonly IUsuarioRolService _usuarioRolService;
        private readonly IUsuarioCargoService _usuarioCargoService;
        private readonly IGoogleDriveService _googleDriveService;
        private readonly string pathUsuarios = ConfigurationManager.AppSettings["path-usuarios"];

        public UsuarioService(IUsuarioRepository usuarioRepository,
            IApiResponseMessage apiResponseMessage,
            ISesionService sesionService,
            IEnvioEmail envioEmail,
            IPersonaRepository personaRepository,
            IUsuarioRolService usuarioRolService,
            IPersonaService personaService,
            IGoogleDriveService googleDriveService,
            IUsuarioCargoService usuarioCargoService
            )
        {
            _usuarioRepository = usuarioRepository;
            _personaRepository = personaRepository;
            _apiResponseMessage = apiResponseMessage;
            _envioEmail = envioEmail;
            _sesionService = sesionService;
            _usuarioRolService = usuarioRolService;
            _personaService = personaService;
            _googleDriveService = googleDriveService;
            _usuarioCargoService = usuarioCargoService;
        }

        #region Métodos Publicos


        public DtoApiResponseMessage ObtenerListaUsuariosByParametros(DtoEspecificacion dto)
        {
            var spec = new Sg01_UsuarioCondicional().FiltrarListaUsuariosByParams(dto);
            var dtoMapeado = MapearListaEntidadADto(_usuarioRepository.FiltrarUsuario(spec, dto.Pagina, out int total), total);

            if (dtoMapeado?.Any() ?? false)
            {
                return _apiResponseMessage.CrearDtoApiResponseMessage(dtoMapeado, "VE_SEG_USR_005");
            }
            else
            {
                return _apiResponseMessage.CrearDtoApiResponseMessage(null, "VE_SEG_USR_006");
            }

        }

        public IEnumerable<DtoPersona> ObtenerListaUsuariosActivosMedianteNombreUsuario(string nombreUsuario)
        {
            var RolAdministrador = Enumeration.ObtenerDescripcion(Roles.Administrador);

            var listaUsuariosActivos = _usuarioRepository.Get<Sg01_Usuario>(
                usuario => usuario.NombreUsuario.Contains(nombreUsuario)
                && usuario.Estado.Equals(Auditoria.EstadoActivo)
                && usuario.Persona.Estado.Equals(Auditoria.EstadoActivo)
                && !usuario.UsuariosRol.Any(rol => rol.Rol.NombreRol.Equals(RolAdministrador))
                );

            
            
            //if ((dtoMapeado?.Count() ?? 0) != 0)
            if(listaUsuariosActivos?.Any() ?? false)
            {
                var dtoMapeado = MapearListaUsuariosAutocomplete(listaUsuariosActivos);
                //return _apiResponseMessage.CrearDtoApiResponseMessage(listaUsuariosActivos, "VE_SEG_USR_005");
                return dtoMapeado;
            }
            else
            {
                //return _apiResponseMessage.CrearDtoApiResponseMessage(null, "VE_SEG_USR_006");
                return null; 
            }

        }

        
        public DtoApiResponseMessage ReenviarEmailUsuario(DtoUsuario dto)
        {
            var usuario = _usuarioRepository.GetById<Sg01_Usuario>(dto.Id);
            if (usuario.Estado.Equals(Auditoria.EstadoActivo))
            {
                if (usuario.Persona?.Estado?.Equals(Auditoria.EstadoActivo) ?? false)
                {
                    if (!usuario.EnvioEmailActivacion)
                    {
                        using var transaction = new TransactionScope();
                        try
                        {
                            usuario.TokenCambioClave = TokenGenerator.GenerateTokenJwt(usuario.NombreUsuario, DateTime.Now.ToShortTimeString());
                            Actualizar(usuario);
                            EnvioEmailUsuario(usuario, _personaRepository.GetOneOrDefault<Sg02_Persona>(x => x.Id == usuario.PersonaId));
                            transaction.Complete();
                        }
                        catch (Exception err)
                        {
                            throw err;
                        }
                        finally
                        {
                            transaction.Dispose();
                        }
                        return _apiResponseMessage.CrearDtoApiResponseMessage(null, "VE_SEG_USR_011");
                    }
                    throw new Exception("Usuario ya se encuantra actvado");
                }
            }
            throw new Exception("Usuario no disponible");
        }



        public DtoApiResponseMessage ObtenerUsuarioMedianteId(DtoUsuario dto)
        {
            var usuario = ObtenerUsuarioId(dto.Id);
            var dtoMapeado = mapearEntidadADto(usuario);
            return _apiResponseMessage.CrearDtoApiResponseMessage(dtoMapeado, "VE_SEG_USR_004");
        }

        public DtoApiResponseMessage ObtenerUsuarioMedianteToken(string token)
        {
            var usuario = ObtenerUsuarioToken(token);
            if (usuario != null)
            {
                var dtoMapeado = mapearEntidadADtoConInfoPersona(usuario);
                return _apiResponseMessage.CrearDtoApiResponseMessage(dtoMapeado, "VE_SEG_USR_004");
            }
            else
            {

                return _apiResponseMessage.CrearDtoApiResponseMessage(null, "VE_SEG_USR_006");
            }
        }

        public DtoApiResponseMessage CrearUsuarioFromWeb(DtoUsuario dto, string token)
        {
            var usuarioActivo = _usuarioRepository.GetOneOrDefault<Sg01_Usuario>(x => x.NombreUsuario.Equals(dto.nombreUsuario) && x.Estado.Equals(Auditoria.EstadoActivo));

            if (usuarioActivo != null)
            {
                return _apiResponseMessage.CrearDtoApiResponseMessage(null, "VE_SEG_USR_009");
            }

            var usuario = new Sg01_Usuario();
            using var transaccion = new TransactionScope();
            try
            {
                usuario = ValidarInsertarUsuario(new DtoUsuario()
                {
                    nombreUsuario = dto.nombreUsuario,
                    PersonaId = dto.PersonaId,
                    Clave = UtilEncodeDecode.Sha256Hash("generic_user_" + dto.nombreUsuario),

                }, token);

                _usuarioRolService.InsertarUsuarioRol(new DtoUsuarioRol() { UsuarioId = usuario.Id, RolId = dto.rolId }, token);
                _usuarioCargoService.ValidarInsertarUsuarioCargo(new DtoUsuarioCargo() { usuarioId = usuario.Id, cargoId = dto.cargoId }, token);
                transaccion.Complete();

            }
            catch (Exception ex)
            {
                return _apiResponseMessage.CrearDtoApiResponseMessage(null, "VE_SEG_USR_008");
            }
            finally
            {
                transaccion.Dispose();

            }

            return _apiResponseMessage.CrearDtoApiResponseMessage(mapearEntidadADto(usuario), "VE_SEG_USR_001");

        }

        public DtoApiResponseMessage CrearUsuario(DtoUsuario dto, string token)
            => _apiResponseMessage.CrearDtoApiResponseMessage(mapearEntidadADto(InsertarUsuario(dto, token, false)), "VE_SEG_USR_001");

        public Sg01_Usuario InsertarUsuario(DtoUsuario dto, string token, bool envioEmail)
        {

            var usuario = mapearDtoAEntidad(dto, token);
            if (envioEmail)
                usuario.TokenCambioClave = TokenGenerator.GenerateTokenJwt(usuario.NombreUsuario, DateTime.Now.ToShortTimeString());

            Crear(usuario);
            if (envioEmail)
            {
                Task.Factory.StartNew(() =>
               EnvioEmailUsuario(usuario, _personaRepository.GetOneOrDefault<Sg02_Persona>(x => x.Id == usuario.PersonaId))
               );
            }
            return usuario;
        }


        public Sg01_Usuario InsertarUsuarioImportacionPadron(DtoUsuario dto, Sg02_Persona persona, string usuarioCreacion)
        {
            var usuario = mapearDtoAEntidad(dto, string.Empty, usuarioCreacion);
            usuario.TokenCambioClave = TokenGenerator.GenerateTokenJwt(usuario.NombreUsuario, DateTime.Now.ToShortTimeString());
            Crear(usuario);
            // Task.Factory.StartNew(() =>
            //EnvioEmailUsuario(usuario, persona)
            //);
            return usuario;
        }


        Sg01_Usuario ValidarInsertarUsuario(DtoUsuario dto, string token)
        {
            var usuario = _usuarioRepository.GetOneOrDefault<Sg01_Usuario>(x => x.NombreUsuario.Equals(dto.nombreUsuario));
            if (usuario != null)
            {
                if (usuario.Estado.Equals(Auditoria.EstadoInactivo))
                {
                    ReactivarUsuario(usuario.Id, token);
                }
                return usuario;
            }
            else
                return InsertarUsuario(dto, token, envioEmail: true);
        }
        public Sg01_Usuario ValidarInsertarUsuarioImportacionPadron(DtoUsuario dto, Sg02_Persona persona, string usuarioCreacion, out bool nuevo)
        {

            var usuario = _usuarioRepository.GetOneOrDefault<Sg01_Usuario>(x => x.NombreUsuario.Equals(dto.nombreUsuario));
            if (usuario != null)
            {
                if (usuario.Estado.Equals(Auditoria.EstadoInactivo))
                {
                    ReactivarUsuario(usuario.Id, string.Empty, usuarioCreacion);
                }
                nuevo = false;
                return usuario;
            }
            else
            {
                nuevo = true;
                return InsertarUsuarioImportacionPadron(dto, persona, usuarioCreacion);
            }

        }



        public DtoApiResponseMessage ActualizarUsuario(DtoUsuario dto, string token)
        {
            var usuario = ObtenerUsuarioId(dto.Id);
            //usuario.Email = dto.Email;
            usuario.EnvioEmailActivacion = dto.EnvioEmailActivacion;
            usuario.Clave = dto.Clave;
            usuario.NombreUsuario = dto.nombreUsuario;
            usuario.Estado = dto.estado;
            usuario.UsuarioModificacion = _sesionService.ObtenerUsuarioPorToken(token)?.NombreUsuario;
            usuario.FechaModificacion = DateTime.Now;
            usuario.Token = dto.Token;
            usuario.PersonaId = dto.PersonaId;
            Actualizar(usuario);

            var dtoMapeado = mapearEntidadADto(usuario);
            return _apiResponseMessage.CrearDtoApiResponseMessage(dtoMapeado, "VE_SEG_USR_002");
        }

        public DtoApiResponseMessage ActualizarUsuarioWeb(DtoUsuario dto, string token)
        {

            var usuario = ObtenerUsuarioId(dto.Id);

            if (usuario != null)
            {
                if (dto.fotoObjeto != null)
                    dto.fotoUrl = _googleDriveService.UploadBase64(dto.fotoObjeto.base64, dto.fotoObjeto.tipo, dto.fotoObjeto.extension, pathUsuarios);
                else
                {
                    dto.fotoUrl = usuario.ImagenUsuario;
                }

                usuario.UsuarioModificacion = _sesionService.ObtenerUsuarioPorToken(token)?.NombreUsuario;
                usuario.FechaModificacion = DateTime.Now;
                usuario.ImagenUsuario = dto.fotoUrl;
                //usuario.Token = _sesionService.ObtenerUsuarioPorToken(token)?.NombreUsuario;
                //usuario.PersonaId = dto.PersonaId;
                using var transaccion = new TransactionScope();
                try
                {
                    Actualizar(usuario);
                    if (dto.rolId != 0) 
                    {
                        _usuarioRolService.InsertarUsuarioRol(new DtoUsuarioRol() { UsuarioId = usuario.Id, RolId = dto.rolId }, token);
                    }
                    if (dto.cargoId != 0)
                    {
                        _usuarioCargoService.ValidarInsertarUsuarioCargo(new DtoUsuarioCargo() { usuarioId = usuario.Id, cargoId = dto.cargoId }, token);
                    }
                    transaccion.Complete();

                }
                catch (Exception ex)
                {
                    return _apiResponseMessage.CrearDtoApiResponseMessage(null, "VE_SEG_USRL_003");
                }
                finally
                {
                    transaccion.Dispose();

                }
                return _apiResponseMessage.CrearDtoApiResponseMessage(mapearEntidadADto(usuario), "VE_SEG_USR_002");
            }
            else
            {
                return _apiResponseMessage.CrearDtoApiResponseMessage(null, "VE_SEG_USR_007");
            }

        }




        //public void EliminarUsuario(DtoUsuario dto)
        //{
        //    //var usuario = ObtenerUsuarioId(id);
        //    //usuario.Estado = "INACTIVO";
        //    //Actualizar(usuario);
        //    //_usuarioRepository.Delete<Sg01_Usuario>(dto.Id);


        public DtoApiResponseMessage EliminarUsuario(DtoUsuario dto)
        {
            var usuario = ObtenerUsuarioId(dto.Id);
            _usuarioRepository.Delete<Sg01_Usuario>(usuario.Id);
            _usuarioRepository.Save();

            var dtoMapeado = mapearEntidadADto(usuario);
            return _apiResponseMessage.CrearDtoApiResponseMessage(dtoMapeado, "VE_SEG_USR_003");
        }

        public DtoApiResponseMessage CambiarEstadoUsuario(DtoUsuario dto, string token)
        {
            var usuario = ObtenerUsuarioId(dto.Id);

            if (usuario != null)
            {
                usuario.Estado = dto.estado;
                usuario.UsuarioModificacion = _sesionService.ObtenerUsuarioPorToken(token)?.NombreUsuario;
                usuario.FechaModificacion = DateTime.Now;
                Actualizar(usuario);

                var dtoMapeado = mapearEntidadADto(usuario);
                if (dto.estado.Equals(Auditoria.EstadoInactivo))
                {
                    return _apiResponseMessage.CrearDtoApiResponseMessage(dtoMapeado, "VE_SEG_USR_003");
                }
                else
                {
                    return _apiResponseMessage.CrearDtoApiResponseMessage(dtoMapeado, "VE_SEG_USR_010");
                }

            }
            else
            {
                return _apiResponseMessage.CrearDtoApiResponseMessage(null, "VE_SEG_USR_007");
            }
        }

        public void ReactivarUsuario(long id, string token, string nombreUsuario = "")
        {
            var usuario = ObtenerUsuarioId(id);
            usuario.Estado = "ACTIVO";
            usuario.UsuarioModificacion = !string.IsNullOrEmpty(nombreUsuario) ? nombreUsuario : _sesionService.ObtenerUsuarioPorToken(token)?.NombreUsuario;
            usuario.FechaModificacion = DateTime.Now;
            Actualizar(usuario);
        }




        public void EnvioEmailUsuario(Sg01_Usuario usuario, Sg02_Persona persona)
        {
            if (usuario != null && persona != null)
            {
                var datos = new Dictionary<string, string>
                    {
                        { "0",$"{persona?.NombreUno} {persona?.ApellidoUno}"},
                        { "1", $"{ConfigurationManager.AppSettings["dominio"]}/sessions/cambioclave?tkn={usuario.TokenCambioClave}" },
                    };
                _envioEmail.EnviarEmail(persona.Email, "EVOTE EPN - nuevo usuario", _envioEmail.ActivarUsuarioGenerico(datos));
            }
        }
        public void EnvioMasivoEmails(List<ObjetoEnvioEmailImportacion> listaObjetoEnvioEmail)
        {
            listaObjetoEnvioEmail?.ForEach(x =>
            {
                EnvioEmailUsuario(x.Usuario, x.Persona);
            });
        }

        public DtoApiResponseMessage VaciarToken(string token)
        {
            if (!string.IsNullOrEmpty(token))
            {
                var usuario = _usuarioRepository.GetOneOrDefault<Sg01_Usuario>(x => x.Estado.Equals(Auditoria.EstadoActivo) && x.InicioSesion && x.Token.Equals(token));
                if (usuario != null)
                {
                    usuario.Token = string.Empty;
                    usuario.InicioSesion = false;
                    Actualizar(usuario);
                }
                return _apiResponseMessage.CrearDtoApiResponseMessage(null, "VE_SSN_INS_001");
            }
            return _apiResponseMessage.CrearDtoApiResponseMessage(null, "VE_SSN_INS_005");
        }

        #endregion

        #region Métodos Privados
        Sg01_Usuario ObtenerUsuarioId(long id)
        => _usuarioRepository.GetById<Sg01_Usuario>(id);

        Sg01_Usuario ObtenerUsuarioToken(string token)
        => _usuarioRepository.GetOneOrDefault<Sg01_Usuario>(usuario => usuario.Token.Equals(token));

        Sg01_Usuario mapearDtoAEntidad(DtoUsuario dto, string token, string usuarioCreacion = "")
            => new Sg01_Usuario()
            {
                NombreUsuario = dto.nombreUsuario,
                Clave = dto.Clave,
                PersonaId = dto.PersonaId,
                Estado = Auditoria.EstadoActivo,
                UsuarioCreacion = !string.IsNullOrEmpty(usuarioCreacion) ? usuarioCreacion : _sesionService.ObtenerUsuarioPorToken(token)?.NombreUsuario,
                FechaCreacion = DateTime.Now,
                ImagenUsuario = ConfigurationManager.AppSettings["path-user-image"]
            };


        IEnumerable<DtoUsuario> mapearEntidadADto(Sg01_Usuario usuario)
        {
            var persona = _personaRepository.GetOne<Sg02_Persona>(persona => persona.Id.Equals(usuario.PersonaId));
            var dtoPersona = mapearEntidadPersonaADto(persona);

            DtoUsuario dto = new DtoUsuario();
            dto.Id = usuario.Id;
            //dto.Email = usuario.Email;
            dto.EnvioEmailActivacion = usuario.EnvioEmailActivacion;
            dto.Clave = usuario.Clave;
            dto.nombreUsuario = usuario.NombreUsuario;
            dto.usuarioCreacion = usuario.UsuarioCreacion;
            dto.fechaCreacion = usuario?.FechaCreacion ?? DateTime.MinValue;
            dto.fechaModificacion = usuario?.FechaModificacion ?? DateTime.MinValue;
            dto.Token = usuario.Token;
            dto.PersonaId = usuario.PersonaId;
            dto.fotoUrl = string.IsNullOrEmpty(usuario.ImagenUsuario) ? null : $"{CtEstaticas.StrGoogleDrive}{usuario.ImagenUsuario}";
            dto.objetoPersona = dtoPersona;

            List<DtoUsuario> lista = new List<DtoUsuario>();
            lista.Add(dto);
            return lista;
        }

        IEnumerable<DtoUsuario> mapearEntidadADtoConInfoPersona(Sg01_Usuario usuario)
        {
            var persona = _personaRepository.GetOne<Sg02_Persona>(persona => persona.Id.Equals(usuario.PersonaId));
            var dtoPersona = mapearEntidadPersonaADto(persona);

            DtoUsuario dto = new DtoUsuario();
            dto.Id = usuario.Id;
            dto.PersonaId = usuario.PersonaId;
            dto.fotoUrl = string.IsNullOrEmpty(usuario.ImagenUsuario) ? null : $"{CtEstaticas.StrGoogleDrive}{usuario.ImagenUsuario}";
            dto.objetoPersona = dtoPersona;

            List<DtoUsuario> lista = new List<DtoUsuario>();
            lista.Add(dto);
            return lista;
        }

        IEnumerable<DtoUsuario> MapearListaEntidadGenerico(IEnumerable<Sg01_Usuario> usuario)
        {
            return usuario.Select(x => new DtoUsuario()
            {
                nombreUsuario = x.NombreUsuario,
                //Email = x.Email
            });
        }

        IEnumerable<DtoPersona> MapearListaUsuariosAutocomplete(IEnumerable<Sg01_Usuario> listaUsuarios)
        {
            return listaUsuarios.Select(x => new DtoPersona()
            {
                dt1 = x.NombreUsuario,
                Id = x.Id,
                nombreUno = x.Persona.NombreUno,
                nombreDos = x.Persona.NombreDos,
                apellidoUno = x.Persona.ApellidoUno,
                apellidoDos = x.Persona.ApellidoDos,
                telefonoUno = x.Persona.Telefono,
                telefonoDos = x.Persona.Telefono2,
                estado = x.Estado,
                email = x.Persona.Email,
            });
        }


        IEnumerable<DtoUsuario> MapearListaEntidadADto(IEnumerable<Sg01_Usuario> usuario, int total)
        {
            return usuario?.Select(usuario => {

                var usuarioRol = usuario.UsuariosRol?.FirstOrDefault();
                var usuaroCargo = usuario.UsuariosCargo?.FirstOrDefault(x => x.Estado.Equals(Auditoria.EstadoActivo));

                return new DtoUsuario()
                {


                    nombreUsuario = usuario.NombreUsuario,
                    Email = usuario.Persona?.Email,
                    Id = usuario.Id,
                    estado = usuario.Estado,
                    PersonaId = usuario.PersonaId,
                    objetoPersona = mapearEntidadPersonaADto(usuario.Persona),
                    nombreRol = usuarioRol?.Rol?.NombreRol,
                    rolId = usuarioRol?.RolId ?? 0,
                    EnvioEmailActivacion = usuario.EnvioEmailActivacion,
                    nombreCargo = usuaroCargo?.Cargo?.NombreCargo,
                    cargoId = usuaroCargo?.CargoId ?? 0,
                    dt1 = usuario.NombreUsuario,
                    Total = total
                    //Email = x.Email
                };
            });
        } 
        

        DtoPersona mapearEntidadPersonaADto(Sg02_Persona Persona)
        {
            DtoPersona dto = new DtoPersona();
            dto.Id = Persona?.Id ?? 0;
            dto.nombreUno = Persona?.NombreUno;
            dto.nombreDos = Persona?.NombreDos;
            dto.apellidoUno = Persona?.ApellidoUno;
            dto.apellidoDos = Persona?.ApellidoDos;
            dto.identificacion = Persona?.Identificacion;
            dto.telefonoUno = Persona?.Telefono;
            dto.telefonoDos = Persona?.Telefono2;
            dto.estado = Persona?.Estado;
            dto.email = Persona?.Email;
            return dto;
        }


        void Crear(Sg01_Usuario usuario)
        {
            _usuarioRepository.Create<Sg01_Usuario>(usuario);
            _usuarioRepository.Save();
        }

        void Actualizar(Sg01_Usuario usuario)
        {
            _usuarioRepository.Update<Sg01_Usuario>(usuario);
            _usuarioRepository.Save();
        }

        void Eliminar(Sg01_Usuario usuario)
        {
            _usuarioRepository.Delete<Sg01_Usuario>(usuario.Id);
            _usuarioRepository.Save();
        }
        #endregion


    }
}
