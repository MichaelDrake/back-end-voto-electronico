﻿using EcVotoElectronico.Repositorios;
using System;
using System.Collections.Generic;
using VotoElectronico.Entidades.Pk.PkSeguridad;
using VotoElectronico.Generico;
using VotoElectronico.Generico.Propiedades;

namespace VotoElectronico.LogicaNegocio.Servicios
{
    public class UsuarioRolService : IUsuarioRolService
    {

        private readonly IUsuarioRolRepository _usuarioRolRepository;
        private readonly ISesionService _sesionService;

        public UsuarioRolService(IUsuarioRolRepository usuarioRolRepository, ISesionService sesionService)
        {
            _usuarioRolRepository = usuarioRolRepository;
            _sesionService = sesionService;
        }

        #region Métodos Publicos


        public void InsertarUsuarioRol(DtoUsuarioRol dto, string token, string usuarioCreacion = "")
        {
            var usuarioRol = _usuarioRolRepository.GetOneOrDefault<Sg08_UsuarioRol>(x => x.UsuarioId.Equals(dto.UsuarioId));
            if (usuarioRol != null)
            {
                usuarioRol.RolId = dto.RolId;
                usuarioRol.FechaModificacion = new DateTime();
                usuarioRol.UsuarioModificacion = !string.IsNullOrEmpty(usuarioCreacion) ? usuarioCreacion : _sesionService.ObtenerUsuarioPorToken(token)?.NombreUsuario;
                Actualizar(usuarioRol);
            }
            else
            {
                var usuarioRolEntidad = MapearDtoAEntidad(dto, token, usuarioCreacion);
                Crear(usuarioRolEntidad);
            }

        }

        public void InsertarUsuarioRolImportacion(DtoUsuarioRol dto, string token, string usuarioCreacion = "")
        {
            var usuarioRolEntidad = MapearDtoAEntidad(dto, token, usuarioCreacion);
            Crear(usuarioRolEntidad);
        }


        public void ValidarInsertarUsuarioRol(DtoUsuarioRol dto, string token, string usuarioCreacion = "")
        {
            var rolActual = _usuarioRolRepository.GetOneOrDefault<Sg08_UsuarioRol>(x => x.UsuarioId == dto.UsuarioId && x.Estado.Equals(Auditoria.EstadoActivo));
            if (rolActual == null)
            {
                var userRol = _usuarioRolRepository.GetOneOrDefault<Sg08_UsuarioRol>(x => x.RolId == dto.RolId && x.UsuarioId == dto.UsuarioId);
                if (userRol != null)
                {
                    if (userRol.Estado.Equals(Auditoria.EstadoInactivo))
                    {
                        ReactivarUsuarioRol(userRol, token, usuarioCreacion);
                    }
                }
                else
                    InsertarUsuarioRolImportacion(dto, token, usuarioCreacion);
            }
        }




        public void ReactivarUsuarioRol(Sg08_UsuarioRol usuarioRol, string token, string usuarioCreacion = "")
        {
            usuarioRol.Estado = Auditoria.EstadoActivo;
            usuarioRol.FechaCreacion = DateTime.Now;
            usuarioRol.UsuarioModificacion = !string.IsNullOrEmpty(usuarioCreacion) ? usuarioCreacion : _sesionService.ObtenerUsuarioPorToken(token)?.NombreUsuario;
            Actualizar(usuarioRol);
        }


        //Obtener el registro de usuarioRol mediante token de sesion

        public long obtenerRolIdbyToken(string token)
        {
            var usuarioRol = _usuarioRolRepository.GetOneOrDefault<Sg08_UsuarioRol>(usuarioRol => usuarioRol.Usuario.Token.Equals(token));

            return usuarioRol != null ? usuarioRol.RolId : 0;
        }

        #endregion

        #region Métodos Privados

        Sg08_UsuarioRol MapearDtoAEntidad(DtoUsuarioRol dto, string token, string usuarioCreacion = "")
            => new Sg08_UsuarioRol()
            {

                RolId = dto.RolId,
                UsuarioId = dto.UsuarioId,
                Estado = Auditoria.EstadoActivo,
                UsuarioCreacion = !string.IsNullOrEmpty(usuarioCreacion) ? usuarioCreacion : _sesionService.ObtenerUsuarioPorToken(token)?.NombreUsuario,
                FechaCreacion = DateTime.Now,
            };



        IEnumerable<DtoUsuarioRol> mapearEntidadADto(Sg08_UsuarioRol usuarioRol)
        {
            DtoUsuarioRol dto = new DtoUsuarioRol();
            dto.Id = usuarioRol.Id;
            dto.RolId = usuarioRol.RolId;
            dto.UsuarioId = usuarioRol.UsuarioId;

            List<DtoUsuarioRol> lista = new List<DtoUsuarioRol>();
            lista.Add(dto);
            return lista;
        }


        void Crear(Sg08_UsuarioRol usuarioRol)
        {
            _usuarioRolRepository.Create<Sg08_UsuarioRol>(usuarioRol);
            _usuarioRolRepository.Save();
        }

        void Actualizar(Sg08_UsuarioRol usuarioRol)
        {
            _usuarioRolRepository.Update<Sg08_UsuarioRol>(usuarioRol);
            _usuarioRolRepository.Save();
        }

        void Eliminar(long idusuarioRol)
        {
            _usuarioRolRepository.Delete<Sg08_UsuarioRol>(idusuarioRol);
            _usuarioRolRepository.Save();
        }
        #endregion


    }
}
