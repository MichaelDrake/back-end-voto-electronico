﻿
using System.Collections.Generic;
using VotoElectronico.Entidades.Pk.PkSeguridad;
using VotoElectronico.Generico;

namespace VotoElectronico.LogicaNegocio.Servicios
{
    public interface IRecursoService
    {
        DtoApiResponseMessage ObtenerMenuUsuarioPorToken(string token);
        DtoApiResponseMessage ObtenerRecursosMenuPrincipales();
        DtoApiResponseMessage ObtenerRecursosRuta();

        IEnumerable<DtoRecurso> ObtenerRecursosMenuPrincipalesRol(Sg07_Rol rol);
        DtoApiResponseMessage ObtenerRecursosMenuPrincipalesSimple();
        DtoApiResponseMessage ObtenerRecursosRutaPrincipalesSimple();
        IEnumerable<DtoRecurso> ObtenerRecursosMenuPrincipalesRolRuta(Sg07_Rol rol);

    }
}
