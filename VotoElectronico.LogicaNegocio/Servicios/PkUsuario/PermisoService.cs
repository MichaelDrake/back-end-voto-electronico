﻿using EcVotoElectronico.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using VotoElectronico.Entidades.Pk.PkMesaIdentificacion;
using VotoElectronico.Entidades.Pk.PkSeguridad;
using VotoElectronico.Generico;
using VotoElectronico.Generico.Configs;
using VotoElectronico.Generico.Enumeraciones;
using VotoElectronico.Generico.Propiedades;
using VotoElectronico.Util;

namespace VotoElectronico.LogicaNegocio.Servicios
{
    public class PermisoService : IPermisoService
    {

        private readonly IPermisoRepository _permisoRepository;
        private IApiResponseMessage _utilitarios;
        private readonly IUsuarioRolService _usuarioRolService;
        private readonly ISesionService _sesionService;
        private readonly IPadronVotacionRepository _padronVotacionRepository;

        public PermisoService(
            IPermisoRepository permisoRepository,
            IApiResponseMessage utilitarios,
            IUsuarioRolService usuarioRolService,
            ISesionService sesionService,
            IPadronVotacionRepository padronVotacionRepository

            )
        {
            _permisoRepository = permisoRepository;
            _utilitarios = utilitarios;
            _usuarioRolService = usuarioRolService;
            _sesionService = sesionService;
            _padronVotacionRepository = padronVotacionRepository;
        }

        public bool ValidarRutaMedianteRol(DtoPermiso dto, string token)
        {
            var rolId = _usuarioRolService.obtenerRolIdbyToken(token);
            return rolId != 0 ? obtenerPermisosMedianteCargo(rolId, dto.ruta) : false;
        }

        public bool validarAccesoAMesaDeVoto(DtoPermiso dto, string token)
        {
            string ruta = dto.ruta;
            //string[] substringsRuta = ruta.Split('/');
            var procesoIdDesencriptado = long.Parse(UtilEncodeDecode.Base64Decode(ruta));
            //var rutaSinProcesoId = 
            var existeUsuarioEnPadron = _padronVotacionRepository.GetExists<Mi01_PadronVotacion>(
                x => x.ProcesoElectoralId.Equals(procesoIdDesencriptado) 
                && x.Usuario.Token.Equals(token) 
                && x.Usuario.Estado.Equals(Auditoria.EstadoActivo)
                && x.Estado.Equals(Auditoria.EstadoActivo)
                && !x.VotoRealizado
            );

            //var rolId = _usuarioRolService.obtenerRolIdbyToken(token);
            //return rolId != 0 ? obtenerPermisosMedianteCargo(rolId, dto.ruta) : false;
            return existeUsuarioEnPadron;
        }

        public DtoApiResponseMessage CrearPermisoMenu(long rolId, IEnumerable<DtoRecurso> recursos, string token, bool esRuta = false)
        {
            var listaRecursosNuevo = new List<Sg05_Permiso>();
            var etiqueta = esRuta ? Enumeration.ObtenerDescripcion(TipoRecurso.Ruta) : Enumeration.ObtenerDescripcion(TipoRecurso.Menu);
            var listaPermisosRol = _permisoRepository.Get<Sg05_Permiso>(x => x.RolId == rolId && x.Recurso.CodigoRecurso.Equals(etiqueta));
            var userCreacion = _sesionService.ObtenerUsuarioPorToken(token)?.NombreUsuario;
            using var transaccion = new TransactionScope();
            try {
               
                recursos?.ToList()?.ForEach(x =>
                {
                    var permiso = listaPermisosRol?.ToList()?.Find(y => y.RecursoId == x.Id);
                    if (permiso != null)
                    {
                        if (permiso.Estado.Equals(Auditoria.EstadoInactivo))
                        {
                            permiso.Estado = Auditoria.EstadoActivo;
                            permiso.UsuarioModificacion = userCreacion;
                            permiso.FechaModificacion = DateTime.Now;
                            Actualizar(permiso);
                        }

                    }
                    else
                    {

                                listaRecursosNuevo.Add(
                                    new Sg05_Permiso()
                                    {
                                        RolId = rolId,
                                        RecursoId = x.Id,
                                        UsuarioCreacion = userCreacion,
                                        FechaCreacion = DateTime.Now,
                                        Estado = Auditoria.EstadoActivo
                                    });
                           
                    }
                });
                if(listaRecursosNuevo.Count() > 0)
                CrearLista(listaRecursosNuevo);
                var eliminados = listaPermisosRol?.Where(x => !recursos.ToList()?.Any(y => y.Id == x.RecursoId) ?? true);
                eliminados?.ToList()?.ForEach(x =>
                {
                    x.Estado = Auditoria.EstadoInactivo;
                    x.UsuarioModificacion = userCreacion;
                    x.FechaModificacion = DateTime.Now;
                    Actualizar(x);
                });

                transaccion.Complete();
            }
            catch (Exception err)
            {
                throw err;
            }
            finally {
                transaccion.Dispose();
            }
          
            return _utilitarios.CrearDtoApiResponseMessage(codigoMesaje: "VE_PRM_INS_001");
        }

        #region Métodos Publicos


        bool obtenerPermisosMedianteCargo(long rolId, string ruta)
        {
            return _permisoRepository.GetExists<Sg05_Permiso>(permiso => permiso.RolId.Equals(rolId)
            && permiso.Recurso.ValorRecursoString.Equals(ruta)
            && permiso.Recurso.CodigoRecurso.Equals("RUTA")
            );


        }


        #endregion


        //#region Métodos Privados
        //Sg03_Cargo ObtenerrolId(long id)
        //=> _cargoRepository.GetById<Sg03_Cargo>(id);

        //Sg03_Cargo mapearDtoAEntidad(DtoCargo dto)
        //    => new Sg03_Cargo()
        //    {
        //        NombreCargo = dto.nombreCargo,
        //        Estado = dto.estado,
        //        UsuarioCreacion = _ses,
        //        FechaCreacion = DateTime.Now,
        //    };

        //IEnumerable<DtoCargo> mapearEntidadADto(Sg03_Cargo Cargo)
        //{
        //    DtoCargo dto = new DtoCargo();
        //    dto.Id = Cargo.Id; ;
        //    dto.nombreCargo = Cargo.NombreCargo;
        //    dto.usuarioCreacion = Cargo.UsuarioCreacion;
        //    dto.usuarioModificacion = Cargo.UsuarioModificacion;
        //    dto.estado = Cargo.Estado;

        //    List<DtoCargo> lista = new List<DtoCargo>();
        //    lista.Add(dto);
        //    return lista;
        //}

        //IEnumerable<DtoCargo> MapearListaEntidadADtoCargo(IEnumerable<Sg03_Cargo> cargos)
        //{
        //    return cargos == null ?null: cargos.Select(x => new DtoCargo()
        //    {
        //        Id = x.Id,
        //        nombreCargo = x.NombreCargo,
        //        usuarioCreacion = x.UsuarioCreacion,
        //        usuarioModificacion = x.UsuarioModificacion,
        //        estado = x.Estado

        //    });
        //}


        void Crear(Sg05_Permiso Cargo)
        {
            _permisoRepository.Create<Sg05_Permiso>(Cargo);
            _permisoRepository.Save();
        }

        void Actualizar(Sg05_Permiso Cargo)
        {
            _permisoRepository.Update<Sg05_Permiso>(Cargo);
            _permisoRepository.Save();
        }

        //void Eliminar(long idCargo)
        //{
        //    _cargoRepository.Delete<Sg03_Cargo>(idCargo);
        //    _cargoRepository.Save();
        //}

        //public DtoApiResponseMessage ValidarRutaMedianteCargo(DtoUsuario dto)
        //{
        //    throw new NotImplementedException();
        //}

        void CrearLista(IEnumerable<Sg05_Permiso> Cargos)
        {
            _permisoRepository.CreateList<Sg05_Permiso>(Cargos);
            _permisoRepository.Save();
        }

    }
}
