﻿using System.Collections.Generic;
using VotoElectronico.Entidades.Pk.PkSeguridad;
using VotoElectronico.Generico;

namespace VotoElectronico.LogicaNegocio.Servicios
{
    public interface IUsuarioService
    {

        DtoApiResponseMessage ObtenerUsuarioMedianteId(DtoUsuario dto);

        DtoApiResponseMessage CrearUsuario(DtoUsuario dto, string token);
        

       DtoApiResponseMessage CrearUsuarioFromWeb(DtoUsuario dto, string token);

        Sg01_Usuario InsertarUsuario(DtoUsuario dto, string token, bool envioEmail);
        Sg01_Usuario InsertarUsuarioImportacionPadron(DtoUsuario dto, Sg02_Persona persona, string usuarioCreacion);

        Sg01_Usuario ValidarInsertarUsuarioImportacionPadron(DtoUsuario dto, Sg02_Persona persona, string usuarioCreacion, out bool nuevo);

        DtoApiResponseMessage ActualizarUsuario(DtoUsuario dto, string token);

        DtoApiResponseMessage ActualizarUsuarioWeb(DtoUsuario dto, string token);

        DtoApiResponseMessage EliminarUsuario(DtoUsuario dto);

        DtoApiResponseMessage CambiarEstadoUsuario(DtoUsuario dto, string token);


        DtoApiResponseMessage ObtenerListaUsuariosByParametros(DtoEspecificacion dto);
        IEnumerable<DtoPersona> ObtenerListaUsuariosActivosMedianteNombreUsuario(string nombreUsario);

        void EnvioEmailUsuario(Sg01_Usuario usuarios, Sg02_Persona persona);

        DtoApiResponseMessage ObtenerUsuarioMedianteToken(string token);

        DtoApiResponseMessage ReenviarEmailUsuario(DtoUsuario dto);
        DtoApiResponseMessage VaciarToken(string token);
        void EnvioMasivoEmails(List<ObjetoEnvioEmailImportacion> listaObjetoEnvioEmail);
    }
}
