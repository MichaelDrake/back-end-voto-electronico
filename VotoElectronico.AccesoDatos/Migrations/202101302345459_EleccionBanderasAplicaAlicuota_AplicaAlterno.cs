﻿namespace VotoElectronico.AccesoDatos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EleccionBanderasAplicaAlicuota_AplicaAlterno : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Pe02_Eleccion", "AplicaCandidatoAlterno", c => c.Boolean(nullable: false));
            AddColumn("dbo.Pe02_Eleccion", "AplicaAlicuotaCargo", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Pe02_Eleccion", "AplicaAlicuotaCargo");
            DropColumn("dbo.Pe02_Eleccion", "AplicaCandidatoAlterno");
        }
    }
}
