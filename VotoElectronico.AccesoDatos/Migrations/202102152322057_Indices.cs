﻿namespace VotoElectronico.AccesoDatos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Indices : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Mi01_PadronVotacion", new[] { "ProcesoElectoralId" });
            DropIndex("dbo.Mi01_PadronVotacion", new[] { "UsuarioId" });
            DropIndex("dbo.Pe06_Candidato", new[] { "ListaId" });
            DropIndex("dbo.Pe06_Candidato", new[] { "PersonaId" });
            DropIndex("dbo.Sg01_Usuario", new[] { "PersonaId" });
            DropIndex("dbo.Sg04_UsuarioCargo", new[] { "CargoId" });
            DropIndex("dbo.Sg04_UsuarioCargo", new[] { "UsuarioId" });
            DropIndex("dbo.Pe05_Lista", new[] { "ProcesoElectoralId" });
            DropIndex("dbo.Sg08_UsuarioRol", new[] { "RolId" });
            DropIndex("dbo.Sg08_UsuarioRol", new[] { "UsuarioId" });
            RenameIndex(table: "dbo.Pe04_Escanio", name: "IX_NombreEleccion", newName: "IX_EscanioEleccion");
            AlterColumn("dbo.Pe05_Lista", "NombreLista", c => c.String(nullable: false, maxLength: 200));
            CreateIndex("dbo.Mi01_PadronVotacion", new[] { "ProcesoElectoralId", "UsuarioId" }, unique: true, name: "IX_ProcesoELusuario");
            CreateIndex("dbo.Pe06_Candidato", new[] { "ListaId", "PersonaId" }, unique: true, name: "IX_ListaPersona");
            CreateIndex("dbo.Sg02_Persona", "Identificacion", unique: true);
            CreateIndex("dbo.Sg02_Persona", "Email", unique: true);
            CreateIndex("dbo.Sg01_Usuario", "NombreUsuario", unique: true);
            CreateIndex("dbo.Sg01_Usuario", "PersonaId", unique: true);
            CreateIndex("dbo.Sg04_UsuarioCargo", new[] { "CargoId", "UsuarioId" }, unique: true, name: "IX_UsuarioCargo");
            CreateIndex("dbo.Pe05_Lista", new[] { "NombreLista", "ProcesoElectoralId" }, unique: true, name: "IX_NombreListaProceso");
            CreateIndex("dbo.Sg08_UsuarioRol", new[] { "RolId", "UsuarioId" }, unique: true, name: "IX_UsuarioRol");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Sg08_UsuarioRol", "IX_UsuarioRol");
            DropIndex("dbo.Pe05_Lista", "IX_NombreListaProceso");
            DropIndex("dbo.Sg04_UsuarioCargo", "IX_UsuarioCargo");
            DropIndex("dbo.Sg01_Usuario", new[] { "PersonaId" });
            DropIndex("dbo.Sg01_Usuario", new[] { "NombreUsuario" });
            DropIndex("dbo.Sg02_Persona", new[] { "Email" });
            DropIndex("dbo.Sg02_Persona", new[] { "Identificacion" });
            DropIndex("dbo.Pe06_Candidato", "IX_ListaPersona");
            DropIndex("dbo.Mi01_PadronVotacion", "IX_ProcesoELusuario");
            AlterColumn("dbo.Pe05_Lista", "NombreLista", c => c.String(nullable: false));
            RenameIndex(table: "dbo.Pe04_Escanio", name: "IX_EscanioEleccion", newName: "IX_NombreEleccion");
            CreateIndex("dbo.Sg08_UsuarioRol", "UsuarioId");
            CreateIndex("dbo.Sg08_UsuarioRol", "RolId");
            CreateIndex("dbo.Pe05_Lista", "ProcesoElectoralId");
            CreateIndex("dbo.Sg04_UsuarioCargo", "UsuarioId");
            CreateIndex("dbo.Sg04_UsuarioCargo", "CargoId");
            CreateIndex("dbo.Sg01_Usuario", "PersonaId");
            CreateIndex("dbo.Pe06_Candidato", "PersonaId");
            CreateIndex("dbo.Pe06_Candidato", "ListaId");
            CreateIndex("dbo.Mi01_PadronVotacion", "UsuarioId");
            CreateIndex("dbo.Mi01_PadronVotacion", "ProcesoElectoralId");
        }
    }
}
