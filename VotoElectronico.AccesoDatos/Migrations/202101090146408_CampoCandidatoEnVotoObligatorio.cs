﻿namespace VotoElectronico.AccesoDatos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CampoCandidatoEnVotoObligatorio : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Mv02_Opcion", new[] { "CandidatoId" });
            AlterColumn("dbo.Mv02_Opcion", "CandidatoId", c => c.Long(nullable: false));
            CreateIndex("dbo.Mv02_Opcion", "CandidatoId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Mv02_Opcion", new[] { "CandidatoId" });
            AlterColumn("dbo.Mv02_Opcion", "CandidatoId", c => c.Long());
            CreateIndex("dbo.Mv02_Opcion", "CandidatoId");
        }
    }
}
