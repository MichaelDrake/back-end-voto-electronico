﻿namespace VotoElectronico.AccesoDatos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migracion : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Sg03_Cargo", "Participacion", c => c.Decimal(nullable: false, precision: 20, scale: 6));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Sg03_Cargo", "Participacion");
        }
    }
}
