﻿namespace VotoElectronico.AccesoDatos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class VotoCargo_CandidatoAlterno : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Pe07_AlternoCandidato",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        Foto = c.String(maxLength: 60),
                        PersonaId = c.Long(nullable: false),
                        Estado = c.String(maxLength: 10),
                        UsuarioCreacion = c.String(maxLength: 80),
                        FechaCreacion = c.DateTime(precision: 7, storeType: "datetime2"),
                        UsuarioModificacion = c.String(maxLength: 80),
                        FechaModificacion = c.DateTime(precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Pe06_Candidato", t => t.Id)
                .ForeignKey("dbo.Sg02_Persona", t => t.PersonaId)
                .Index(t => t.Id)
                .Index(t => t.PersonaId);
            
            AddColumn("dbo.Mv01_Voto", "CargoId", c => c.Long());
            CreateIndex("dbo.Mv01_Voto", "CargoId");
            AddForeignKey("dbo.Mv01_Voto", "CargoId", "dbo.Sg03_Cargo", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Pe07_AlternoCandidato", "PersonaId", "dbo.Sg02_Persona");
            DropForeignKey("dbo.Mv01_Voto", "CargoId", "dbo.Sg03_Cargo");
            DropForeignKey("dbo.Pe07_AlternoCandidato", "Id", "dbo.Pe06_Candidato");
            DropIndex("dbo.Mv01_Voto", new[] { "CargoId" });
            DropIndex("dbo.Pe07_AlternoCandidato", new[] { "PersonaId" });
            DropIndex("dbo.Pe07_AlternoCandidato", new[] { "Id" });
            DropColumn("dbo.Mv01_Voto", "CargoId");
            DropTable("dbo.Pe07_AlternoCandidato");
        }
    }
}
