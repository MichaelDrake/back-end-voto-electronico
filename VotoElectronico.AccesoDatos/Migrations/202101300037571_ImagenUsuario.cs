﻿namespace VotoElectronico.AccesoDatos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ImagenUsuario : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Sg01_Usuario", "ImagenUsuario", c => c.String(maxLength: 60));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Sg01_Usuario", "ImagenUsuario");
        }
    }
}
