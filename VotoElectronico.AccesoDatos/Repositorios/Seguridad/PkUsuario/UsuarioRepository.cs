﻿
using System.Collections.Generic;
using System.Linq;
using VotoElectronico.Entidades.Pk.PkSeguridad;
using VotoElectronicoExtensiones.Configuraciones;
using VotoElectronicoExtensiones.EntityFrameworkRepository;

namespace EcVotoElectronico.Repositorios
{
    public class UsuarioRepository : EntityFrameworkReadOnlyRepository<VotoDbContext>, IUsuarioRepository
    {
        public UsuarioRepository(VotoDbContext Context) : base(Context)
        {


        }

        public IEnumerable<Sg01_Usuario> FiltrarUsuario(ISpecification<Sg01_Usuario> specification, int pagina, out int total)
        {
            total = Context.Sg01_Usuarios.Count(specification.SatisfiedBy());
            return Context.Sg01_Usuarios.Where(specification.SatisfiedBy())?.OrderByDescending(x => x.FechaCreacion)?.Skip(pagina * 10)?.Take(10);
        }
        
    }
}
