﻿
using System.Collections.Generic;
using System.Linq;
using VotoElectronico.Entidades.Pk.PkMesaIdentificacion;
using VotoElectronicoExtensiones.Configuraciones;
using VotoElectronicoExtensiones.EntityFrameworkRepository;

namespace EcVotoElectronico.Repositorios
{
    public class PadronVotacionRepository : EntityFrameworkReadOnlyRepository<VotoDbContext>, IPadronVotacionRepository
    {
        public PadronVotacionRepository(VotoDbContext Context) : base(Context)
        {


        }

        public IEnumerable<Mi01_PadronVotacion> FiltrarPadronVotacion(ISpecification<Mi01_PadronVotacion> specification, int pagina, out int total)
        {

            total = Context.Mi01_PadronVotaciones.Count(specification.SatisfiedBy());
            return Context.Mi01_PadronVotaciones.Where(specification.SatisfiedBy())?.OrderByDescending(x => x.FechaCreacion)?.Skip(pagina * 10)?.Take(10);
        }
        }
}
