﻿
using VotoElectronicoExtensiones.EntityFrameworkRepository;

namespace EcVotoElectronico.Repositorios
{
    public class CandidatoAlternoRepository : EntityFrameworkReadOnlyRepository<VotoDbContext>, ICandidatoAlternoRepository
    {
        public CandidatoAlternoRepository(VotoDbContext Context) : base(Context)
        {


        }
    }
}
