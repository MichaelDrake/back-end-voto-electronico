﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VotoElectronico.Generico;
using VotoElectronico.LogicaNegocio.Servicios;
using VotoElectronico.Util;

namespace VotoElectronico.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/CtCrg")]
    public class CargoController: ApiController
    {
        private readonly ICargoService _cargoService;
        private readonly IApiResponseMessage _utilitarios;
        private readonly ITokenValidator _tokenValidator;
        public CargoController(ICargoService cargoService, IApiResponseMessage utilitarios, ITokenValidator tokenValidator)
        {
            _cargoService = cargoService;
            _utilitarios = utilitarios;
            _tokenValidator = tokenValidator;
        }


        [HttpPost]
        [Route("VESLCrgTkn")]
        public HttpResponseMessage VESLCrgTkn()
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                return Request.CreateResponse(HttpStatusCode.OK, _cargoService.ObtenerCargosActivoToken(token));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _utilitarios.crearDtoErrorExceptionMessage(ex));

            }
        }

        [HttpPost]
        [Route("VeSlCrgAct")]
        public HttpResponseMessage VeSlCrgAct()
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                return Request.CreateResponse(HttpStatusCode.OK, _cargoService.ObtenerTodosLosCargosActivos());
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _utilitarios.crearDtoErrorExceptionMessage(ex));

            }
        }


    }
}