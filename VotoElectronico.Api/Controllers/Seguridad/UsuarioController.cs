﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VotoElectronico.Generico;
using VotoElectronico.LogicaNegocio.Servicios;
using VotoElectronico.Util;

namespace VotoElectronico.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/usuario")]
    public class UsrController: ApiController
    {
        private readonly IUsuarioService _usuarioService;
        private readonly IApiResponseMessage _apiResponseMessage;
        private readonly ITokenValidator _tokenValidator;
        public UsrController(IUsuarioService usuarioService, IApiResponseMessage util, ITokenValidator tokenValidator)
        {
            _usuarioService = usuarioService;
            _apiResponseMessage = util;
            _tokenValidator = tokenValidator;
        }


        [HttpPost]
        [Route("crear-usuario")]
        public HttpResponseMessage crearUsuarioController (DtoUsuario dtoUsuario)
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                return Request.CreateResponse(HttpStatusCode.OK, _usuarioService.CrearUsuario(dtoUsuario,token));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));
            }
        }
        

        [HttpPost]
        [Route("VeCrUsrWb")]
        public HttpResponseMessage CrearUsuarioFromWebController(DtoUsuario dtoUsuario)
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                return Request.CreateResponse(HttpStatusCode.OK, _usuarioService.CrearUsuarioFromWeb(dtoUsuario, token));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));
            }
        }


        [HttpPut]
        [Route("actualizar-usuario")]
        public HttpResponseMessage modificarUsuarioController(DtoUsuario dtoUsuario)
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                return Request.CreateResponse(HttpStatusCode.OK, _usuarioService.ActualizarUsuario(dtoUsuario,token));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));
            }
        }

        [HttpPut]
        [Route("VeUpUsrWb")]
        public HttpResponseMessage ActualizarUsuarioWeb(DtoUsuario dtoUsuario)
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                return Request.CreateResponse(HttpStatusCode.OK, _usuarioService.ActualizarUsuarioWeb(dtoUsuario, token));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));
            }
        }

        [HttpPost]
        [Route("obtener-usuario-mediante-id")]
        public HttpResponseMessage obtenerUsuarioMedianteId(DtoUsuario dtoUsuario)
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                return Request.CreateResponse(HttpStatusCode.OK, _usuarioService.ObtenerUsuarioMedianteId(dtoUsuario));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));

            }
        }

        [HttpDelete]
        [Route("eliminar-usuario")]
        public HttpResponseMessage eliminarUsuario(DtoUsuario dtoUsuario)
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                return Request.CreateResponse(HttpStatusCode.OK, _usuarioService.EliminarUsuario(dtoUsuario));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));

            }
        }

        

        [HttpPost]
        [Route("obtUsrPrms")]
        public HttpResponseMessage ObtenerListaUsuariosByParametros(DtoEspecificacion dtoSpec)
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                return Request.CreateResponse(HttpStatusCode.OK, _usuarioService.ObtenerListaUsuariosByParametros(dtoSpec));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));

            }
        }


        [HttpPut]
        [Route("VeIncUsr")]
        public HttpResponseMessage InactivarUsuarioController(DtoUsuario dtoUsuario)
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                return Request.CreateResponse(HttpStatusCode.OK, _usuarioService.CambiarEstadoUsuario(dtoUsuario,token));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));

            }
        }


        [HttpPost]
        [Route("VeObUsrTkn")]
        public HttpResponseMessage obtenerUsuarioMedianteToken()
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                return Request.CreateResponse(HttpStatusCode.OK, _usuarioService.ObtenerUsuarioMedianteToken(token));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));

            }
        }

        [HttpPost]
        [Route("VERmvMl")]
        public HttpResponseMessage VERmvMl(DtoUsuario dto)
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                return Request.CreateResponse(HttpStatusCode.OK, _usuarioService.ReenviarEmailUsuario(dto));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));

            }
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("VErcLsT")]
        public HttpResponseMessage VErcLsT(DtoGenerico dto)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, _usuarioService.VaciarToken(dto.Dt1));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));

            }
        }


        [HttpPost]
        [Route("VELstUsrAct")]
        public HttpResponseMessage ObtenerListaUsuariosActivosMedianteNombreUsuario(DtoEspecificacion dto)
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                return Request.CreateResponse(HttpStatusCode.OK, _usuarioService.ObtenerListaUsuariosActivosMedianteNombreUsuario(dto.parametroBusqueda1));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));

            }
        }
    }
}