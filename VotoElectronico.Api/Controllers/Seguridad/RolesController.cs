﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VotoElectronico.Generico;
using VotoElectronico.Generico.Configs;
using VotoElectronico.Generico.Enumeraciones;
using VotoElectronico.Generico.Propiedades;
using VotoElectronico.LogicaNegocio.Servicios;
using VotoElectronico.Util;

namespace VotoElectronico.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/Roles")]
    public class RolesController: ApiController
    {
        private readonly IRolService _RolService;
        private readonly IApiResponseMessage _apiResponseMessage;
        private readonly ITokenValidator _tokenValidator;
        public RolesController(IRolService RolService, IApiResponseMessage apiResponseMessage, ITokenValidator tokenValidator)
        {
            _RolService = RolService;
            _apiResponseMessage = apiResponseMessage;
            _tokenValidator = tokenValidator;
        }


        [HttpPost]
        [Route("VESlRlsCb")]
        public HttpResponseMessage VESlRlsCb(DtoRol DtoRol)
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                return Request.CreateResponse(HttpStatusCode.OK, _RolService.ObtenerRolsPorNombre(DtoRol.NombreRol, Auditoria.EstadoActivo));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));
            }
        }
        [HttpPost]
        [Route("VESlRlsCbNoAd")]
        public HttpResponseMessage VESlRlsCbNoAd(DtoRol DtoRol)
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                return Request.CreateResponse(HttpStatusCode.OK, _RolService.ObtenerRolsPorNombre(DtoRol.NombreRol, Auditoria.EstadoActivo));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));
            }
        }

        [HttpPost]
        [Route("VESlRlsCbNoAdRt")]
        public HttpResponseMessage VESlRlsCbNoAdRt(DtoRol DtoRol)
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                return Request.CreateResponse(HttpStatusCode.OK, _RolService.ObtenerRolsRutaPorNombre(DtoRol.NombreRol, Auditoria.EstadoActivo));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));
            }
        }

        [HttpPost]
        [Route("VESlTdsRls")]
        public HttpResponseMessage ObtenerTodosLosRolesActivos()
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                return Request.CreateResponse(HttpStatusCode.OK, _RolService.ObtenerTodosLosRolesActivos());
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));
            }
        }

    }
}