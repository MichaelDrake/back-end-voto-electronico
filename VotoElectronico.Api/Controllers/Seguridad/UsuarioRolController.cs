﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VotoElectronico.Generico;
using VotoElectronico.LogicaNegocio.Servicios;
using VotoElectronico.Util;

namespace VotoElectronico.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/usrrl")]
    public class UsuarioRolController: ApiController
    {
        private readonly IUsuarioRolService _usuarioRolService;
        private readonly IApiResponseMessage _util;
        private readonly ITokenValidator _tokenValidator;
        public UsuarioRolController(IUsuarioRolService usuarioRolService, IApiResponseMessage apiResponseMessage, ITokenValidator tokenValidator)
        {
            _usuarioRolService = usuarioRolService;
            _util = apiResponseMessage;
            _tokenValidator = tokenValidator;
        }


    }
}