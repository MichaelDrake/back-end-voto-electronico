﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VotoElectronico.Generico;
using VotoElectronico.LogicaNegocio.Servicios;
using VotoElectronico.Util;

namespace VotoElectronico.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/Scn")]
    public class ScnController: ApiController
    {
        private readonly IEscanioService _escanioService;
        private readonly IApiResponseMessage _apiResponseMessage;
        private readonly ITokenValidator _tokenValidator;
        public ScnController(IEscanioService escanioService, IApiResponseMessage apiResponseMessage, ITokenValidator tokenValidator)
        {
            _escanioService = escanioService;
            _apiResponseMessage = apiResponseMessage;
            _tokenValidator = tokenValidator;
        }


        [HttpPost]
        [Route("VELCrEsc")]
        public HttpResponseMessage crearEscanioController (DtoEscanio dtoEscanio)
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                return Request.CreateResponse(HttpStatusCode.OK, _escanioService.CrearEscanio(dtoEscanio, token));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));
            }
        }

        [HttpPut]
        [Route("VELUpEsc")]
        public HttpResponseMessage modificarEscanioController(DtoEscanio dtoEscanio)
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                return Request.CreateResponse(HttpStatusCode.OK, _escanioService.ActualizarEscanio(dtoEscanio, token));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));
            }
        }

        [HttpPost]
        [Route("obtener-escanio-mediante-id")]
        public HttpResponseMessage obtenerEscanioMedianteId(DtoEscanio dtoEscanio)
        {
            try
            {
                _tokenValidator.ValidarToken(Request.Headers.Authorization.Parameter);
                return Request.CreateResponse(HttpStatusCode.OK, _escanioService.ObtenerEscanioMedianteId(dtoEscanio));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));

            }
        }

        [HttpPut]
        [Route("VELDlEsc")]
        public HttpResponseMessage VELDlEsc(DtoEscanio dtoEscanio)
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                return Request.CreateResponse(HttpStatusCode.OK, _escanioService.EliminarEscanio(dtoEscanio, token));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));

            }
        }

        [HttpPost]
        [Route("VELSlEsc")]
        public HttpResponseMessage VELSlEsc(DtoEscanio dto)
        {
            try
            {
                _tokenValidator.ValidarToken(Request.Headers.Authorization.Parameter);
                return Request.CreateResponse(HttpStatusCode.OK, _escanioService.ObtenerEscanios(dto.nombreEscanio, dto.estado));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));

            }
        }

        [HttpPost]
        [Route("VELSlEscLisId")]
        public HttpResponseMessage VELSlEscLisId(DtoLista dto)
        {
            try
            {
                _tokenValidator.ValidarToken(Request.Headers.Authorization.Parameter);
                return Request.CreateResponse(HttpStatusCode.OK, _escanioService.ObtenerEscaniosPorListaId(dto.Id));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));

            }
        }

        //Obtener escanios a elegir de un proceso electoral.

        [HttpPost]
        [Route("VELSlEscPe")]
        public HttpResponseMessage VELSlEscPe(DtoProcesoElectoral dtoProcesoElectoral)
        {
            try
            {
                _tokenValidator.ValidarToken(Request.Headers.Authorization.Parameter);
                return Request.CreateResponse(HttpStatusCode.OK, _escanioService.ObtenerEscaniosPorProcesoElectoralId(dtoProcesoElectoral.Id));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));

            }
        }

        [HttpPost]
        [Route("VESlEscEl")]
        public HttpResponseMessage VESlEscEl(DtoProcesoElectoral dtoProcesoElectoral)
        {
            try
            {
                _tokenValidator.ValidarToken(Request.Headers.Authorization.Parameter);
                return Request.CreateResponse(HttpStatusCode.OK, _escanioService.ObtenerEscaniosPorEleccionId(dtoProcesoElectoral.Id));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));

            }
        }

        [HttpPost]
        [Route("VERCESc")]
        public HttpResponseMessage VERCESc(DtoEscanio dto)
             {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                return Request.CreateResponse(HttpStatusCode.OK, _escanioService.ReactivarEscanio(dto.escanioId, token));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));
            }
        }


    }
}