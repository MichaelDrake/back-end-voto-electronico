﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VotoElectronico.Generico;
using VotoElectronico.LogicaNegocio.Servicios;
using VotoElectronico.ReglasNegocio.Validators;
using VotoElectronico.Util;

namespace VotoElectronico.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/Cnd")]
    public class CandidatoController: ApiController
    {
        private readonly ICandidatoService _candidatoService;
        private readonly IApiResponseMessage _apiResponseMessage;
        private readonly ITokenValidator _tokenValidator;
        private readonly IProcesoValidator _procesoValidator;
        public CandidatoController(ICandidatoService candidatoService, 
            IApiResponseMessage apiResponseMessage,
            ITokenValidator tokenValidator,
            IProcesoValidator procesoValidator
            )
        {
            _candidatoService = candidatoService;
            _apiResponseMessage = apiResponseMessage;
            _tokenValidator = tokenValidator;
            _procesoValidator = procesoValidator;
    }


        [HttpPost]
        [Route("VELCrCnd")]
        public HttpResponseMessage crearCandidatoController (DtoCandidato dtoCandidato)
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                _procesoValidator.ValidarProcesoListaId(dtoCandidato.listaId);
                return Request.CreateResponse(HttpStatusCode.OK, _candidatoService.CrearCandidato(dtoCandidato, token));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));
            }
        }

        [HttpPut]
        [Route("VELUpCnd")]
        public HttpResponseMessage modificarCandidatoController(DtoCandidato dtoCandidato)
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                _procesoValidator.ValidarProcesoListaId(dtoCandidato.listaId);
                return Request.CreateResponse(HttpStatusCode.OK, _candidatoService.ActualizarCandidato(dtoCandidato));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));
            }
        }

        [HttpPost]
        [Route("VELObtCndById")]
        public HttpResponseMessage obtenerCandidatoMedianteId(DtoCandidato dtoCandidato)
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                return Request.CreateResponse(HttpStatusCode.OK, _candidatoService.ObtenerCandidatoMedianteId(dtoCandidato));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));

            }
        }

        [HttpDelete]
        [Route("VELDlCnd")]
        public HttpResponseMessage eliminarCandidato(long IdCandidato)
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                _procesoValidator.ValidarProcesoCandidatoId(IdCandidato);
                return Request.CreateResponse(HttpStatusCode.OK, _candidatoService.EliminarCandidato(new DtoCandidato() { Id = IdCandidato }));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));

            }
        }

        //[HttpPost]
        //[Route("obtener-candidato-por-nombre")]
        //public HttpResponseMessage obtenerCandidatosPorNombre(DtoEspecificacion dtoSpec)
        //{
        //    try
        //    {
        //        return Request.CreateResponse(HttpStatusCode.OK, _candidatoService.ObtenerCandidatoesPorNombre(dtoSpec.parametroBusqueda1));
        //    }
        //    catch (Exception ex)
        //    {
        //        return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));

        //    }
        //}


        [HttpPost]
        [Route("VELObtCndByParams")]
        public HttpResponseMessage obtenerCandidatosPorNombre(DtoEspecificacion dtoSpec)
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                return Request.CreateResponse(HttpStatusCode.OK, _candidatoService.ObtenerCandidatoMedianteParametros(dtoSpec.parametroBusqueda4, dtoSpec.parametroBusqueda2,dtoSpec.parametroBusqueda3));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));

            }
        }

        [HttpPost]
        [Route("VELObtCndByLisId")]
        public HttpResponseMessage obtenerCandidatosMedianteListaId(DtoCandidato dtoCandidato)
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                return Request.CreateResponse(HttpStatusCode.OK, _candidatoService.ObtenerCandidatoMedianteListaId(dtoCandidato.listaId, dtoCandidato.estado));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));

            }
        }


    }
}