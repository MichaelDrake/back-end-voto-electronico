﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VotoElectronico.Generico;
using VotoElectronico.LogicaNegocio.Servicios;
using VotoElectronico.ReglasNegocio.Validators;
using VotoElectronico.Util;

namespace VotoElectronico.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/Lis")]
    public class ListaController: ApiController
    {
        private readonly IListaService _listaService;
        private readonly IApiResponseMessage _apiResponseMessage;
        private readonly IProcesoValidator _procesoValidator;
        private readonly ITokenValidator _tokenValidator;

        public ListaController(IListaService listaService, IApiResponseMessage apiResponseMessage, IProcesoValidator procesoValidator, ITokenValidator tokenValidator)
        {
            _listaService = listaService;
            _apiResponseMessage = apiResponseMessage;
            _procesoValidator = procesoValidator;
            _tokenValidator = tokenValidator;
        }


        [HttpPost]
        [Route("VELCrLis")]
        public HttpResponseMessage crearListaController (DtoLista dtoLista)
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                _procesoValidator.ValidarProceso(dtoLista.procesoElectoralId);
                return Request.CreateResponse(HttpStatusCode.OK, _listaService.CrearLista(dtoLista , token));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));
            }
        }

        [HttpPut]
        [Route("VELUpLis")]
        public HttpResponseMessage modificarListaController(DtoLista dtoLista)
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                _procesoValidator.ValidarProceso(dtoLista.procesoElectoralId);
                return Request.CreateResponse(HttpStatusCode.OK, _listaService.ActualizarLista(dtoLista, token));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));
            }
        }

        [HttpPost]
        [Route("obtener-lista-mediante-id")]
        public HttpResponseMessage obtenerListaMedianteId(DtoLista dtoLista)
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                return Request.CreateResponse(HttpStatusCode.OK, _listaService.ObtenerListaMedianteId(dtoLista));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));

            }
        }

        [HttpDelete]
        [Route("VELDelLis")]
        public HttpResponseMessage eliminarLista(long listaId)
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                _procesoValidator.ValidarProcesoListaId(listaId);
                return Request.CreateResponse(HttpStatusCode.OK, _listaService.EliminarLista(listaId));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));

            }
        }

        [HttpPost]
        [Route("VELObtLisByPrms")]
        public HttpResponseMessage obtenerListasPorNombre(DtoLista dtoLista)
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                return Request.CreateResponse(HttpStatusCode.OK, _listaService.ObtenerListasMedianteParams(dtoLista.nombreLista, dtoLista.estado));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));

            }
        }

        [HttpPost]
        [Route("VELObtLisByProcEl")]
        public HttpResponseMessage obtenerListasPorProcesoElectoralId(DtoLista dtoLista)
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                return Request.CreateResponse(HttpStatusCode.OK, _listaService.obtenerListasPorProcesoElectoralId(dtoLista.procesoElectoralId, dtoLista.estado));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));

            }
        }


        //Obtener listas con informacion de los candidatos, para el momento de votar.
        [HttpPost]
        [Route("VELObtLisCandProcEl")]
        public HttpResponseMessage obtenerListasYCandidatosPorProcesoElectoralId(DtoLista dtoLista)
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                return Request.CreateResponse(HttpStatusCode.OK, _listaService.obtenerListasYCandidatosByProcesoElectoralId(dtoLista.procesoElectoralId));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));

            }
        }

    }
}