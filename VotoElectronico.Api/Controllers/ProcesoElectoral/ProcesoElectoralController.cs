﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VotoElectronico.Generico;
using VotoElectronico.LogicaNegocio.Servicios;
using VotoElectronico.ReglasNegocio.Validators;
using VotoElectronico.Util;

namespace VotoElectronico.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/Pelc")]
    public class ProcesoElectoralController: ApiController
    {
        private readonly IProcesoElectoralService _procesoElectoralService;
        private readonly IApiResponseMessage _apiResponseMessage;
        private readonly ITokenValidator _tokenValidator;
        private readonly IProcesoValidator _procesoValidator;
        public ProcesoElectoralController(IProcesoElectoralService procesoElectoralService, IApiResponseMessage apiResponseMessage, ITokenValidator tokenValidator, IProcesoValidator procesoValidator)
        {
            _procesoElectoralService = procesoElectoralService;
            _apiResponseMessage = apiResponseMessage;
            _tokenValidator = tokenValidator;
            _procesoValidator = procesoValidator;
        }


        [HttpPost]
        [Route("VELCrPelc")]
        public HttpResponseMessage crearProcesoElectoralController (DtoProcesoElectoral dtoProcesoElectoral)
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                return Request.CreateResponse(HttpStatusCode.OK, _procesoElectoralService.CrearProcesoElectoral(dtoProcesoElectoral, token));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));
            }
        }

        [HttpPut]
        [Route("VELUpPelc")]
        public HttpResponseMessage modificarProcesoElectoralController(DtoProcesoElectoral dtoProcesoElectoral)
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                _procesoValidator.ValidarProceso(dtoProcesoElectoral.Id);
                return Request.CreateResponse(HttpStatusCode.OK, _procesoElectoralService.ActualizarProcesoElectoral(dtoProcesoElectoral, token));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));
            }
        }

        [HttpPost]
        [Route("VELObtPelcById")]
        public HttpResponseMessage obtenerProcesoElectoralMedianteId(DtoProcesoElectoral dtoProcesoElectoral)
        {
            try
            {
                _tokenValidator.ValidarToken(Request.Headers.Authorization.Parameter);
                return Request.CreateResponse(HttpStatusCode.OK, _procesoElectoralService.ObtenerProcesoElectoralMedianteId(dtoProcesoElectoral));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));

            }
        }

        [HttpDelete]
        [Route("VELDlPelc")]
        public HttpResponseMessage eliminarProcesoElectoral(long idProcesoElectoral)
        {
            try
            {
                _tokenValidator.ValidarToken(Request.Headers.Authorization.Parameter);
                _procesoValidator.ValidarProceso(idProcesoElectoral);
                return Request.CreateResponse(HttpStatusCode.OK, _procesoElectoralService.EliminarProcesoElectoral(new DtoProcesoElectoral() { Id = idProcesoElectoral}));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));

            }
        }

        [HttpPost]
        [Route("VELObtPelcByPrms")]
        public HttpResponseMessage obtenerProcesoElectoralsPorNombre(DtoEspecificacion dtoProcesoElectoral)
        {
            try
            {
                _tokenValidator.ValidarToken(Request.Headers.Authorization.Parameter);
                return Request.CreateResponse(HttpStatusCode.OK, _procesoElectoralService.ObtenerProcesoElectoralesPorNombre(dtoProcesoElectoral.parametroBusqueda1, dtoProcesoElectoral.parametroBusqueda2, dtoProcesoElectoral.parametroBusqueda4, dtoProcesoElectoral.Bdt));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));

            }
        }

        //Obtener los procesos electorales vigentes(Que estan en proceso/ que se puede votar ), mediante el token del usuario.

        [HttpPost]
        [Route("VELObtPelcVig")]
        public HttpResponseMessage obtenerProcesoElectoralesVigentes()
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                return Request.CreateResponse(HttpStatusCode.OK, _procesoElectoralService.ObtenerProcesoElectoralesVigentesByToken(token));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));

            }
        }


        [HttpPost]
        [Route("VESLPrcCtv")]
        public HttpResponseMessage VESLPrcCtv()
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                return Request.CreateResponse(HttpStatusCode.OK, _procesoElectoralService.ObtenerTodosProcesosElectoralesActivos());
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));

            }
        }

        /*Obtener procesos electorales finalizados mendiate token*/
        /*fecha: 16 de marzo de 2021 */
        [HttpPost]
        [Route("VELPelFnlz")]
        public HttpResponseMessage VELPelFnlz(DtoEspecificacion dtoProcesoElectoral)
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                return Request.CreateResponse(HttpStatusCode.OK, _procesoElectoralService.ObtenerProcesoElectoralesFinalizadosByParams(token,dtoProcesoElectoral.parametroBusqueda1,dtoProcesoElectoral.parametroBusqueda5));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));

            }
        }


    }
}