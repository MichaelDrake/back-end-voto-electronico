﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VotoElectronico.Generico;
using VotoElectronico.LogicaNegocio.Servicios;
using VotoElectronico.Util;

namespace VotoElectronico.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/Vt")] 
    public class VotoController: ApiController
    {
        private readonly IVotoService _votoService;
        private readonly IApiResponseMessage _apiResponseMessage;
        private readonly ITokenValidator _tokenValidator;
        public VotoController(IVotoService votoService, IApiResponseMessage apiResponseMessage, ITokenValidator tokenValidator)
        {
            _votoService = votoService;
            _apiResponseMessage = apiResponseMessage;
            _tokenValidator = tokenValidator;
        }


        [HttpPost]
        [Route("VeRsmPrc")]
        public HttpResponseMessage VeRsmPrc(DtoProcesoElectoral dtoProcesoElectoral)
            {
            try
            {
                _tokenValidator.ValidarToken(Request.Headers.Authorization.Parameter);
                return Request.CreateResponse(HttpStatusCode.OK, _votoService.ObtenerResumenProcesoElectoral(dtoProcesoElectoral.Id));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));
            }
        }

    }
}