﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VotoElectronico.Generico;
using VotoElectronico.LogicaNegocio.Servicios;
using VotoElectronico.Util;

namespace VotoElectronico.Api.Controllers
{
    //[Authorize]
    [RoutePrefix("api/TpElc")]
    public class TipoEleccionController: ApiController
    {
        private readonly ITipoEleccionService _tipoEleccionService;
        private readonly IApiResponseMessage _apiResponseMessage;
        public TipoEleccionController(ITipoEleccionService TipoEleccionService, IApiResponseMessage apiResponseMessage)
        {
            _tipoEleccionService = TipoEleccionService;
            _apiResponseMessage = apiResponseMessage;
        }


        [HttpPost]
        [Route("VELCrElc")]
        public HttpResponseMessage crearTipoEleccionController (DtoTipoEleccion dtoTipoEleccion)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, _tipoEleccionService.CrearTipoEleccion(dtoTipoEleccion));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));
            }
        }

        [HttpPut]
        [Route("VELUpElc")]
        public HttpResponseMessage modificarTipoEleccionController(DtoTipoEleccion dtoTipoEleccion)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, _tipoEleccionService.ActualizarTipoEleccion(dtoTipoEleccion));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));
            }
        }

        [HttpPost]
        [Route("VELObtElcById")]
        public HttpResponseMessage obtenerTipoEleccionMedianteId(DtoTipoEleccion dtoTipoEleccion)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, _tipoEleccionService.ObtenerTipoEleccionMedianteId(dtoTipoEleccion));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));

            }
        }

        [HttpDelete]
        [Route("VELDlElc")]
        public HttpResponseMessage eliminarTipoEleccion(DtoTipoEleccion dtoTipoEleccion)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, _tipoEleccionService.EliminarTipoEleccion(dtoTipoEleccion));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));

            }
        }

        [HttpPost]
        [Route("VELObtTpElcByPrms")]
        public HttpResponseMessage obtenerTipoEleccionsPorNombre(DtoTipoEleccion dtoTipoEleccion)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, _tipoEleccionService.ObtenerTipoEleccionPorParametros(dtoTipoEleccion.nombreTipoEleccion, dtoTipoEleccion.estado));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));

            }
        }


    }
}