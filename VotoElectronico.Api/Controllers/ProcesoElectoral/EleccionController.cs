﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VotoElectronico.Generico;
using VotoElectronico.LogicaNegocio.Servicios;
using VotoElectronico.Util;

namespace VotoElectronico.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/Elc")]
    public class EleccionController: ApiController
    {
        private readonly IEleccionService _eleccionService;
        private readonly IApiResponseMessage _apiResponseMessage;
        private readonly ITokenValidator _tokenValidator;
        public EleccionController(IEleccionService EleccionService, IApiResponseMessage apiResponseMessage, ITokenValidator tokenValidator)
        {
            _eleccionService = EleccionService;
            _apiResponseMessage = apiResponseMessage;
            _tokenValidator = tokenValidator;
        }


        [HttpPost]
        [Route("VELCrElc")]
        public HttpResponseMessage crearEleccionController (DtoEleccion dtoEleccion)
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                return Request.CreateResponse(HttpStatusCode.OK, _eleccionService.CrearEleccion(dtoEleccion, token));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));
            }
        }

        [HttpPut]
        [Route("VELUpElc")]
        public HttpResponseMessage modificarEleccionController(DtoEleccion dtoEleccion)
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                return Request.CreateResponse(HttpStatusCode.OK, _eleccionService.ActualizarEleccion(dtoEleccion, token));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));
            }
        }

        [HttpPost]
        [Route("VELObtElcById")]
        public HttpResponseMessage obtenerEleccionMedianteId(DtoEleccion dtoEleccion)
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                return Request.CreateResponse(HttpStatusCode.OK, _eleccionService.ObtenerEleccionMedianteId(dtoEleccion));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));

            }
        }

        [HttpPut]
        [Route("VELDlElc")]
        public HttpResponseMessage eliminarEleccion(DtoEleccion dtoEleccion)
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                return Request.CreateResponse(HttpStatusCode.OK, _eleccionService.EliminarEleccion(dtoEleccion, token));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));

            }
        }

        [HttpPost]
        [Route("VELObtElcByPrms")]
        public HttpResponseMessage obtenerEleccionsPorParametros(DtoEleccion dtoEleccion)
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                return Request.CreateResponse(HttpStatusCode.OK, _eleccionService.ObtenerEleccionesPorNombre(dtoEleccion.nombreEleccion, dtoEleccion.estado ));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));

            }
        }



        [HttpPost]
        [Route("VERcElcc")]
        public HttpResponseMessage VERcElcc(DtoEleccion dtoEleccion)
        {
            try
            {
                var token = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(token);
                return Request.CreateResponse(HttpStatusCode.OK, _eleccionService.ReactivarElecion(dtoEleccion.Id, token));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));

            }
        }

    }
}