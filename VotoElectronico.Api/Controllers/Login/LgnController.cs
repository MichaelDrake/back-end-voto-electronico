﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using VotoElectronico.Generico;
using VotoElectronico.LogicaNegocio.Servicios;
using VotoElectronico.Util;

namespace VotoElectronico.Api.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("api/Lgn")]
    public class LgnController: ApiController
    {
        private readonly ILoginService _loginService;
        private readonly IApiResponseMessage _apiResponseMessage;
        private readonly ISesionService _sesionService;
        public LgnController(ILoginService loginService, IApiResponseMessage apiResponseMessage, ISesionService sesionService)
        {
            _loginService = loginService;
            _apiResponseMessage = apiResponseMessage;
            _sesionService = sesionService;
        }


        [HttpPost]
        [Route("VELgnUsr")]
        public async Task<HttpResponseMessage> VELgnUsr(DtoGenerico dto)
        {
            try
            {
                var usuario = _loginService.IniciarSesion(dto.Dt1, dto.Dt2);
                if (usuario.Bdt1)
                {
                    var token = TokenGenerator.GenerateTokenJwt(usuario.Dt1, DateTime.Now.ToShortTimeString());
                    usuario.Dt8 = token;
                    _sesionService.AniadirTokenUsuario(usuario.Id, token);
                    return Request.CreateResponse(HttpStatusCode.OK, _apiResponseMessage.CrearDtoApiResponseMessage(new List<DtoGenerico>() {usuario}, "VE_LGN_INS_004"));
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.CrearDtoApiResponseMessage(new List<DtoGenerico>() {usuario}, usuario.Dt1));
            }
            catch (Exception ex)
            {
                 return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));

            }
        }


    }
}