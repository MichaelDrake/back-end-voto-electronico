﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using VotoElectronico.Generico;
using VotoElectronico.LogicaNegocio.Servicios;
using VotoElectronico.ReglasNegocio.Validators;
using VotoElectronico.Util;

namespace VotoElectronico.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/PdrVtc")] 
    public class PdrVtcController : ApiController
    {
        private readonly IPadronVotacionService _padronVotacionService;
        private readonly IApiResponseMessage _apiResponseMessage;
        private readonly ITokenValidator _tokenValidator;
        private readonly IAesService _aesService;
        private readonly IProcesoValidator _procesoValidator;
        public PdrVtcController (
            IPadronVotacionService padronVotacionService, 
            IApiResponseMessage apiResponseMessage, 
            ITokenValidator tokenValidator,
            IAesService aesService,
            IProcesoValidator procesoValidator
            )
        {
            _padronVotacionService = padronVotacionService;
            _apiResponseMessage = apiResponseMessage;
            _tokenValidator = tokenValidator;
            _aesService = aesService;
            _procesoValidator = procesoValidator;

        }



        [HttpPut]
        [Route("actualizar-padron-votacion")]
        public HttpResponseMessage modificarPadronVotacionController(DtoPadronVotacion dtoPadronVotacion)
        {
            try
            {
                
                _tokenValidator.ValidarToken(Request.Headers.Authorization.Parameter);
                var token = Request.Headers.Authorization.Parameter;
                return Request.CreateResponse(HttpStatusCode.OK, _padronVotacionService.ActualizarPadronVotacion(dtoPadronVotacion));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));
            }
        }

        [HttpPost]
        [Route("VESlPdId")]
        public HttpResponseMessage obtenerPadronVotacionMedianteId(DtoPadronVotacion dtoPadronVotacion)
        {
            try
            {
                _tokenValidator.ValidarToken(Request.Headers.Authorization.Parameter);
                var token = Request.Headers.Authorization.Parameter;
                return Request.CreateResponse(HttpStatusCode.OK, _padronVotacionService.ObtenerPadronVotacionMedianteId(dtoPadronVotacion));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));

            }
        }

        [HttpPost]
        [Route("eliminar-padron-votacion")]
        public HttpResponseMessage eliminarPadronVotacion(DtoPadronVotacion dtoPadronVotacion)
        {
            try
            {
                _procesoValidator.ValidarProceso(dtoPadronVotacion.procesoElectoralId);
                _tokenValidator.ValidarToken(Request.Headers.Authorization.Parameter);
                var token = Request.Headers.Authorization.Parameter;
                return Request.CreateResponse(HttpStatusCode.OK, _padronVotacionService.EliminarPadronVotacion(dtoPadronVotacion));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));

            }
        }





        [HttpPost]
        [Route("VESlPdr")]
        public HttpResponseMessage ObtenerPadronesFiltros(DtoPadronVotacion dtoPadronVotacion)
        {
            try
            {
                _tokenValidator.ValidarToken(Request.Headers.Authorization.Parameter);
                var token = Request.Headers.Authorization.Parameter;
                return Request.CreateResponse(HttpStatusCode.OK, _padronVotacionService.ObtenerPadronVotacionesPorNombre(dtoPadronVotacion.Id, dtoPadronVotacion.NombreEmpadronado, dtoPadronVotacion.Pagina));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));

            }
        }

        [HttpPost]
        [Route("VeCrPel")]
        public HttpResponseMessage VeCrPel(DtoPadronVotacion dtoPadronVotacion)
        {
            try
            {
                _procesoValidator.ValidarProceso(dtoPadronVotacion.procesoElectoralId);
                _tokenValidator.ValidarToken(Request.Headers.Authorization.Parameter);
                var token = Request.Headers.Authorization.Parameter;
                return Request.CreateResponse(HttpStatusCode.OK, _padronVotacionService.CrearPadronVotacion(dtoPadronVotacion, token));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));

            }
        }


        //Funcion para autorizar y firmar un voto con la llave privada 
        //    mesa de identificacion
        //16/12/20
        [HttpPost]
        [Route("AutUsrPdr")]
        public HttpResponseMessage AutorizarUsuarioPadron(DtoVotoRSA dtoVotoRsa)
        {

            try
            {
                _tokenValidator.ValidarToken(Request.Headers.Authorization.Parameter);
                var token = Request.Headers.Authorization.Parameter;
                return Request.CreateResponse(HttpStatusCode.OK, _padronVotacionService.AutorizarVoto(dtoVotoRsa,token));
               
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);

            }
        }


    }
}