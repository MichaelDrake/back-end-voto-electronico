﻿using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using VotoElectronico.Generico;
using VotoElectronico.Importaciones.Importaciones.Padron;
using VotoElectronico.LogicaNegocio.Servicios;
using VotoElectronico.ReglasNegocio.Validators;
using VotoElectronico.Util;

namespace VotoElectronico.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/MptrPdr")]
    public class MptrPdrController : ApiController
    {
        private readonly IApiResponseMessage _apiResponseMessage;
        private readonly IImportacionPadron _importacionPadron;
        private readonly ITokenValidator _tokenValidator;
        private readonly IProcesoValidator _procesoValidator;
        public MptrPdrController(IApiResponseMessage apiResponseMessage, ITokenValidator tokenValidator, IImportacionPadron importacionPadron, IProcesoValidator procesoValidator)
        {
            _apiResponseMessage = apiResponseMessage;
            _tokenValidator = tokenValidator;
            _importacionPadron = importacionPadron;
            _procesoValidator = procesoValidator;
        }


   
       
        [HttpPost]
        [Route("VmMptPdr")]
        public HttpResponseMessage VmMptCnt()
        {
            try
            {
                string pathArchivos = ConfigurationManager.AppSettings["fld-tmp"];

                var file = HttpContext.Current.Request.Files.Count > 0 ? HttpContext.Current.Request.Files[0] : throw new Exception("Archivo no encontrado");
                DtoGenerico dto = JsonConvert.DeserializeObject<DtoGenerico>(HttpContext.Current.Request.Params["dto"]);

                dto.Dt20 = Request.Headers.Authorization.Parameter;
                _tokenValidator.ValidarToken(dto.Dt20);
                _procesoValidator.ValidarProceso(dto.Id);

                Utilidades.ValidarExisteCarpeta(pathArchivos);

                if (file != null && file.ContentLength > 0 && file.ValidarExtencionArchivo(".xlsx", ".xsl"))
                {
                    string rutaArchivo = pathArchivos + file.FileName;
                    Utilidades.VerificarExistenciArchivo(rutaArchivo);
                    file.SaveAs(rutaArchivo);
                    dto.Dt1 = rutaArchivo;
                    var importacionPadrones = _importacionPadron.LeerArchivo(dto.Dt1, dto.Id, dto.Dt20);
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, importacionPadrones);
                    response.Headers.Add("dto", JsonConvert.SerializeObject(importacionPadrones));
                    response.Headers.Add("Access-Control-Expose-Headers", "dto");
                    return response;

                }
                throw new Exception("Archivo no válido");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));
            }
        }


    }
}