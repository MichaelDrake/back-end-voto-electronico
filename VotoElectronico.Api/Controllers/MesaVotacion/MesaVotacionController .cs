﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using VotoElectronico.Generico;
using VotoElectronico.LogicaNegocio.Servicios;
using VotoElectronico.ReglasNegocio.Validators;
using VotoElectronico.Util;

namespace VotoElectronico.Api.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("api/MV")] 
    public class MesaVotacionController : ApiController
    {
        private readonly IMesaVotoService _mesaVotoService;
        private readonly IApiResponseMessage _apiResponseMessage;
        private readonly IProcesoValidator _procesoValidator;
        public MesaVotacionController(
            IMesaVotoService mesaVotoService,
            IApiResponseMessage apiResponseMessage,
            IProcesoValidator procesoValidator
            )
        {
            _mesaVotoService = mesaVotoService;
            _apiResponseMessage = apiResponseMessage;
            _procesoValidator = procesoValidator;
        }



        //Funcion para guardar un voto firmado
        //18/12/20
        [HttpPost]
        [Route("EmtVt")]
        public HttpResponseMessage EmitirVoto(DtoAES dtoAes)
        {

            try
            {
                var procesoId = Convert.ToInt64(UtilEncodeDecode.Base64Decode(dtoAes.procesoElectoralId));
                _procesoValidator.ValidarProcesoParaEmitirVoto(procesoId);
                return Request.CreateResponse(HttpStatusCode.OK, _mesaVotoService.EmitirVoto(dtoAes));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));
            }
        }


        //Funcion para obtener el voto mediante la mascara
        //08-01-21
        [HttpPost]
        [Route("ObtVtMas")]
        public HttpResponseMessage ObtVtMas(DtoVoto dtovoto)
        {

            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, _mesaVotoService.VerificarVoto(dtovoto.Mascara, dtovoto.ProcesoElectoralId));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, _apiResponseMessage.crearDtoErrorExceptionMessage(ex));
            }
        }



    }
}